function warm_rand(s)
{
	srand(s);
}
function max(s1, a, s2, b, s3, c)
{
	if (a > b && a > c)
		return s1;
	else if (b > a && b > c)
		return s2;
	else if (c > a && c > b)
		return s3;
}
BEGIN {
	PI = 3.1415927;
	t1 = 674; t0 = 0;

# A elements data structure:
	a_y0 = .8; a_y1 = .129; a_tot = 44;
	a_bconst = log(a_y0);
	a_aconst = (log(a_y1) - a_bconst) / t1;
# B elements data structure:
	b_y0 = .11; b_y1 = .1; b_tot = 33;
	b_amp = .317 - b_y0;
	b_offset = b_y0 + b_amp;
# C elements data structure:
	c_y0 = .1; c_y1 = .8; c_tot = 32;
	c_bconst = log(c_y0);
	c_aconst = (log(c_y1) - c_bconst) / t1;

	seed_flag = 1;
}
/^\n/ {}
/^#/  {}
/^=/ {
	print;
}
/^ / || /^[0-9]/ {
	if (seed_flag)
	{
		if (!seed)
			seed = 143;
		warm_rand(seed);
		seed_flag = 0;
	}
	A_value = exp((a_aconst*$1)+a_bconst);
	B_value = b_offset + (b_amp * -cos(($1/t1)*(2*PI)));
	C_value = exp((c_aconst*$1)+c_bconst);
	tot = A_value + B_value + C_value;
	A_draw = rand() * tot * A_value;
	B_draw = rand() * tot * B_value;
	C_draw = rand() * tot * C_value;

	winner = max("A", A_draw, "B", B_draw, "C", C_draw);

	printf ("%s %s\n", $0, winner);
}
