#!/bin/ksh
#
# rfindera.ksh - Usage:
#	rfindera <start_time> <end_time> <tempo_variable_name>
#
# $Id: rfindera.ksh 0.0 93/06/06 23:57:39 nicb Exp Locker: nicb $
# 
# set -x
if [ $# -lt 3 ]
then
	echo "Usage: $0 <start_time> <end_time> <tempo_variable_name>" 1>&2;
	exit -1;
fi
START_TIME=$1
END_TIME=$2
TEMPO_VAR_NAME=$3
TEMPO_FILE_NAME=tempos/floats.500/${TEMPO_VAR_NAME}.out
RFINDERA_TMPFILE=$TMP/rfa${RANDOM}.tmp
RFINDERA_SPLIT_PREFIX=$TMP/rfa

split -200 $TEMPO_FILE_NAME $RFINDERA_SPLIT_PREFIX

RFINDERA_INDEXES1=$(cut -d: -f 2 ${RFINDERA_SPLIT_PREFIX}aa)
RFINDERA_INDEXES2=$(cut -d: -f 2 ${RFINDERA_SPLIT_PREFIX}ab)
RFINDERA_INDEXES3=$(cut -d: -f 2 ${RFINDERA_SPLIT_PREFIX}ac)

awk -f bin/twin.awk start=$START_TIME end=$END_TIME \
	 fsorted.dat > $RFINDERA_TMPFILE

echo $0 "$@"
for i in $RFINDERA_INDEXES1 $RFINDERA_INDEXES2 $RFINDERA_INDEXES3
do
	awk -f bin/rfit.awk no_normal_output=0 ${TEMPO_VAR_NAME}=$i \
		debug_tempo=1 debug_output=1 $RFINDERA_TMPFILE | \
	awk -f bin/rfitavg.awk tempo=$i
done

rm ${RFINDERA_SPLIT_PREFIX}aa ${RFINDERA_SPLIT_PREFIX}ab \
	${RFINDERA_SPLIT_PREFIX}ac $RFINDERA_TMPFILE
#
# $Log:	rfindera.ksh $
# Revision 0.0  93/06/06  23:57:39  nicb
# Initial Revision
# 
#
