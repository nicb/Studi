# This is the pass-two awk script driven by all shell scripts
# in single directories
#
function set_tempo(tm)
{
	tempo = tm;
	conv = 60/tempo; # remap tempo
}
function _dur(s)
{
	return conv * s;
}
function dur(s)
{
	return _dur(s);
}
function _at_time(s)
{
	intv = s - last_at_time;
	offset += _dur(intv);
	last_at_time = s;
# print "s: " s ", last_at_time: " last_at_time ", offset: " offset;
	if (TEMPO_CHANGED)
	{
		set_tempo(new_tempo);
		TEMPO_CHANGED = 0;
	}

	return offset;
}
function at_time(s)
{
	return (_at_time(s));
}
function signal_tempo_change(t, at)
{
	TEMPO_CHANGED = 1;
	last_tempo_change_time = at;
	new_tempo = t;
}
BEGIN {
	FS=":";
	last_at_time = 0;
	bar = 0;
	TEMPO_CHANGED = 0;
	last_tempo_change_time = 0;
	new_tempo = 0;
}
NR == 1 { # this can't be put in the BEGIN section!
# t: tempo argument from outside
# ot: offset time argument from outside
# ob: offset beats argument from outside
	set_tempo(t);
	offset = ot - (conv*ob); # initial offset
}
/^#/ {
#	print # do not kill comments (at least for now)
	next;
}
/\===/ {
#	print "==="; # print the ===
	next;
}
$2 == "tempo" {
	signal_tempo_change($3, $1);
	next;
}
$1 ~ /[0-9.]*/ {
	printf("%.4f %d %.4f %s\n", at_time($1), $2, dur($3), $4);
	next;
}
