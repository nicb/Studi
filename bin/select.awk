BEGIN {
	while ("awk -F: '{ print $1 }' frags.dat" | getline authors[i] > 0)
		++i;
	i = 0;
	while ("awk -F: '{ print $2 }' frags.dat" | getline works[i] > 0)
		++i;
	i = 0;
	while ("awk -F: '{ print $3 }' frags.dat" | getline years[i] > 0)
		++i;

	for (j=0; j < i; ++j)
		print authors[j],years[j],works[j];
}
