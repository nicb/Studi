function m_12(i)
{
	if (i > 11)
		i -= 12;
	else if (i < 0)
		i += 12;

	return i;
}
function calc_index(n)
{
	if (n < 0)
		n += 12;
	while (n > 6)
		n = 12 - n;

	return n;
}
function calc_ivector(vector, vct, ivec_num)
{
	for (i = 1; i <= ivec_num; ++i)
	{
		for (j = 1; j < i; ++j)
		{
			num = calc_index(vector[i] - vector[j]);
			if (num > 0)
				vct[num] += 1;
		}
	}
}
function clear_vector(clrv)
{
	for (i = 0; i <= 128; ++i)
		delete clrv[i];
}
function load_vector(vect, vload, start, end)
{
	for (i = 1; i <= end; ++i)
		vload[i] = vect[start+i];
}
/^-->/ {
	print;
	next;
}
{
	clear_vector(v);
	split($0, vect, " ");
	calc_ivector(vect, v, 4);
	printf ("--> first 4-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 4, 4);
	clear_vector(v);
	calc_ivector(vctr, v, 4);
	printf ("--> second 4-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 8, 4);
	clear_vector(v);
	calc_ivector(vctr, v, 4);
	printf ("--> third 4-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 1, 6);
	clear_vector(v);
	calc_ivector(vctr, v, 6);
	printf ("--> first 6-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 6, 6);
	clear_vector(v);
	calc_ivector(vctr, v, 6);
	printf ("--> second 6-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 1, 8);
	clear_vector(v);
	calc_ivector(vctr, v, 8);
	printf ("--> first 8-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);

	load_vector(vect, vctr, 4, 8);
	clear_vector(v);
	calc_ivector(vctr, v, 8);
	printf ("--> second 8-vector:\t[%1d%1d%1d%1d%1d%1d]\n", v[1],v[2],v[3],v[4],v[5],v[6]);
	print $0;
}
