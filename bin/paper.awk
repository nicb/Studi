# paper linear time conversion for the timings provided in notes.dat
# current calculation conversion is based on 38.4 cm long staves which
# convert the whole piece (674 seconds) into 8 systems.
# The conversion rate is thus:
# 	0.4557863 cm/sec
BEGIN {
	dur = 674;
	len = 38.4;
	rate= (len * 8) / dur;
}
function pagenum(t)
{
	return ((t * rate) / len) + 1;
}
function convert(num)
{
	return (num * rate) % len;
}
/^[0-9 ]/ {
	printf("%7.2f\t%s\t%7.2f", convert($1), $2, $3*rate);
	for (i = 4; i <= NF; ++i)
		printf (" %s", $i);
	printf(" System n.%2d\n", pagenum($1));
}
/^=/ {
	print;
}
