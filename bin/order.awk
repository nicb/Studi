# order.awk:
# picks up the pieces in frags.dat and orders them on the basis of
# a triangular function discontinued on year 1881 (it inverses its sense)

BEGIN {
	FS=":";
	a_1 = .2670807;
	a_2 = -.4574468;
	b_2 = 43;
	c_1 = 1720;
	c_2 = 1881;
}
/^#/ {
# 	jump comments
}
$0 !~ /^#/ && $3 <= c_2 {
	y = ($3-c_1) * a_1;
	printf("%3d %4d %20s %s\n", y+1.5,$3,$1,$2);
}
$0 !~ /^#/ && $3 > c_2 {
	y = (($3-c_2) * a_2)+b_2;
	printf("%3d %4d %20s %s\n", y+1.5,$3,$1,$2);
}
