# paper linear time conversion to determine page positioning
# current calculation conversion is based on 38.4 cm long staves which
# convert the whole piece (674 seconds) into 8 systems.
# Pages are 30.4 cm long and the conversion is instead 5 cm/sec.
# The conversion rate is thus:
# 	0.4557863 cm/sec for the large schema
#	5 cm/sec for each small page
BEGIN {
	dur = 674;
	len = 38.4;
	page = 30.4;
	rate= (len * 8) / dur;
	page_rate = (page / 5) * rate;
}
function pagenum(pg)
{
	return int(((pg * page_rate) / len));
}
function convert(num)
{
	return (num * page_rate) % len;
}
{
	print "Small Page n.: " $1 \
		" - Page n.: " pagenum($1) " - cms: " convert($1);
}
