#
# $Id: rfitavg.awk 0.0 93/06/12 19:01:55 nicb Exp Locker: nicb $
#
# rhythm fitter averager - takes in input from rfit.awk debug_output=1
#	and builds an average based on best rhythmic divisors used
#	The algorithm is:
#		if divisor is a power of two
#			value = 1/divisor - 2/64
#		else
#			value = -(1/divisor)
#
function is_power_of_two(num,		sq, sqi)
{
	sq = sqrt(num);
	sqi = int(sq);

	return (sq == sqi) ? 1 : 0;
}
function calculate_value(den,		result)
{
	if (den)
		if (is_power_of_two(den))
			result = (1/den) - den_positive_offset;
		else
			result = -(1/den);

	return result;
}
BEGIN {
	FS=":"; OFS=":"; num_entries = 0;
	den_positive_offset = 2/64;
}
END {
	at_value_avg = (at_value / num_entries) * average_at;
	dur_value_avg = (dur_value / num_entries) * average_dur;

	print "tempo",tempo,"at avg",at_value_avg,"dur avg",dur_value_avg;
}
/^average/ {
	average_at = 1 / $2;
	average_dur = 1 / $4;
}
NF == 7 {
	++num_entries;
	at_value += calculate_value($2);
	dur_value += calculate_value($5);
}
#
# $Log:	rfitavg.awk $
# Revision 0.0  93/06/12  19:01:55  nicb
# Initial Revision
#
