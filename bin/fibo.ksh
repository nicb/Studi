#
# $Id: fibo.ksh,v 0.3 1997/03/31 09:08:40 nicb Exp $
#
# fibo.ksh: calculates fibonacci proportions on a given interval
#
# set -x
FIBO_OPTIONS="s:o:r:t"
FIBO_OPTIONS_DESC="[-t] [-s <start time>] [-o <offset time>] [-r <num of reiterations>]"
FIBO_START_TIME=0
FIBO_OFFSET_TIME=0
FIBO_REITERATIONS=6
FIBO_REVERSE=0
FIBO_GETOPT=${FIBO_GETOPT:-getoptprog}
function usage
{
	echo "Usage: $0 $FIBO_OPTIONS_DESC <end/interval time>" 1>&2;
}
#
#	set up options first
#
${FIBO_GETOPT} $FIBO_OPTIONS $* > /dev/null 2>&1
if [ "$?" != 0 ]
then
	usage
	exit -1
fi
set -- $(${FIBO_GETOPT} $FIBO_OPTIONS $*)
for i in $*
do
	case $i in
		-s) shift; FIBO_START_TIME=$1; shift;;
		-o) shift; FIBO_OFFSET_TIME=$1; shift;;
		-r) shift; FIBO_REITERATIONS=$1; shift;;
		-t) shift; FIBO_REVERSE=1;;
		--)  shift;  break;;
	esac
done
if [ $# -lt 1 ]
then
	usage
	exit -1
else
	FIBO_INTERVAL=$1
fi
echo $FIBO_INTERVAL | awk '\
BEGIN { \
	phi = 2/(sqrt(5) + 1); \
}\
{\
	ft = $1 - fibo_start_time;\
	print "**** end: " $1 ", start:" fibo_start_time " (" ft "), reit: " fibo_reiterations ", offset: ", fibo_offset_time;\
	if (fibo_reverse) \
		printf "%10.6g\n", (last = fibo_start_time); \
	else \
		printf "%10.6g\n", (last = (ft + fibo_start_time)); \
	for (i = 0; i < fibo_reiterations; ++i) \
	{\
		ft *= phi;\
		if (fibo_reverse) \
			out = $1 - ft - fibo_offset_time; \
		else \
			out = ft + fibo_offset_time + fibo_start_time; \
		printf "%10.6f\t(%-6.6f)\n", out, (out - last); \
		last = out; \
	}\
}' fibo_start_time=$FIBO_START_TIME fibo_offset_time=$FIBO_OFFSET_TIME \
   fibo_reiterations=$FIBO_REITERATIONS fibo_reverse=$FIBO_REVERSE
#
# $Log: fibo.ksh,v $
# Revision 0.3  1997/03/31 09:08:40  nicb
# adapted to linux
#
# Revision 0.2  1994/03/13 19:09:13  nicb
# better diff measurement
#
# Revision 0.1  94/02/28  16:34:14  nicb
# added reverse mode
# 
# Revision 0.0  94/02/28  14:35:33  nicb
# Initial Revision
# 
