# reconv.awk: awk script to process the formal scheme taken from Blue Monk
# This is now tuned on the actual excerpt (notes_n.dat)
# and it is used to generate notes.dat in combination with stretch.awk
# (command line: awk -f reconv.awk notes_n.dat | awk -f stretch.awk > notes.dat)
#
function convert(s,b)
{
	return (conv/b) * s * space;
}
function at_time(s,b)
{
	return (convert(s-1,b)+bar+offset) % page;
}
BEGIN {
	FS=":";
	tempo = 124; # mm
	page = 38.5; # cms
	space = 1 # cms/sec on the page

	conv = ((60/tempo)*4);
	_1_bar = conv * space;
	offset = 0;
	bar = 0;
}
/^#/ {
	print # kill comments
}
/^=/ {
	bar += _1_bar;
	print
}
/^[0-9]/ {
	printf("%7.4f %6s %7.4f\n", at_time($1,$2), $3, convert($4,$5));
}
