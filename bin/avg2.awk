# This averages the scarto medio as produced by fit_1.awk
BEGIN {
	tot_A = tot_B = tot_C = 0;
}
END {
	printf("Averages -\tA: %10.4f B: %10.4f C: %10.4f\n",
		tot_A/NR, tot_B/NR, tot_C/NR);
}
{
	dA = $5 - A; dB = $8 - B; dC = $11 - C;
	tot_A += dA; tot_B += dB; tot_C += dC;
	print;
}
