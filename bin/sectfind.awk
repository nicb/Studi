#
#
# sectfind.awk: finds all the sections in the notes_d.dat file
#
# $Id$
#
function section(start, end, count, name, comment)
{
	printf("$1 >= %g && $1 < %g {\t# %s\n\tif (!tempo_%d)\n\t\ttempo_%d = 60;\n\tif (!first_time_%d)\n\t{\n\t\tfirst_time_%d = 1;\n\t\tdo_header(\"%s\", tempo_%d);\n\t}\n\tdo_output($1,$2,$3,tempo_%d);\n\tnext;\n}\n", start, end, comment,
													count, count, count,
													count, name, count,
													count);
}
function diag_output(start, end, count)
{
	print start, end - 0.001, count;
}
BEGIN {
	last_at = 0.0000;
	last_composer = "";
}
/^=/ {
	next; # skip bar lines
}
$5 == "A" {
	last_composer = $6;
}
{
	if ($2 == last_at)
		next;
	else
	{
		if ($5 == "A")
		{
			++sect_counter;
			sect_name = sprintf("section %d", sect_counter);
			if (do_diag)
				diag_output(last_at, $2, sect_counter);
			else
				section(last_at, $2, sect_counter, sect_name, last_composer);
		}
		last_at = $2;
	}
}
