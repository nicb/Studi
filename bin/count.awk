BEGIN {
	A = 0;
	B = 0;
	C = 0;
}
END { print "Seed: " seed " Counted: " A, B, C " Total: " A+B+C; }
/^\n/ {}
/^=/ {
}
$4 == "A" { ++A; }
$4 == "B" { ++B; }
$4 == "C" { ++C; }
