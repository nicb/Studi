function int_pow(number,power)
{
	result = number;

	for (i = 1; i < power; ++i)
		result *= number;

	return result;
}
BEGIN {
	phi = (sqrt(5)+1)/2;
	cf = int_pow(phi,7);
}
/^\n/ {}
/^#/ { print; }
/^=/ {
	print;
}
/^ / || /^[0-9]/ {
	$1 *= cf; $3 *= cf;
	printf("%7.4f %6s %7.4f\n", $1, $2, $3);
}
