#
# twin: filters out a time window in a sorted time-tagged note file
#
#	format: at note dur
#
# $Id: twin.awk 0.1 93/07/11 15:07:52 nicb Exp $
BEGIN {
	if (!start)
		start = 0;
	if (!end)
		end = 9.3665;
}
$1 !~ /(\+|-)?[0-9]+\.?[0-9]*/ { # not a number
	print
}
$1 < start {
	next;
}
$1 > end {
	next;
}
{
	print;
}
#
# $Log:	twin.awk $
# Revision 0.1  93/07/11  15:07:52  nicb
# added pass-through of non-numeric records
# 
# Revision 0.0  93/07/11  14:52:46  nicb
# Initial Revision
# 
#
