#!/bin/ksh
#
# prowf.ksh - Usage:
#	prowf.ksh -$PROWF_OPTIONS_DESC
#
# $Id: prowf.ksh 1.5 94/01/04 22:08:49 nicb Exp $
# 
# set -x
ST_DIR="${ST_DIR:-/user/nicb/music/st}"
PROWF_ROW_DIR=${ST_DIR}/rows
PROWF_OPTIONS="vr:p:l:"
PROWF_OPTIONS_DESC="[-v] [-p <pattern>] [-r <row file>] [-l <num lines>]"
PROWF_PATTERN="a:a:a:a:a:a:a:a:a:a:a:a"
PROWF_ROW=row1-2nd
PROWF_AWK_FILE=${PROWF_AWK_FILE:-${ST_DIR}/bin/prowf.awk}
PROWF_EXE_FILE=${PROWF_EXE_FILE:-${ST_DIR}/bin/prowfx.exe}
PROWF_LINES=${PROWF_LINES:-20}
PROWF_VERSION_ONLY=0
function usage
{
	echo "Usage: $0 $PROWF_OPTIONS_DESC" 1>&2
}
function version
{
	echo '# $Id: prowf.ksh 1.5 94/01/04 22:08:49 nicb Exp $'
}
#
#	set up options first
#
getopt $PROWF_OPTIONS $* > /dev/null 2>&1
if [ "$?" != 0 ]
then
	usage
	exit -1
fi
set -- $(getopt $PROWF_OPTIONS $*)
for i in $*
do
	case $i in
		-p) shift; PROWF_PATTERN=$1; shift;;
		-r) shift; PROWF_ROW=$1; shift;;
		-l) shift; PROWF_LINES=$1; shift;;
		-v) PROWF_VERSION_ONLY=1; shift;;
		--)  shift;  break;;
	esac
done
if [ $PROWF_VERSION_ONLY = 1 ]
then
	version
fi
cat $PROWF_ROW_DIR/$PROWF_ROW | ${PROWF_EXE_FILE} $PROWF_PATTERN \
	| sort -t: +2n +3nr | head -$PROWF_LINES
#
# $Log:	prowf.ksh $
# Revision 1.5  94/01/04  22:08:49  nicb
# reversed sorting keys
# 
# Revision 1.4  94/01/04  16:02:33  nicb
# adapted to new executable prowfx.exe
# 
# Revision 1.3  93/12/18  20:19:54  nicb
# adapted to the 0.4 version of prowf.awk
# 
# Revision 1.2  93/12/18  17:05:07  nicb
# the version option now does not exit anymore
# 
# Revision 1.1  93/12/18  16:43:31  nicb
# corrected idiotic version bug
# 
# Revision 1.0  93/12/18  16:41:26  nicb
# Initial Revision
# 
