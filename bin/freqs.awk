#
# $Id$
# freqs.awk: tempered frequencies calculator
#
BEGIN {
	split("a:a#:b:c:c#:d:d#:e:f:f#:g:g#", note_names, ":");
	freq = 27.5;
	inc = 1.0594658;
	octave = -1;
	j = 1;
	while (freq < 6000)
	{
		if (note_names[j] == "c")
			++octave;
		printf("%3s%d:\t%4.4f\n", note_names[j], octave, freq);
		freq *= inc;
		if (note_names[j] == "g#")
			j = 1;
		else
			++j;
	}
}
