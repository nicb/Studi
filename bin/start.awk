function get_start()
{
	return (bar/$2) * ($1-1);
}
BEGIN {
	offset = 0;
	metro = 124;
	beat = 60/metro;
	bar = beat * 4;
}
/^=/ {
	offset += bar;
	print;
}
$0 !~ /^=/ {
	start = get_start($1,$2);
	printf ("%7.4f %-4s %s\n", start+offset, $3, $4);
}
