function calc_dur(the_dur)
{
	return bar / the_dur;
}
BEGIN {
	FS="+";
	metro = 124;
	beat = 60/metro;
	bar = beat * 4;
}
/^=/ {
	print;
}
$0 !~ /^=/ {
	dur = 0;
	for (i = 1; i <= NF; ++i)
		dur += calc_dur($i);

	print dur;
}
