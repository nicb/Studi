function m_12(i)
{
	if (i > 11)
		i -= 12;
	else if (i < 0)
		i += 12;

	return i;
}
/^-->/ {
	print;
	next;
}
{
	O[0] = m_12($1 + offset);
	O[1] = m_12($2 + offset);
	O[2] = m_12($3 + offset);
	O[3] = m_12($4 + offset);
	O[4] = m_12($5 + offset);
	O[5] = m_12($6 + offset);
	O[6] = m_12($7 + offset);
	O[7] = m_12($8 + offset);
	O[8] = m_12($9 + offset);
	O[9] = m_12($10 + offset);
	O[10] = m_12($11 + offset);
	O[11] = m_12($12 + offset);

	I[0] = m_12($1 + offset);
	I[1] = I[0] - (O[1] - O[0]);
	I[2] = I[1] - (O[2] - O[1]);
	I[3] = I[2] - (O[3] - O[2]);
	I[4] = I[3] - (O[4] - O[3]);
	I[5] = I[4] - (O[5] - O[4]);
	I[6] = I[5] - (O[6] - O[5]);
	I[7] = I[6] - (O[7] - O[6]);
	I[8] = I[7] - (O[8] - O[7]);
	I[9] = I[8] - (O[9] - O[8]);
	I[10] = I[9] - (O[10] - O[9]);
	I[11] = I[10] - (O[11] - O[10]);

	print "Original row: ";
	printf ("%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n",
		O[0],O[1],O[2],O[3],O[4],O[5],O[6],O[7],O[8],O[9],O[10],O[11]);

	print "Transpositions:\t\t\t\t|\tInversions:";
	for (i = 0; i < 12; ++i)
		printf ("O%-2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\t|\tI%-2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n",
		i, m_12(O[0]+i), m_12(O[1]+i), m_12(O[2]+i), m_12(O[3]+i),
		m_12(O[4]+i), m_12(O[5]+i), m_12(O[6]+i), m_12(O[7]+i),
		m_12(O[8]+i), m_12(O[9]+i), m_12(O[10]+i), m_12(O[11]+i),
		i, m_12(I[0]+i), m_12(I[1]+i), m_12(I[2]+i), m_12(I[3]+i),
		m_12(I[4]+i), m_12(I[5]+i), m_12(I[6]+i), m_12(I[7]+i),
		m_12(I[8]+i), m_12(I[9]+i), m_12(I[10]+i), m_12(I[11]+i));
	print "\n";
	print "Retrogrades:\t\t\t\t|\tRetrograde Inversions:";
	for (i = 0; i < 12; ++i)
		printf ("O%-2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\t|\tI%-2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n",
		i, m_12(O[11]+i), m_12(O[10]+i), m_12(O[9]+i), m_12(O[8]+i),
		m_12(O[7]+i), m_12(O[6]+i), m_12(O[5]+i), m_12(O[4]+i),
		m_12(O[3]+i), m_12(O[2]+i), m_12(O[1]+i), m_12(O[0]+i),
		i, m_12(I[11]+i), m_12(I[10]+i), m_12(I[9]+i), m_12(I[8]+i),
		m_12(I[7]+i), m_12(I[6]+i), m_12(I[5]+i), m_12(I[4]+i),
		m_12(I[3]+i), m_12(I[2]+i), m_12(I[1]+i), m_12(I[0]+i));
	print "\n------------------------------------------------------------------------------------------\f";
}
