#
# $Id: rowf.awk 0.3 94/01/16 19:00:14 nicb Exp $
#
# rowf.awk: row finder - Usage:
#	{cat | echo} row | awk -f rowf.awk pattern="<regexp>:...:<regexp>"
#
function print_row(row, name, i, p)
{
	printf("%-2s[%02d]P[%02d]: %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n",
		name, i, p,
		row[1],row[2],row[3],row[4],row[5],row[6],
		row[7],row[8],row[9],row[10],row[11],row[12]);
}
function compare_note(pattern_note, row_note) 
{
	return (match(row_note, pattern_note)) ? 0 : 1;
}
function permutate_row(row, return_row, p,	i)
{
	for (i = 1; i <= 12; i++)
	{
		return_row[i] = row[p];
		p = (p % 12) + 1;
	}
}
function compare_rows(pattern, row, num_fields, name, t,	prow, i, j)
{
	for (i = 1; i <= 12; ++i)
	{
		permutate_row(row, prow, i);
		for (j = 1; j <= num_fields; ++j)
			if (compare_note(pattern[j], prow[j]))
				break;
		if (j > num_fields)
			print_row(prow, name, t, i-1);
	}
}
function transpose_row(row, factor, return_row, 	i)
{
	for (i = 1; i <= 12; ++i)
		return_row[i] = (row[i] + factor) % 12;
}
function do_all_rows(pattern, row, num_fields, name,	trow, i)
{
	for (i = 0; i < 12; ++i)
	{
		transpose_row(row, i, trow);
		compare_rows(pattern, trow, num_fields, name, i);
	}
}
function search_row(_pattrn, orow, irow, rorow, rirow, 	__p, nf)
{
	nf = split(_pattrn, __p, ":");
	do_all_rows(__p, orow, nf, "O");
	do_all_rows(__p, irow, nf, "I");
	do_all_rows(__p, rorow, nf, "RO");
	do_all_rows(__p, rirow, nf, "RI");
}
function m_12(i)
{
	if (i > 11)
		i -= 12;
	else if (i < 0)
		i += 12;

	return i;
}
function invert_row(row, rrow)
{
	rrow[1] = row[1];
	rrow[2] = m_12(rrow[1] - (row[2] - row[1]));
	rrow[3] = m_12(rrow[2] - (row[3] - row[2]));
	rrow[4] = m_12(rrow[3] - (row[4] - row[3]));
	rrow[5] = m_12(rrow[4] - (row[5] - row[4]));
	rrow[6] = m_12(rrow[5] - (row[6] - row[5]));
	rrow[7] = m_12(rrow[6] - (row[7] - row[6]));
	rrow[8] = m_12(rrow[7] - (row[8] - row[7]));
	rrow[9] = m_12(rrow[8] - (row[9] - row[8]));
	rrow[10] = m_12(rrow[9] - (row[10] - row[9]));
	rrow[11] = m_12(rrow[10] - (row[11] - row[10]));
	rrow[12] = m_12(rrow[11] - (row[12] - row[11]));
}
function retrograde_row(row, rrow, 		i, j)
{
	j = 12;
	for (i = 1; i <= 12; ++i)
	{
		rrow[i] = row[j];
		--j;
	}
}
{
	split($0, _orw, " ");
	invert_row(_orw, _irw);
	retrograde_row(_orw, _rorw);
	retrograde_row(_irw, _rirw);

	search_row(pattern, _orw, _irw, _rorw, _rirw);
}
#
# $Log:	rowf.awk $
# Revision 0.3  94/01/16  19:00:14  nicb
# equalized output to prowf program
# 
# Revision 0.2  93/08/08  17:15:11  nicb
# better formatting of output rows
# 
# Revision 0.1  93/08/04  11:03:06  nicb
# removed x and now using reg exps only
# 
# Revision 0.0  93/08/04  10:25:28  nicb
# Initial Revision
# 
#
