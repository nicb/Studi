#!/bin/ksh
#
# rfit.ksh - Usage:
#	rfit.ksh -$RFIT_OPTIONS_DESC <start_time> <end_time>
#		<section_name> <tempo>
#
# $Id: rfit.ksh 0.5 93/10/30 13:11:30 nicb Exp $
# 
# set -x
RFIT_OPTIONS="dvp:f:a:"
RFIT_OPTIONS_DESC="[-dv] [-p <offset time>] [-f <input file>] [-a <allowed values>]"
RFIT_PHASE_OFFSET=0
RFIT_DATA_FILE=fsorted.dat
RFIT_DEBUG_OPTION=0
RFIT_VERSION_ONLY=0
function usage
{
	echo "Usage: $0 $RFIT_OPTIONS_DESC <start_time> <end_time> <section_name> <tempo>" 1>&2;
}
function version
{
	echo '# $Id: rfit.ksh 0.5 93/10/30 13:11:30 nicb Exp $'
	echo "xxx" | awk -f bin/rfit.awk version_only=1 $RFIT_ALLOWED_VALUES
	exit 0
}
#
#	set up options first
#
getopt $RFIT_OPTIONS $* > /dev/null 2>&1
if [ "$?" != 0 ]
then
	usage
	exit -1
fi
set -- $(getopt $RFIT_OPTIONS $*)
for i in $*
do
	case $i in
		-p) shift; RFIT_PHASE_OFFSET=$1; shift;;
		-f) shift; RFIT_DATA_FILE=$1; shift;;
		-v) RFIT_VERSION_ONLY=1; shift;;
		-d) RFIT_DEBUG_OPTION=1; shift;;
		-a) shift; RFIT_ALLOWED_VALUES="allowed_vals_string=$1"; shift;;
		--)  shift;  break;;
	esac
done
if [ $RFIT_VERSION_ONLY = 1 ]
then
	version
fi
if [ $# -lt 4 ]
then
	usage
	exit -1
fi
START_TIME=$1
END_TIME=$2
SECTION_NAME=$3
(awk -f bin/twin.awk start=$START_TIME end=$END_TIME $RFIT_DATA_FILE | \
	awk -f bin/rfit.awk ph=${RFIT_PHASE_OFFSET} no_normal_output=0 \
		debug_output=$RFIT_DEBUG_OPTION \
		section_name=${SECTION_NAME} tempo_x=$4 \
		${RFIT_ALLOWED_VALUES} \
		debug_tempo=0)
#
# $Log:	rfit.ksh $
# Revision 0.5  93/10/30  13:11:30  nicb
# added -a option (allowed divisors)
# 
# Revision 0.4  93/07/27  22:26:32  nicb
# simplified, single section rfit.awk pass
# 
# Revision 0.3  93/07/14  10:37:38  nicb
# added -d option (debug_output)
# 
# Revision 0.2  93/07/11  15:10:37  nicb
# added -f file option
# 
# Revision 0.1  93/07/10  10:29:39  nicb
# added phase offset function
# added version option
# 
# Revision 0.0  93/07/10  08:42:38  nicb
# Initial revision
# 
#
