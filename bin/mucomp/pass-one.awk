# This is the first pass awk script driven by all shell scripts
# in single directories
#
# $Id: pass-one.awk,v 1.1 1994/11/27 19:02:29 nicb Exp $
# (log at end of file)
#
function internal_find(note)
{
	for (i = 0; i < ik; ++i)
		if (note == keys_f[i,0] || note == keys_s[i,0] || note == keys_f[i,1] || note == keys_s[i,1])
			return i;

	print "note " note " not found!";
	exit -1;
}
function internal_wrap(_n)
{
	if (_n < 0)
		return _n + 88;
	else if (_n > 88)
		return _n - 88;
	else
		return _n;
}
function transpose(n, t)
{
	if (n == "rest")
		return n;
	nt = internal_find(n) + t;
	nt = internal_wrap(nt);
	if (transpose_pref == "flat")
		return keys_f[nt, 0];
	else if (transpose_pref == "sharp")
		return keys_s[nt, 0];
	else
	{
	   if (match(n,/[a-g]f[0-9]/))
			return keys_f[nt, 0];
	   else
	        return keys_s[nt, 0];
	}
}
function dur(s,b) # reduce everything to quarter note fractions
{
	return (conv/b) * s;
}
function at_time(s,b)
{
	return (dur(s-1,b)+bar);
}
NR == 1 { # this can't be put in the BEGIN section!
	transpose_factor = tf; # where should it be transposed

	conv = 4;
	bar_to_quarters_ratio = btqr; # bar to 4/4 bar ratio
	_1_bar = conv * btqr;
	bar = 0;
}
BEGIN {
	FS=":";
	ALREADY_CHANGED = 1;
	UNCHANGED = 0;
	ratio_flag = UNCHANGED;

#	keyboard representation (two dimensions to allow double sharps etc.)

	ik = 0; jk = 0;
	keys_f[ik++,0] = "a-1";	keys_s[jk++,0] = "a-1";
	keys_f[ik++,0] = "bf-1";	keys_s[jk++,0] = "a#-1";
	keys_f[ik++,0] = "b-1";	keys_s[jk++,0] = "b-1";
	keys_f[ik++,0] = "c0";	keys_s[jk++,0] = "c0";
	keys_f[ik++,0] = "df0";	keys_s[jk++,0] = "c#0";
	keys_f[ik++,0] = "d0";	keys_s[jk++,0] = "d0";
	keys_f[ik++,0] = "ef0";	keys_s[jk++,0] = "d#0";
	keys_f[ik++,0] = "e0";	keys_s[jk++,0] = "e0";
	keys_f[ik++,0] = "f0";	keys_s[jk++,0] = "f0";
	keys_f[ik++,0] = "gf0";	keys_s[jk++,0] = "f#0";
	keys_f[ik++,0] = "g0";	keys_s[jk++,0] = "g0";
	keys_f[ik++,0] = "af0";	keys_s[jk++,0] = "g#0";
	keys_f[ik++,0] = "a0";	keys_s[jk++,0] = "a0";
	keys_f[ik++,0] = "bf0";	keys_s[jk++,0] = "a#0";
	keys_f[ik++,0] = "b0";	keys_s[jk++,0] = "b0";
	keys_f[ik++,0] = "c1";	keys_s[jk++,0] = "c1";
	keys_f[ik++,0] = "df1";	keys_s[jk++,0] = "c#1";
	keys_f[ik++,0] = "d1";	keys_s[jk++,0] = "d1";
	keys_f[ik++,0] = "ef1";	keys_s[jk++,0] = "d#1";
	keys_f[ik++,0] = "e1";	keys_s[jk++,0] = "e1";
	keys_f[ik++,0] = "f1";	keys_s[jk++,0] = "f1";
	keys_f[ik++,0] = "gf1";	keys_s[jk++,0] = "f#1";
	keys_f[ik++,0] = "g1";	keys_s[jk++,0] = "g1";
	keys_f[ik++,0] = "af1";	keys_s[jk++,0] = "g#1";
	keys_f[ik++,0] = "a1";	keys_s[jk++,0] = "a1";
	keys_f[ik++,0] = "bf1";	keys_s[jk++,0] = "a#1";
	keys_f[ik++,0] = "b1";	keys_s[jk++,0] = "b1";
	keys_f[ik++,0] = "c2";	keys_s[jk++,0] = "c2";
	keys_f[ik++,0] = "df2";	keys_s[jk++,0] = "c#2";
	keys_f[ik++,0] = "d2";	keys_s[jk++,0] = "d2";
	keys_f[ik++,0] = "ef2";	keys_s[jk++,0] = "d#2";
	keys_f[ik++,0] = "e2";	keys_s[jk++,0] = "e2";
	keys_f[ik++,0] = "f2";	keys_s[jk++,0] = "f2";
	keys_f[ik++,0] = "gf2";	keys_s[jk++,0] = "f#2";
	keys_f[ik++,0] = "g2";	keys_s[jk++,0] = "g2";
	keys_f[ik++,0] = "af2";	keys_s[jk++,0] = "g#2";
	keys_f[ik++,0] = "a2";	keys_s[jk++,0] = "a2";
	keys_f[ik++,0] = "bf2";	keys_s[jk++,0] = "a#2";
	keys_f[ik++,0] = "b2";	keys_s[jk++,0] = "b2";
	keys_f[ik++,0] = "c3";	keys_s[jk++,0] = "c3";
	keys_f[ik++,0] = "df3";	keys_s[jk++,0] = "c#3";
	keys_f[ik++,0] = "d3";	keys_s[jk++,0] = "d3";
	keys_f[ik++,0] = "ef3";	keys_s[jk++,0] = "d#3";
	keys_f[ik++,0] = "e3";	keys_s[jk++,0] = "e3";
	keys_f[ik++,0] = "f3";	keys_s[jk++,0] = "f3";
	keys_f[ik++,0] = "gf3";	keys_s[jk++,0] = "f#3";
	keys_f[ik++,0] = "g3";	keys_s[jk++,0] = "g3";
	keys_f[ik++,0] = "af3";	keys_s[jk++,0] = "g#3";
	keys_f[ik++,0] = "a3";	keys_s[jk++,0] = "a3";
	keys_f[ik++,0] = "bf3";	keys_s[jk++,0] = "a#3";
	keys_f[ik++,0] = "b3";	keys_s[jk++,0] = "b3";
	keys_f[ik++,0] = "c4";	keys_s[jk++,0] = "c4";
	keys_f[ik++,0] = "df4";	keys_s[jk++,0] = "c#4";
	keys_f[ik++,0] = "d4";	keys_s[jk++,0] = "d4";
	keys_f[ik++,0] = "ef4";	keys_s[jk++,0] = "d#4";
	keys_f[ik++,0] = "e4";	keys_s[jk++,0] = "e4";
	keys_f[ik++,0] = "f4";	keys_s[jk++,0] = "f4";
	keys_f[ik++,0] = "gf4";	keys_s[jk++,0] = "f#4";
	keys_f[ik++,0] = "g4";	keys_s[jk++,0] = "g4";
	keys_f[ik++,0] = "af4";	keys_s[jk++,0] = "g#4";
	keys_f[ik++,0] = "a4";	keys_s[jk++,0] = "a4";
	keys_f[ik++,0] = "bf4";	keys_s[jk++,0] = "a#4";
	keys_f[ik++,0] = "b4";	keys_s[jk++,0] = "b4";
	keys_f[ik++,0] = "c5";	keys_s[jk++,0] = "c5";
	keys_f[ik++,0] = "df5";	keys_s[jk++,0] = "c#5";
	keys_f[ik++,0] = "d5";	keys_s[jk++,0] = "d5";
	keys_f[ik++,0] = "ef5";	keys_s[jk++,0] = "d#5";
	keys_f[ik++,0] = "e5";	keys_s[jk++,0] = "e5";
	keys_f[ik++,0] = "f5";	keys_s[jk++,0] = "f5";
	keys_f[ik++,0] = "gf5";	keys_s[jk++,0] = "f#5";
	keys_f[ik++,0] = "g5";	keys_s[jk++,0] = "g5";
	keys_f[ik++,0] = "af5";	keys_s[jk++,0] = "g#5";
	keys_f[ik++,0] = "a5";	keys_s[jk++,0] = "a5";
	keys_f[ik++,0] = "bf5";	keys_s[jk++,0] = "a#5";
	keys_f[ik++,0] = "b5";	keys_s[jk++,0] = "b5";
	keys_f[ik++,0] = "c6";	keys_s[jk++,0] = "c6";
	keys_f[ik++,0] = "df6";	keys_s[jk++,0] = "c#6";
	keys_f[ik++,0] = "d6";	keys_s[jk++,0] = "d6";
	keys_f[ik++,0] = "ef6";	keys_s[jk++,0] = "d#6";
	keys_f[ik++,0] = "e6";	keys_s[jk++,0] = "e6";
	keys_f[ik++,0] = "f6";	keys_s[jk++,0] = "f6";
	keys_f[ik++,0] = "gf6";	keys_s[jk++,0] = "f#6";
	keys_f[ik++,0] = "g6";	keys_s[jk++,0] = "g6";
	keys_f[ik++,0] = "af6";	keys_s[jk++,0] = "g#6";
	keys_f[ik++,0] = "a6";	keys_s[jk++,0] = "a6";
	keys_f[ik++,0] = "bf6";	keys_s[jk++,0] = "a#6";
	keys_f[ik++,0] = "b6";	keys_s[jk++,0] = "b6";
	keys_f[ik++,0] = "c7";	keys_s[jk++,0] = "c7";

	ik = 0; jk = 0; # second dimension

	keys_f[ik++,1] = "bff-1"; keys_s[jk++,1] = "g##-1";
	keys_f[ik++,1] = "cff0"; keys_s[jk++,1] = "a#-1";
	keys_f[ik++,1] = "cf0"; keys_s[jk++,1] = "a##-1";
	keys_f[ik++,1] = "dff0"; keys_s[jk++,1] = "b#-1";
	keys_f[ik++,1] = "df0"; keys_s[jk++,1] = "c#0";
	keys_f[ik++,1] = "eff0"; keys_s[jk++,1] = "c##0";
	keys_f[ik++,1] = "ef0"; keys_s[jk++,1] = "d#0";
	keys_f[ik++,1] = "ff0"; keys_s[jk++,1] = "d##0";
	keys_f[ik++,1] = "gff0"; keys_s[jk++,1] = "e#0";
	keys_f[ik++,1] = "gf0"; keys_s[jk++,1] = "f#0";
	keys_f[ik++,1] = "aff0"; keys_s[jk++,1] = "f##0";
	keys_f[ik++,1] = "af0"; keys_s[jk++,1] = "g#0";
	keys_f[ik++,1] = "bff0"; keys_s[jk++,1] = "g##0";
	keys_f[ik++,1] = "cff1"; keys_s[jk++,1] = "a#0";
	keys_f[ik++,1] = "cf1"; keys_s[jk++,1] = "a##0";
	keys_f[ik++,1] = "dff1"; keys_s[jk++,1] = "b#0";
	keys_f[ik++,1] = "df1"; keys_s[jk++,1] = "c#1";
	keys_f[ik++,1] = "eff1"; keys_s[jk++,1] = "c##1";
	keys_f[ik++,1] = "ef1"; keys_s[jk++,1] = "d#1";
	keys_f[ik++,1] = "ff1"; keys_s[jk++,1] = "d##1";
	keys_f[ik++,1] = "gff1"; keys_s[jk++,1] = "e#1";
	keys_f[ik++,1] = "gf1"; keys_s[jk++,1] = "f#1";
	keys_f[ik++,1] = "aff1"; keys_s[jk++,1] = "f##1";
	keys_f[ik++,1] = "af1"; keys_s[jk++,1] = "g#1";
	keys_f[ik++,1] = "bff1"; keys_s[jk++,1] = "g##1";
	keys_f[ik++,1] = "cff2"; keys_s[jk++,1] = "a#1";
	keys_f[ik++,1] = "cf2"; keys_s[jk++,1] = "a##1";
	keys_f[ik++,1] = "dff2"; keys_s[jk++,1] = "b#1";
	keys_f[ik++,1] = "df2"; keys_s[jk++,1] = "c#2";
	keys_f[ik++,1] = "eff2"; keys_s[jk++,1] = "c##2";
	keys_f[ik++,1] = "ef2"; keys_s[jk++,1] = "d#2";
	keys_f[ik++,1] = "ff2"; keys_s[jk++,1] = "d##2";
	keys_f[ik++,1] = "gff2"; keys_s[jk++,1] = "e#2";
	keys_f[ik++,1] = "gf2"; keys_s[jk++,1] = "f#2";
	keys_f[ik++,1] = "aff2"; keys_s[jk++,1] = "f##2";
	keys_f[ik++,1] = "af2"; keys_s[jk++,1] = "g#2";
	keys_f[ik++,1] = "bff2"; keys_s[jk++,1] = "g##2";
	keys_f[ik++,1] = "cff3"; keys_s[jk++,1] = "a#2";
	keys_f[ik++,1] = "cf3"; keys_s[jk++,1] = "a##2";
	keys_f[ik++,1] = "dff3"; keys_s[jk++,1] = "b#2";
	keys_f[ik++,1] = "df3"; keys_s[jk++,1] = "c#3";
	keys_f[ik++,1] = "eff3"; keys_s[jk++,1] = "c##3";
	keys_f[ik++,1] = "ef3"; keys_s[jk++,1] = "d#3";
	keys_f[ik++,1] = "ff3"; keys_s[jk++,1] = "d##3";
	keys_f[ik++,1] = "gff3"; keys_s[jk++,1] = "e#3";
	keys_f[ik++,1] = "gf3"; keys_s[jk++,1] = "f#3";
	keys_f[ik++,1] = "aff3"; keys_s[jk++,1] = "f##3";
	keys_f[ik++,1] = "af3"; keys_s[jk++,1] = "g#3";
	keys_f[ik++,1] = "bff3"; keys_s[jk++,1] = "g##3";
	keys_f[ik++,1] = "cff4"; keys_s[jk++,1] = "a#3";
	keys_f[ik++,1] = "cf4"; keys_s[jk++,1] = "a##3";
	keys_f[ik++,1] = "dff4"; keys_s[jk++,1] = "b#3";
	keys_f[ik++,1] = "df4"; keys_s[jk++,1] = "c#4";
	keys_f[ik++,1] = "eff4"; keys_s[jk++,1] = "c##4";
	keys_f[ik++,1] = "ef4"; keys_s[jk++,1] = "d#4";
	keys_f[ik++,1] = "ff4"; keys_s[jk++,1] = "d##4";
	keys_f[ik++,1] = "gff4"; keys_s[jk++,1] = "e#4";
	keys_f[ik++,1] = "gf4"; keys_s[jk++,1] = "f#4";
	keys_f[ik++,1] = "aff4"; keys_s[jk++,1] = "f##4";
	keys_f[ik++,1] = "af4"; keys_s[jk++,1] = "g#4";
	keys_f[ik++,1] = "bff4"; keys_s[jk++,1] = "g##4";
	keys_f[ik++,1] = "cff5"; keys_s[jk++,1] = "a#4";
	keys_f[ik++,1] = "cf5"; keys_s[jk++,1] = "a##4";
	keys_f[ik++,1] = "dff5"; keys_s[jk++,1] = "b#4";
	keys_f[ik++,1] = "df5"; keys_s[jk++,1] = "c#5";
	keys_f[ik++,1] = "eff5"; keys_s[jk++,1] = "c##5";
	keys_f[ik++,1] = "ef5"; keys_s[jk++,1] = "d#5";
	keys_f[ik++,1] = "ff5"; keys_s[jk++,1] = "d##5";
	keys_f[ik++,1] = "gff5"; keys_s[jk++,1] = "e#5";
	keys_f[ik++,1] = "gf5"; keys_s[jk++,1] = "f#5";
	keys_f[ik++,1] = "aff5"; keys_s[jk++,1] = "f##5";
	keys_f[ik++,1] = "af5"; keys_s[jk++,1] = "g#5";
	keys_f[ik++,1] = "bff5"; keys_s[jk++,1] = "g##5";
	keys_f[ik++,1] = "cff6"; keys_s[jk++,1] = "a#5";
	keys_f[ik++,1] = "cf6"; keys_s[jk++,1] = "a##5";
	keys_f[ik++,1] = "dff6"; keys_s[jk++,1] = "b#5";
	keys_f[ik++,1] = "df6"; keys_s[jk++,1] = "c#6";
	keys_f[ik++,1] = "eff6"; keys_s[jk++,1] = "c##6";
	keys_f[ik++,1] = "ef6"; keys_s[jk++,1] = "d#6";
	keys_f[ik++,1] = "ff6"; keys_s[jk++,1] = "d##6";
	keys_f[ik++,1] = "gff6"; keys_s[jk++,1] = "e#6";
	keys_f[ik++,1] = "gf6"; keys_s[jk++,1] = "f#6";
	keys_f[ik++,1] = "aff6"; keys_s[jk++,1] = "f##6";
	keys_f[ik++,1] = "af6"; keys_s[jk++,1] = "g#6";
	keys_f[ik++,1] = "bff6"; keys_s[jk++,1] = "g##6";
	keys_f[ik++,1] = "cff7"; keys_s[jk++,1] = "a#6";
	keys_f[ik++,1] = "cf7"; keys_s[jk++,1] = "a##6";
	keys_f[ik++,1] = "dff7"; keys_s[jk++,1] = "b#6";

}
/^#/ {
	print # do not kill comments (at least for now)
	next;
}
/^=/ {
	ratio_flag = UNCHANGED;
	bar += _1_bar; # add another bar
	printf("%7.4f:===\n",bar); # print the ===
	next;
}
$3 == "ratio" {
	if (ratio_flag == ALREADY_CHANGED)
	{
		message = "cannot change time signature twice in a bar!";
		print message | "cat 1>&2";
		next;
	}
	btqr = $4/$5; # modify bar to 4/4 ratio
	_1_bar = conv * btqr; # modify the _1_bar variable
	ratio_flag = ALREADY_CHANGED;
	next;
}
$3 == "tempo" {
	printf("%7.4f:tempo:%d:%s\n", at_time($1,$2), $4, $5);
	next;
}
/^[0-9]/ {
	actual_note = transpose($3,transpose_factor); 
	printf("%7.4f:%6s:->:%6s:%7.4f:%s\n", at_time($1,$2), $3,
		actual_note, dur($4,$5), $6);
}
#
# $Log: pass-one.awk,v $
# Revision 1.1  1994/11/27  19:02:29  nicb
# Definitive version for Studi Terreni
#
#
