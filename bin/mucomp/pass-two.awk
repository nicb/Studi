# This is the pass-two awk script driven by all shell scripts
# in single directories
#
# $Id: pass-two.awk,v 1.1 1994/11/27 19:13:48 nicb Exp $
#
BEGIN {
	FS=":";
	last_at_time = 0;
	bar = 0;
	page = 30.4; # cms
	space = 5 # cms/sec on the page
}
function set_tempo(tm, at)
{
	_at_time(at);
	tempo = tm;
	conv = 60/tempo; # remap tempo
}
function _dur(s)
{
	return conv * s;
}
function dur(s)
{
	return _dur(s) * space;
}
function _at_time(s)
{
	intv = s - last_at_time;
	offset += _dur(intv);
	last_at_time = s;
	return offset;
}
function at_time(s)
{
	return (_at_time(s) * space) % page;
}
NR == 1 { # this can't be put in the BEGIN section!
# t: tempo argument from outside
# ot: offset time argument from outside
# ob: offset beats argument from outside
	set_tempo(t, 0);
	offset = ot - (conv*ob); # initial offset
}
/^#/ {
	print # do not kill comments (at least for now)
	next;
}
/\===/ {
	print "==="; # print the ===
	next;
}
$2 == "tempo" {
	set_tempo($3, $1);
	next;
}
$1 ~ /[0-9.]*/ {
	printf("%7.4f %6s -> %6s %7.4f %s\n", at_time($1), $2, $4,
		dur($5), $6);
	next;
}
#
# $Log: pass-two.awk,v $
# Revision 1.1  1994/11/27 19:13:48  nicb
# corrected bug in set_tempo()
#
#
