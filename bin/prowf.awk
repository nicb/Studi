#
# $Id: prowf.awk 0.4 93/12/18 20:18:41 nicb Exp $
#
# prowf.awk: permutating best row finder - Usage:
#	{cat | echo} row | awk -f prowf.awk pattern="<regexp>:...:<regexp>"
#	(where the pattern should be the best possible one - prowf.awk will
#	 attempt to find the best fitting
#
function abs(n)
{
	return (n < 0) ? -n : n;
}
function delete_array(bf, nf,
	i)
{
	for (i = 0; i < nf; ++i)
		delete bf[i];
}
function copy_array(source, dest, n_elements,
	i)
{
	for (i = 0; i < n_elements; ++i)
		dest[i] = source[i];
}
function remove_from_saved_chord(result,
	i)
{
	for (i = 1; i <= last_chord[0]; ++i)
		if (result == last_chord[i])
		{
			last_chord[i] = "D";
			break;
		}
}
function save_chord(chord)
{
	delete_array(last_chord, last_chord[0] + 1);
	copy_array(chord, last_chord, chord[0] + 1);
}
function deal_with_chord(result)
{
	copy_array(last_chord, result, last_chord[0] + 1);
	return last_chord[0];
}
function get_notes_from_regexp(pattern, result,
	_tr, __tr, _ma, i, j, k, _nf)
{
	result[0] = 0;
	if (pattern ~ /[cC]/)
		_nf = deal_with_chord(result);
	else
	{
		__tr = pattern;
		gsub(/[\^\$]|(\[)|(\])/,"",__tr);
		_nf = split(__tr,_tr,"|");
		for (i = 1; i <= _nf; ++i)
		{
			gsub(/[()]/,"", _tr[i]);
			if (int(_tr[i]) != 10 && int(_tr[i]) != 11)
			{
				_ma[i,0] = length(_tr[i]);
				for (j = 1; j <= _ma[i,0]; ++j)
				{
					_ma[i,j] = substr(_tr[i], j, 1);
					result[0] += 1;
				}
			}
			else
			{
				_ma[i,0] = 1;
				_ma[i,1] = _tr[i];
				result[0] += 1;
			}
		}
		k = 1;
		for (i = 1; i <= result[0]; ++i)
		{
			for (j = 1; j <= _ma[i,0]; ++j)
				result[k++] = _ma[i,j];
		}
		save_chord(result);
		delete_array(_tr, _nf);
		delete_array(_ma, _nf);
	}
}
function calculate_note_distance(target, actual,
	result)
{
	result = abs(int(target) - int(actual));
	return (result > 6) ? (12-result) : result;
}
function note_distance(pn, rn,
	returned, i, result, note_to_be_removed, tmp_result)
{
	result = 10000;
	get_notes_from_regexp(pn, returned);
	for (i = 1; i <= returned[0]; ++i)
		if (returned[i] == "D")
			continue;
		else
		{
			if ((tmp_result = calculate_note_distance(rn,
				returned[i])) <= result)
			{
				note_to_be_removed = returned[i];
				result = tmp_result;
			}
		}
	
	if (result == 10000)
		result = 0;
	else
		remove_from_saved_chord(note_to_be_removed);

	return result;
}
function match_note(pattern_note, row_note)
{
	return (match(row_note, pattern_note) != 0) ? 0 : note_distance(pattern_note, row_note);
}
function row_distance(pattern_row, found_row,
	i, d)
{
	for (i = 1; i <= 12; ++i)
		d += match_note(pattern_row[i], found_row[i]);

	return d;
}
function print_row(row, name, i, p, pattern, fits)
{
	return sprintf("%-2s[%02d]P[%02d]: %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d  (dist: %3s) (fits: %d)",
		name, i, p,
		row[1],row[2],row[3],row[4],row[5],row[6],
		row[7],row[8],row[9],row[10],row[11],row[12],
		row_distance(pattern, row), fits);
}
function compare_note(pattern_note, row_note) 
{
	return (match(row_note, pattern_note) != 0) ? 0 : 1;
}
function permutate_row(row, return_row, p,	i)
{
	for (i = 1; i <= 12; i++)
	{
		return_row[i] = row[p];
		p = (p % 12) + 1;
	}
}
function compare_rows(pattern, row, num_fields, name, t,
	rval, prow, i, j)
{
	for (i = 1; i <= 12; ++i)
	{
		rval = 0;
		permutate_row(row, prow, i);
		for (j = 1; j <= num_fields; ++j)
			if (!compare_note(pattern[j], prow[j]))
				++rval;
		printf("%s\n", print_row(prow,name,t,i,pattern,rval));
	}
}
function transpose_row(row, factor, return_row, 	i)
{
	for (i = 1; i <= 12; ++i)
		return_row[i] = (row[i] + factor) % 12;
}
function do_all_rows(pattern, row, num_fields, name, 
	trow, i, nfits)
{
	for (i = 0; i < 12; ++i)
	{
		transpose_row(row, i, trow);
		compare_rows(pattern, trow, num_fields, name, i)
	}
}
function search_row(_pattrn, orow, irow, rorow, rirow,
	__p, nf)
{
	nf = split(_pattrn, __p, ":");
	do_all_rows(__p, orow, nf, "O");
	do_all_rows(__p, irow, nf, "I");
	do_all_rows(__p, rorow, nf, "R");
	do_all_rows(__p, rirow, nf, "RI");
}
function m_12(i)
{
	if (i > 11)
		i -= 12;
	else if (i < 0)
		i += 12;

	return i;
}
function invert_row(row, rrow)
{
	rrow[1] = row[1];
	rrow[2] = m_12(rrow[1] - (row[2] - row[1]));
	rrow[3] = m_12(rrow[2] - (row[3] - row[2]));
	rrow[4] = m_12(rrow[3] - (row[4] - row[3]));
	rrow[5] = m_12(rrow[4] - (row[5] - row[4]));
	rrow[6] = m_12(rrow[5] - (row[6] - row[5]));
	rrow[7] = m_12(rrow[6] - (row[7] - row[6]));
	rrow[8] = m_12(rrow[7] - (row[8] - row[7]));
	rrow[9] = m_12(rrow[8] - (row[9] - row[8]));
	rrow[10] = m_12(rrow[9] - (row[10] - row[9]));
	rrow[11] = m_12(rrow[10] - (row[11] - row[10]));
	rrow[12] = m_12(rrow[11] - (row[12] - row[11]));
}
function retrograde_row(row, rrow, 		i, j)
{
	j = 12;
	for (i = 1; i <= 12; ++i)
	{
		rrow[i] = row[j];
		--j;
	}
}
{
	split($0, _orw, " ");
	invert_row(_orw, _irw);
	retrograde_row(_orw, _rorw);
	retrograde_row(_irw, _rirw);

	search_row(pattern, _orw, _irw, _rorw, _rirw);
}
#
# $Log:	prowf.awk $
# Revision 0.4  93/12/18  20:18:41  nicb
# stripped version (does not calculate best fits)
# 
# Revision 0.3  93/09/17  13:29:54  nicb
# added chord pattern indicator (first attempt, not perfect yet)
# 
# Revision 0.2  93/09/07  10:11:37  nicb
# added modulo distance counting
# 
# Revision 0.1  93/09/02  13:43:11  nicb
# added distance counting
# 
# Revision 0.0  93/08/30  10:51:42  nicb
# Initial Revision (working version)
# 
#
