function percent(a,t)
{
	return (a/t)*100;
}
BEGIN {
	A_area = B_area = C_area = 0;
}
END {
	tot_area = A_area + B_area + C_area;

	printf ("Seed %6d - A: %8.4f - B: %8.4f - C: %8.4f\n", seed,
		percent(A_area,tot_area), percent(B_area,tot_area),
		percent(C_area,tot_area));
}
/^\n/ {}
/^=/ {
}
/^ / || /^[0-9]/ && $4 == "A" {
	A_area += $3;
}
/^ / || /^[0-9]/ && $4 == "B" {
	B_area += $3;
}
/^ / || /^[0-9]/ && $4 == "C" {
	C_area += $3;
}
