BEGIN {
	refcount = 0;
	a = 0; b = 0; c = 0;
}
{
	++refcount;
	a += $4; b += $5; c += $6;
}
END {
	print "Average - A: " a/refcount " B: " b/refcount " C: " c/refcount;
}
