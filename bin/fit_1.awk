# this calculates the fitting of the areas as calculated by area.awk
function abs(num)
{
	if (num < 0)
		num = -num;

	return num;
}
BEGIN {
	A = 40.36; B = 29.35; C = 30.27;
}
{
	dA = $5 - A; dB = $8 - B; dC = $11 - C;
	printf ("Seed: %6d - A: %8.4f - B: %8.4f - C: %8.4f - tot: %8.4f\n",
		seed, dA, dB, dC, abs(dA)+abs(dB)+abs(dC));
}
