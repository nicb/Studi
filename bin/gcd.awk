#
# $Id$
# gcd.awk: greatest common divisor calculator
#	Usage: awk -f bin/gcd.awk <num1> <num2>
#
function usage()
{
	return sprintf("awk -f %s <num1> <num2>", ARGV[0]);
}
function max(num1, num2)
{
	return (num1 > num2) ? num1 : num2;
}
function min(num1, num2)
{
	return (num1 < num2) ? num1 : num2;
}
function round(n)
{
	return int(n + 0.5);
}
function gcd(a, b,
	first, second, remainder, rounded_a, rounded_b)
{
	rounded_a = round(a);
	rounded_b = round(b);
	first = max(a, b);
	second = min(a, b);
	while ((remainder = first % second))
	{
		first = second;
		second = remainder;
	}

	return second;
}
BEGIN {
	if (ARGC < 3)
	{
		print usage();
		exit(-1);
	}
	a = ARGV[1];
	b = ARGV[2];
	print gcd(a, b);
}
#
# $Log$
#
