# this is done to distribute the fragments along notes_d.dat
BEGIN {
	i = 0;
	compo[0] = "Bach-Busoni"; frag[0] = "Fantasia Cromatica e Fuga";
	compo[1] = "Cage"; frag[1] = "Etudes Australes";
	compo[2] = "Monk"; frag[2] = "Blue Monk";
	compo[3] = "Scarlatti"; frag[3] = "Toccata L.422 vol.9";
	compo[4] = "Evans"; frag[4] = "Summertime";
	compo[5] = "Galuppi"; frag[5] = "Sonata in do maggiore";
	compo[6] = "Boulez"; frag[6] = "Troisieme Sonate";
	compo[7] = "Stockhausen"; frag[7] = "Klavierstucke IX";
	compo[8] = "Powell"; frag[8] = "Dance of the Infidels";
	compo[9] = "Prokofev"; frag[9] = "Sonata n.7";
	compo[10] = "Stravinskij"; frag[10] = "Tango";
	compo[11] = "CPE Bach"; frag[11] = "Sonata Sib maggiore W59/3";
	compo[12] = "Mozart"; frag[12] = "Fantasia K.475";
	compo[13] = "Clementi"; frag[13] = "Sonatina n.1";
	compo[14] = "Webern"; frag[14] = "op.27 n.2";
	compo[15] = "Gershwin"; frag[15] = "Jazz Preludes";
	compo[16] = "Bartok"; frag[16] = "Sonate";
	compo[17] = "Beethoven"; frag[17] = "Sonata op.53 Waldstein";
	compo[18] = "Schonberg"; frag[18] = "Suite op.25";
	compo[19] = "Debussy"; frag[19] = "Etude Pour les degres chromatiques";
	compo[20] = "Jelly Roll Morton"; frag[20] = "Miserere from il Trovatore";
	compo[21] = "Granados"; frag[21] = "Goyescas";
	compo[22] = "Skriabin"; frag[22] = "Sonata Black Mass";
	compo[23] = "Schubert"; frag[23] = "Impromptu op.142 n.3";
	compo[24] = "Cramer"; frag[24] = "60 studi n.3";
	compo[25] = "Ravel"; frag[25] = "Gaspard de la Nuit-Ondine";
	compo[26] = "Debussy"; frag[26] = "La Cathedrale Engloutie";
	compo[27] = "Chopin"; frag[27] = "Etude op.10 n.2";
	compo[28] = "Berg"; frag[28] = "Klaviersonate n.1";
	compo[29] = "List"; frag[29] = "Feux Follets";
	compo[30] = "Schumann"; frag[30] = "Kreisleriana";
	compo[31] = "Chopin"; frag[31] = "Sonata op.58";
	compo[32] = "Rachmaninov"; frag[32] = "Prelude op.23 n.5";
	compo[33] = "Czerny"; frag[33] = "La Scuola della Velocita` n.1";
	compo[34] = "Saint-Saens"; frag[34] = "Etudes n.6";
	compo[35] = "Faure'"; frag[35] = "Tema & Variazioni";
	compo[36] = "Brahms"; frag[36] = "Op. 119 n.3";
	compo[37] = "Mussorgky"; frag[37] = "Quadri di un'Esposizione";
	compo[38] = "Hanon"; frag[38] = "Studi n.1";
	compo[39] = "List"; frag[39] = "Mephisto Valzer";
}
/^=/ { print }
/^[^=]/ && $4 != "A" {
	printf ("%8.4f %4s %8.4f %s\n", $1, $2, $3, $4);
}
$4 == "A" && $5 != "leave" {
	printf ("%8.4f %4s %8.4f %s %15s %s\n", $1, $2, $3, $4,
		 compo[i], frag[i]);
	++i;
}
$4 == "A" && $5 == "leave" {
	printf ("%8.4f %4s %8.4f %s LEFT ALONE\n", $1, $2, $3, $4);
}
