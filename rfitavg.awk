#
# $Id: rfitavg.awk,v 0.0 1993/06/12 19:01:55 nicb Exp $
#
# rhythm fitter averager - takes in input from rfit.awk debug_output=1
#	and builds an average based on best rhythmic divisors used
#	The algorithm is:
#		if divisor is a power of two
#			value = 1/divisor
#		else
#			value = -(1/divisor)
#
function is_power_of_two(num,		sq, sqi)
{
	sq = sqrt(num);
	sqi = int(sq);

	return (sq == sqi) ? 1 : 0;
}
function calculate_value(den,		result)
{
	if (den)
		if (is_power_of_two(den))
			result = 1/den;
		else
			result = -(1/den);

	return result;
}
BEGIN {
	FS=":"; OFS=":"; num_entries = 0;
}
END {
	at_value_avg = at_value / num_entries;
	dur_value_avg = dur_value / num_entries;

	print "tempo",tempo,"at avg",at_value_avg,"dur avg",dur_value_avg;
}
NF == 7 {
	++num_entries;
	at_value += calculate_value($2);
	dur_value += calculate_value($5);
}
#
# $Log: rfitavg.awk,v $
# Revision 0.0  1993/06/12 19:01:55  nicb
# Initial Revision
#
#
