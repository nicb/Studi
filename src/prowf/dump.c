/*
 * $Id: dump.c,v 0.1 1994/01/04 22:01:01 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <iostream.h>

ostream&
operator<<(ostream& os, const PitchClass r[])
{
	int previous_width = os.width();
	for (register int i = 0; i < ROW_NOTES; ++i)
	{
		os.width(3);
		os << (int) r[i];
	}
	os << flush;
	os.width(previous_width);

	return os;
}

ostream&
operator<<(ostream& os, const Chord& c)
{
	int previous_width = os.width();
	os << "(" << c.num_notes << "): ";
	for (register int i = 0; i < c.num_notes; ++i)
	{
		os.width(3);
		os << (int) c.notes[i];
	}
	os << flush;
	os.width(previous_width);

	return os;
}

ostream&
operator<<(ostream& os, const Pattern p)
{
	int previous_width = os.width();
	for (register int i = 0; i < ROW_NOTES; ++i)
	{
		switch(p[i].pft)
		{
			case PatternField::ANY_PATTERN:
				os << "A:";
				break;
			case PatternField::PART_OF_A_CHORD:
				os << "C:";
				break;
			case PatternField::SINGLE_NOTE:
				os.width(3);
				os << (int) p[i].note << ":";
				break;
			case PatternField::MULTIPLE_NOTE:
				os << p[i].chord << ":";
				break;
			default:
				break;
		}
	}
	os << endl;
	os.width(previous_width);

	return os;
}
/*
 * $Log: dump.c,v $
 * Revision 0.1  1994/01/04  22:01:01  nicb
 * chord distance calculation debug
 *
 * Revision 0.0  94/01/04  15:55:25  nicb
 * prowfx executable: dump debug
 * 
 */
