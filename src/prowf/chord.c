/*
 * $Id: chord.c,v 0.1 1994/01/04 22:00:58 nicb Exp $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <memory.h>

void
Chord::copy_chord(const Chord& c)
{
	num_notes = c.num_notes;
	possible_notes = c.possible_notes;
	copy_row(c.notes, notes, num_notes);
}
/*
 * $Log: chord.c,v $
 * Revision 0.1  1994/01/04 22:00:58  nicb
 * chord distance calculation debug
 *
 * Revision 0.0  94/01/04  15:55:07  nicb
 * prowfx executable: chord struct implementation
 * 
 */
