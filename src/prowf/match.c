/*
 * $Id: match.c,v 0.0 1994/01/04 15:56:59 nicb Exp $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>

Interval
match(const PitchClass n, const PatternField& pf)
{
	return ((pf.pft == PatternField::SINGLE_NOTE) && (pf.note==n)) ||
		(pf.pft == PatternField::ANY_PATTERN) ? 1 : 0;
}
/*
 * $Log: match.c,v $
 * Revision 0.0  1994/01/04 15:56:59  nicb
 * prowfx executable: match functions
 *
 */
