/*
 * $Id: extractr.c,v 0.0 1994/01/04 15:56:10 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <iostream.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

void extract_row(istream& is, PitchClass r[])
{
	register int i = 0;
	register int n = 0;
	char c = 0;
	char buf[5] = { '\0' };

	while((is.get(c)) && i < ROW_NOTES)
	{
		if (isspace(c))
		{
			r[i] = atoi(buf);
			++i;
			n = 0;
			memset(buf, 0, 5);
		}
		else
			buf[n++] = c;
	}
}
/*
 * $Log: extractr.c,v $
 * Revision 0.0  1994/01/04  15:56:10  nicb
 * prowfx executable: row extractor
 *
 */
