/*
 * $Id: main.c,v 0.0 1994/01/04 15:56:43 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <iostream.h>

int main(int ac, char *av[])
{
	Row _orw;
	Row _irw;
	Row _rorw;
	Row _rirw;
	SPattern s_pattern = NULL;
	Pattern pattern;

	if (ac < 2)
		usage();
	else
		s_pattern = av[1];

	/* split($0, _orw, " "); */
	extract_row(cin, _orw);
	invert_row(_orw, _irw);
	retrograde_row(_orw, _rorw);
	retrograde_row(_irw, _rirw);
	extract_pattern(s_pattern, pattern);

	search_row(pattern, _orw, _irw, _rorw, _rirw);

	return 0;
}
/*
 * $Log: main.c,v $
 * Revision 0.0  1994/01/04  15:56:43  nicb
 * prowfx executable: main function
 *
 */
