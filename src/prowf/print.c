/*
 * $Id: print.c,v 0.0 1994/01/04 15:58:38 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <stdio.h>

char*
print_row(const PitchClass row[], char *name, Interval transpo, Interval perm,
	int fits, Interval rd, char buffer[])
{
	sprintf(buffer, "%-2s[%02d]P[%02d]: %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d  (dist: %3d) (fits: %d)",
		name, transpo, perm,
		row[0],row[1],row[2],row[3],row[4],row[5],
		row[6],row[7],row[8],row[9],row[10],row[11],
		rd, fits);

	return buffer;
}
/*
 * $Log: print.c,v $
 * Revision 0.0  1994/01/04  15:58:38  nicb
 * prowfx executable: print function
 *
 */
