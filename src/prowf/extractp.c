/*
 * $Id: extractp.c,v 0.1 1994/01/04 22:01:05 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <strstream.h>

static Chord *last_chord = NULL;

static int
__extract_parenthesis(char *part, PatternField& p, int n)
{
	p.chord.notes[n++] = atoi(++part);

	return n;
}

static int
__extract_brackets(char *part, PatternField& p, int n)
{
	while(*part != '\0')
	{
		if (isdigit(*part))
			p.chord.notes[n++] = *part - '0';
		++part;
	}

	return n;
}

static void
__part_of_a_chord(PatternField& p)
{
	p.pft = PatternField::PART_OF_A_CHORD;
	if (last_chord != NULL)
		++last_chord->possible_notes;
	else
		fatal("Panic: got part of a chord without chord");
}

static void
__multiple_note(char *regexp, PatternField& p)
{
	p.pft = PatternField::MULTIPLE_NOTE;
	p.chord.possible_notes = 1;
	char buf[256] = { '\0' };
	strcpy(buf, regexp);
	char* divisor = buf;
	register int i = 1;
	char *parts[256] = { buf, NULL };

	while((divisor = strchr(divisor, '|')) != NULL)
	{
		*divisor++ = '\0';
		parts[i++] = divisor;
	}

	register int j = 0, n = 0;
	while(j < i)
	{
		if ((divisor = strchr(parts[j], '(')) != NULL)
			n = __extract_parenthesis(divisor, p, n);
		else if ((divisor = strchr(parts[j], '[')) != NULL)
			n = __extract_brackets(divisor, p, n);
		else
			fatal("error in regular expression pattern");
		++j;
	}
	p.chord.num_notes = n;
	last_chord = &p.chord;
}

static void
__extract_note(char *regexp, PatternField& p)
{
	p.pft = PatternField::SINGLE_NOTE;

	while(!isdigit(*regexp))
		++regexp;

	p.note = atoi(regexp);
}

static void
_extract_pattern(char *regexp, PatternField& p)
{
	if (*regexp == 'A' || *regexp == 'a')
		p.pft = PatternField::ANY_PATTERN;
	else if (*regexp == 'C' || *regexp == 'c')
		__part_of_a_chord(p);
	else if (strchr(regexp, '[') || strchr(regexp, '(')
		|| strchr(regexp, '|'))
		__multiple_note(regexp, p);
	else
		__extract_note(regexp, p);
}

void
extract_pattern(const SPattern sp, PatternField p[])
{
	istrstream is(sp, strlen(sp) + 1);
	register int i = 0;
	register int n = 0;
	char c = 0;
	char buf[256] = { '\0' };

	while((is.get(c)) && i < ROW_NOTES)
	{
		if (c == ':' || c == '\0')
		{
			_extract_pattern(buf, p[i]);
			++i;
			n = 0;
			memset(buf, 0, 256);
		}
		else
			buf[n++] = c;
	}
}
/*
 * $Log: extractp.c,v $
 * Revision 0.1  1994/01/04  22:01:05  nicb
 * chord distance calculation debug
 *
 * Revision 0.0  94/01/04  15:55:40  nicb
 * prowfx executable: pattern extractor
 * 
 */
