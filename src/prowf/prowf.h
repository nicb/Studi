/*
 * $Id: prowf.h,v 0.2 1994/01/09 01:52:53 nicb Exp $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */
#if !defined(_prowf_h_)
#define _prowf_h_

#define _ROW_NOTES_	12
extern const char ROW_NOTES;
typedef char PitchClass;
typedef char Interval;
typedef char* SPattern;
typedef PitchClass Row[_ROW_NOTES_];

struct Chord
{
	int num_notes;
	int possible_notes;
	Row notes;
	void copy_chord(const Chord&);
};

struct PatternField
{
	enum PatternFieldType
	{
		ANY_PATTERN,
		PART_OF_A_CHORD,
		MULTIPLE_NOTE,
		SINGLE_NOTE
	} pft;
	union
	{
		Chord chord;
		PitchClass note;
	};
};

typedef PatternField Pattern[_ROW_NOTES_];

/* prototypes */

class istream;
class ostream;

Interval abs(Interval);
void remove_brackets_and_terms(Pattern *);
int split(const Pattern&, Chord [], const char *);
int split(const Pattern&, PitchClass [], const char *);
void remove_parenthesis(Chord&);
int length(PitchClass[]);
PitchClass get_note(const PitchClass _tr[], const int);
/* Interval match(const PitchClass[], const Pattern&, int); */
Interval match(const PitchClass, const PatternField&);
Interval match_note(const Pattern&, const PitchClass[], int&, int&);
Interval note_distance(const PitchClass, const PitchClass);
Interval row_distance(const Pattern& pattern_row, const PitchClass found_row[],
	int& nf);
int calculate_fits(const PitchClass[], const Chord&, int);
void permutate_group(const PitchClass row[], PitchClass return_row[],
	int p, int num_notes);

void extract_row(istream&, PitchClass[]);
void extract_pattern(const SPattern, PatternField[]);
void copy_row(const PitchClass[], PitchClass[], int);
Interval get_best_chord_position(const PitchClass[], const Pattern&, int&, int&);
void fatal(const char *);
ostream& operator<<(ostream&, const PitchClass[]);
ostream& operator<<(ostream&, const Pattern);
ostream& operator<<(ostream&, const Chord&);
char* print_row(const PitchClass[], char*, Interval, Interval, int, Interval, char[]);
void print_step(const char *);
void print_transpo_step(const int);
void step_printing_start();
void step_printing_end();
void next_step();

void usage();
void invert_row(const PitchClass[], PitchClass[]);
void retrograde_row(const PitchClass[], PitchClass[]);
void search_row(const PatternField[], const PitchClass[], const PitchClass[],
	const PitchClass[], const PitchClass[]);
#endif /* !defined(_prowf_h_) */
/*
 * $Log: prowf.h,v $
 * Revision 0.2  1994/01/09  01:52:53  nicb
 * added print_step functions
 *
 * Revision 0.1  94/01/04  22:01:13  nicb
 * chord distance calculation debug
 * 
 * Revision 0.0  94/01/04  15:59:31  nicb
 * prowfx executable: prowf header file
 * 
 */
