/*
 * $Id: psteps.c,v 0.0 1994/01/09 01:53:11 nicb Exp $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <iomanip.h>

void
print_step(const char *name)
{
	cerr << setw(2) << name << ": ";
}

void
print_transpo_step(const int t)
{
	cerr << setw(2) << t;
}

void
step_printing_start()
{
}

void
next_step()
{ 
	cerr << endl;
}

void
step_printing_end()
{ 
}
/*
 * $Log: psteps.c,v $
 * Revision 0.0  1994/01/09  01:53:11  nicb
 * Initial Revision
 *
 */
