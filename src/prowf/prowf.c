/*
 * $Id: prowf.c,v 0.2 1994/01/09 01:52:49 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <iostream.h>
#include <string.h>
#include <stdlib.h>

const char ROW_NOTES = _ROW_NOTES_;
const int PROWF_LINE_SIZE = 128;
static Chord last_chord;

Interval
abs(Interval n)
{
	return (n < 0) ? -n : n;
}

void copy_row(const PitchClass source[], PitchClass dest[], int n_elements)
{
	for (register int i = 0; i < n_elements; ++i)
		dest[i] = source[i];
}

void remove_from_saved_chord(PitchClass result)
{
	for (register int i = 0; i <= last_chord.num_notes; ++i)
		if (result == last_chord.notes[i])
		{
			last_chord.notes[i] = 'D';
			break;
		}
}

Interval
row_distance(const Pattern& pattern_row, const PitchClass found_row[],
	int& nf)
{
	register Interval d = 0;
	int i = 0;
	while (i < ROW_NOTES) /* i is incremented inside match_note */
		d += match_note(pattern_row, found_row, i, nf);

	return d;
}

inline void
permutate_row(const PitchClass row[], PitchClass return_row[], int p)
{
	permutate_group(row, return_row, p, ROW_NOTES);
}

void
compare_rows(const Pattern& pattern, const PitchClass row[],
	char *name, int t)
{
	for (register int i = 0; i < ROW_NOTES; ++i)
	{
		char buffer[PROWF_LINE_SIZE];
		Row prow;
		permutate_row(row, prow, i);
		int num_of_fits = 0;
		Interval rd = row_distance(pattern, prow, num_of_fits);

		cout	<< print_row(prow,name, t, i, num_of_fits, rd, buffer)
			<< endl;
	}
}

void transpose_row(const PitchClass row[], int factor, PitchClass return_row[])
{
	for (register int i = 0; i < ROW_NOTES; ++i)
		return_row[i] = (row[i] + factor) % ROW_NOTES;
}

void do_all_rows(const Pattern& pattern, const PitchClass row[], char *name)
{
	Row trow;
	print_step(name);
	for (register i = 0; i < ROW_NOTES; ++i)
	{
		print_transpo_step(i);
		transpose_row(row, i, trow);
		compare_rows(pattern, trow, name, i);
	}
	next_step();
}

void
search_row(const PatternField _pattrn[], const PitchClass orow[],
	const PitchClass irow[], const PitchClass rorow[],
	const PitchClass rirow[])
{
	step_printing_start();
	do_all_rows(_pattrn, orow, "O");
	do_all_rows(_pattrn, irow, "I");
	do_all_rows(_pattrn, rorow, "R");
	do_all_rows(_pattrn, rirow, "RI");
	step_printing_end();
}

PitchClass m_12(const PitchClass i)
{
	PitchClass result = i;
	if (result > 11)
		result -= ROW_NOTES;
	else if (result < 0)
		result += ROW_NOTES;

	return result;
}

void invert_row(const PitchClass row[], PitchClass rrow[])
{
	rrow[0] = row[0];
	register int i = 1;
	for (i = 1; i < ROW_NOTES; ++i)
		rrow[i] = m_12(rrow[i-1] - (row[i] - row[i-1]));
}

void retrograde_row(const PitchClass row[], PitchClass rrow[])
{
	register int j = ROW_NOTES - 1;
	for (register int i = 0; i < ROW_NOTES; ++i)
	{
		rrow[i] = row[j];
		--j;
	}
}

void usage()
{
	fatal("Usage: prowf <pattern> < <row file>");
}
/*
 * $Log: prowf.c,v $
 * Revision 0.2  1994/01/09  01:52:49  nicb
 * added print_step functions
 *
 * Revision 0.1  94/01/04  22:01:09  nicb
 * chord distance calculation debug
 * 
 * Revision 0.0  94/01/04  15:59:00  nicb
 * prowfx executable: miscellaneous functions
 * 
 */
