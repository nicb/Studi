/*
 * $Id: perm.c,v 0.0 1994/01/04 15:57:19 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>

void
permutate_group(const PitchClass row[], PitchClass return_row[], int p,
	int num_notes)
{
	for (register int i = 0; i < num_notes; ++i)
	{
		return_row[i] = row[p];
		p = (++p % num_notes);
	}
}
/*
 * $Log: perm.c,v $
 * Revision 0.0  1994/01/04  15:57:19  nicb
 * prowfx executable: permutation functions
 *
 */
