/*
 * $Id: calcnote.c,v 0.0 1994/01/04 15:54:49 nicb Exp nicb $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>
#include <iostream.h>
#include <string.h>
#include <stdlib.h>
#include <strstream.h>

/*
static void
_dump_fit(ostream& os, const PitchClass r[], const Chord& c, int idx, int f)
{
	register int end = idx + c.num_notes;
	while(idx < end)
	{
		os.width(3);
		os << (int) r[idx++];
	}
	os << " against " << c << " gives " << f << endl;
}
*/

int
_calculate_fits(const PitchClass r[], const Chord& c, register int idx)
{
	register int end = idx + c.num_notes, i = 0;
	int result = 0;
	while(idx < end)
	{
		if (r[idx] == c.notes[i])
			++result;
		++idx;
		++i;
	}

	return result;
}

int
calculate_fits(const PitchClass r[], const Chord& c, int idx)
{
	int result = 0;
	Chord pc;
	pc.copy_chord(c);
	for (register int i = 0; i < pc.num_notes; ++i)
	{
		permutate_group(c.notes, pc.notes, i, c.num_notes);
		int tmp_result = _calculate_fits(r, pc, idx);
		result = tmp_result > result ? tmp_result : result;
	}

	return result;
}

Interval
note_distance(const PitchClass target, const PitchClass source)
{
	Interval result = abs(target - source);
	return (result > 6) ? (12-result) : result;
}

Interval
match_note(const Pattern& pattern, const PitchClass target[], int& idx, int& nf)
{
	Interval result = 0;
	switch (pattern[idx].pft)
	{
		default:
		case PatternField::ANY_PATTERN:
			nf += (result == 0) ? 1 : 0;
			++idx;
			break;
		case PatternField::MULTIPLE_NOTE:
			result = get_best_chord_position(target, pattern, idx,
				nf);
			break;
		case PatternField::PART_OF_A_CHORD:
			fatal("Got part of a chord without chord");
			break;
		case PatternField::SINGLE_NOTE:
			result = note_distance(target[idx], pattern[idx].note);
			nf += (result == 0) ? 1 : 0;
			++idx;
			break;
	}
	return result;
}
/*
 * $Log: calcnote.c,v $
 * Revision 0.0  1994/01/04  15:54:49  nicb
 * prowfx executable: note distance calculation
 *
 */
