/*
 * $Id: bstchord.c,v 0.3 1994/02/02 18:34:14 nicb Exp $
 *
 * prowf.exe: permutating best row finder - Usage:
 *	{cat | echo} row | prowf <regexp>:...:<regexp>
 *	(where the pattern should be the best possible one - prowf.exe will
 *	 attempt to find the best fitting and will give a quote on number
 *       of fits and global intervallic distance)
 *
 * The regexp used here are slightly modified. Their rules are:
 *
 * - ^[0-9]$: any single digit from 0 to 9 (but repetitions are considered
 *            to be chords with octave doublings)
 * - ^(10)$ | ^(11)$: digits 10 and 11
 * - C or c:  chords (that use previous combinations of notes)
 * - A or a:  any note value (equivalent to .* in normal regexp)
 */

#include <prowf.h>

static Interval
_match_chord(const PitchClass r[], const Chord& c, int idx)
{
	register Interval result = 0;

	for (register int i = 0; i < c.possible_notes; ++i)
		result += note_distance(r[idx + i], c.notes[i]);

	return result;
}

static void
_swap(PitchClass& a, PitchClass& b)
{
	PitchClass tmp = b;
	b = a;
	a = tmp;
}

static Interval
_get_best_chord_position(const PitchClass r[], Chord& pc, int& nf, int idx,
	unsigned long ntot, unsigned long nn = 0)
{
	Interval result = 127;
	if (nn <= ntot)
	{
		register unsigned long i = nn;
		do
		{
			int tmp_nf = 0;
			Interval tmp_result = 0;
			_swap(pc.notes[i], pc.notes[nn]);
			tmp_result = _get_best_chord_position(r, pc, tmp_nf,
				idx, ntot, nn + 1);
			if (tmp_result < result)
			{
				nf = tmp_nf;
				result = tmp_result;
			}
		}
		while(i--);
	}
	else
	{
		result = _match_chord(r, pc, idx);
		nf = calculate_fits(r, pc, idx);
	}

	return result;
}

Interval
get_best_chord_position(const PitchClass row[], const Pattern& p, int& idx, int& nf)
{
	Interval result = 0;
	int num_notes = p[idx].chord.possible_notes, tmp_nf = 0;
	Chord pc;
	pc.copy_chord(p[idx].chord);
	result = _get_best_chord_position(row, pc, tmp_nf, idx,
		num_notes - 1);
	idx += num_notes;
	nf += tmp_nf;

	return result;
}
/*
 * $Log: bstchord.c,v $
 * Revision 0.3  1994/02/02  18:34:14  nicb
 * corrected bug for chord with more notes than selections
 *
 * Revision 0.2  94/01/29  13:58:33  nicb
 * first debug of chord permutations
 * 
 * Revision 0.1  94/01/04  22:00:54  nicb
 * chord distance calculation debug
 * 
 * Revision 0.0  94/01/04  15:54:25  nicb
 * prowfx executable: best chord finder function file
 * 
 */
