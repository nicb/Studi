# Useful resources to learn Lilypond programming in-depth

* [Jean Abou Samra's Book](https://extending-lilypond.gitlab.io/en/index.html)
* [Urs Liska's Book](https://scheme-book.readthedocs.io/en/latest/)
