\version "2.24.3"

\paper {
  #(set-paper-size "a3")
}

\include "../../../.git/shortHash.ly"

\header {
  title = "Studi"
  composer = "Nicola Bernardini"
  year = "(1994)"
  tagline = \markup {
    \revision - Engraved
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\include "tweaks.ly"
\include "global_settings.ly"

\include "bars.ly"
\include "skips.ly"
\include "upper_3.ly"
\include "upper.ly"
\include "lower.ly"
\include "lower_34.ly"
\include "pedals.ly"

\score {

  \new PianoStaff
  <<
   	\new Staff = "RH" \upper
  	\new Staff = "RH.3" \with { alignAboveContext = "RH" }   { \upperthree }
 	\new Staff = "LH" \lower
	\new Staff = "LH.3" \with { alignBelowContext = "LH" }   { \lowerthree }
	\new Staff = "LH.4" \with { alignBelowContext = "LH.3" } { \lowerfour }
	\new Dynamics = "sustain"   \with { alignBelowContext = "LH.4" }     \sustain
	\new Dynamics = "tonal"     \with { alignBelowContext = "sustain" }  \tonal
	\new Dynamics = "unacorda"  \with { alignBelowContext = "tonal" }    \unaC
  >>
  \layout {
  	\context {
		\PianoStaff
		\global_settings
		\numericTimeSignature
		\remove "Time_signature_engraver"
		\remove "Keep_alive_together_engraver"
        	\override Flag.stencil = #modern-straight-flag
		\RemoveAllEmptyStaves
		\consists "Span_stem_engraver"
		\consists "Piano_pedal_engraver"
		\consists "Piano_pedal_align_engraver"
  	}
  }
  \midi {}
}
