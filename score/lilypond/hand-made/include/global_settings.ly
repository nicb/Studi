global_settings = {
		\set subdivideBeams = ##t
		\set tupletFullLength = ##t
		\set tupletFullLengthNote = ##f
		\override TupletNumber.text = #tuplet-number::calc-fraction-text
		\override TupletBracket.bracket-visibility = ##t
	        \set Timing.beamExceptions = #'()
	        \set Timing.baseMoment = #(ly:make-moment 1/8)
	        \set Timing.beatStructure = 1,1,1,1,1,1,1,1
		\set Staff.pedalSustainStyle = #'mixed
		\set Staff.pedalUnaCordaStrings = #'("1C." "(1C.)" "")
		\set Staff.pedalUnaCordaStyle = #'mixed
		\set Staff.pedalSostenutoStrings = #'("Ped.Ton." "(Ped.Ton.)" "")
		\set Staff.pedalSostenutoStyle = #'mixed
		\override Staff.TimeSignature.style = #'single-digit
		\set Score.tempoHideNote = ##t
		\set Staff.ottavationMarkups = #ottavation-ordinals
		\override Hairpin.to-barline = ##f
		\override Hairpin.minimum-length = 6
}
