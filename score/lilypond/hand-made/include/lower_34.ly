%
% lower staves when needed
%
lowerthree = {
	\relative c {
          	\global_settings
		\stopStaff
		\omit Staff.Clef
		\skiponefortyseven
		\lhfortyeightthree
		\lhfortyninethree
		\startStaff
                \lhfiftythree
		\lhfiftyonethree
		\lhfiftytwothree
		\lhfiftythreethree
		\lhfiftyfourthree
		\lhfiftyfivethree
		\lhfiftysixthree
		\lhfiftyseventhree
		\lhfiftyeightthree
		\lhfiftyninethree
	}
}

lowerfour = {
		\relative c {
			\global_settings
			\stopStaff
			\omit Staff.Clef
			\skiponefortyseven 
			\startStaff
			\once \override Clef.transparent = ##t
			\lhfortyeightfour
			\lhfortyninefour
			\lhfiftyfour
			\lhfiftyonefour
			\lhfiftytwofour
			\lhfiftythreefour
			\lhfiftyfourfour
			\lhfiftyfivefour
			\lhfiftysixfour
			\lhfiftysevenfour
			\lhfiftyeightfour
			\lhfiftyninefour
		}
	}
