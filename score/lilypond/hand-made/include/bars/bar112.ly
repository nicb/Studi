%
% bar n.112
%

rhonehundredandtwelveone = {
		r8 \stemDown
		\tempo \markup { "Tempo" \circle 5 ", ben sostenuto" } 4 = 126
		e32[_\p ais, b c d fis, c' e
		e a,! ais d cis fis, gis d'
		f! d bes a cis dis c! b!
		gis' d ais c] |
  }

rhonehundredandtwelvetwo = {
		s1 |
  }

lhonehundredandtwelveone = {
		\clef treble
		r8 \stemUp
		gis'8[_._\p f_. g!_. f!_. f_. g,!_. fis'_.] | 
  }

lhonehundredandtwelvetwo = {
		s1 |
  }
