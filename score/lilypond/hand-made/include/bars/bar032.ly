%
% bar n.32
%

rhthirtytwoone = {
		s1 |
  }

rhthirtytwotwo = {
		s1 |
  }

lhthirtytwoone = {
                \clef bass
		r8 \ottava -1 fis\tweak X-offset -4_\f_.[ \ottava 0
		\change Staff = "RH" b'''\tweak X-offset -2_\f^.] \change Staff = "LH" \clef treble b_\p_.[
		\change Staff = "RH" \clef bass \ottava -1 dis,,,,8\tweak X-offset -3_\p^.] \ottava 0
		\change Staff = "LH" r8
		\clef bass cis''8_\p_.[ \change Staff = "RH" cis8_\p^.] |
		\change Staff = "LH"
  }

lhthirtytwotwo = {
		s1 |
  }

