%
% bar n.43
%

rhfortythreeone = {
	\time 4/4
	\slashedGrace { <cis fis a dis>8 }
	r4 \stemUp
	\slashedGrace { <d'! c'!>8_\mf~ } <d c'>8.[ <cis d'>16]~ <cis d'>4 r16 cis!8.^> | 
  }

rhfortythreetwo = {
	s2 \stemDown a'4\tweak X-offset -3_\f_>~ \slashedGrace { a8 } r8 \tuplet 3/2 { r16 dis,^([ f!)] } |
  }

lhfortythreeone = {
	<<
	        {
			\change Staff = "LHtop" \stemDown <ais, cis g'>8\tweak X-offset -4_\mf[
			\change Staff = "LH" \stemUp  g~] \stemUp \once \override Beam.positions = #'(2.5 . 3) g16[ a!8 r16]
			fis'8^~ \slashedGrace { fis8 } f!8^~ f16[ d8 r16]
		}
		\new Staff = "LHtop" \with { alignBelowContext = "RH" }
		{
			\relative c {
				\global_settings
				\clef bass
				
				s4 \tuplet 15/12 { r4 \ottava -1 dis,,8\tweak X-offset -4\tweak Y-offset -2_\p \ottava 0 r8
				c''!_._\p[ b_.] r ais16~ }
			}
			\override Staff.TimeSignature.stencil = ##f
		}
	>>
  }

lhfortythreetwo = {
	\slashedGrace { b8 }
	s2 \tuplet 3/2 { c!16[_\(\tweak X-offset -3_\f gis' d\)~] } \slashedGrace { \stemDown d8 }
	dis8_~ dis16 r8.
  }

