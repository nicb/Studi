%
% bar n.9
%

rhnineone = {
		\tieSolid
		\tuplet 3/2 { r4 bes'8 } c,8. b'16~ \slashedGrace { b8 } <d, e f c'>4_\mp_\pocoapoco <d ais' b cis> | % bar 9
  }

rhninetwo = {
		s16 \crossStaff { <ees f b!>8\mf } s16 s2.                              | % bar 9
  }

lhnineone = {
		 s4 \tuplet 3/2 { r16[ <f,~ g'~>8] } <f g'>8 s2                                     | % bar 9
  }

lhninetwo = {
		\change Staff = "RH" <d'' dis c' cis>16\mp \change Staff = "LH"
		\offset Y-offset -2 \ottava -1\crossStaff { <c,,,~ des'~>8\f } \slashedGrace { <c des'>8 } r16
		s4 \ottava 0\tuplet 5/4 { <d'' des g>4\mf  <f~ g~ aes~>4.  } | % bar 9
  }

