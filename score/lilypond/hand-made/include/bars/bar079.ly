%
% bar n.79
%

rhseventynineone = {
		\clef treble
		\stemUp
		\tuplet 7/4 { ees'16_\mp_\([ b! a' aes ges f bes]\) }
		r8.\tweak positions #'(10 . 10)[ \ottava 1 <d'' f ges>16_\pppp]^~ <d f ges>2^~ |
  }

rhseventyninetwo = {
	s4 <e'' b' d e>2._\mf
  }

lhseventynineone = {
		\clef treble
		\stemUp
		r4 <d' g c! cis! d>2.\tweak X-offset -4_\mf
  }
lhseventyninetwo = {
		\stemDown
		s4 r8. \tweak Y-offset -4r32[ gis\tweak X-offset -9_\ffff^! gis^! \tweak Y-offset-4r \tweak Y-offset -4r gis^! \tweak Y-offset -4r16 \tweak Y-offset -4r32 gis^!] \tweak Y-offset -4r4
  }
