%
% bar n.70
%

rhseventyone = {
	\stemUp
	s2 a'4^>^~ \slashedGrace { a8 } s4 |
  }

rhseventytwo = {
	\stemDown
	\slashedGrace { <a'' bes c>8_\ff_~^( } <a bes c>8.)[
	<g, b! c d>16_\p_~] <g b c d>4_~
	<g b c d>16[ e' gis, d' e f c' d] |
  }

lhseventyone = {
	\tupletUp \stemUp
	\slashedGrace { gis,,8_\ff_~ }
	\tuplet 4/3 { gis4[ e'' c r] } s4 |
  }

lhseventytwo = {
	\stemDown
	r8.\tweak positions #'(-7 . -6)[ <f ais>16]_~ <f ais>4 <cis,! dis'!>4_>_~ \slashedGrace { <cis dis'>8 }
	<b e'>4_~ |
  }
