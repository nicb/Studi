%
% bar n.105
%

rhonehundredandfiveone = {
		f8[ dis \slashedGrace { ais'8_( } fis) cis d! r fis f!] |
  }

rhonehundredandfivetwo = {
		bis4_~ bis8 b!4^-^> cis8_~ cis4_~ |
  }

lhonehundredandfiveone = {
    \stemDown
    \slashedGrace { a8_\f_( } fis'16)[ a] r e[ a bis] r a[ bis fis'] r gis,,[ gis' b!] r c |
  }

lhonehundredandfivetwo = {
    s1 |
  }
