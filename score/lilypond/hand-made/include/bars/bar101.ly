%
% bar n.101
%

rhonehundredandoneone = {
		\stemUp
		\slashedGrace { \stemUp e8^~ } e8^>[ d!]^> \slashedGrace { b8^~_( } b2)^> s4 |
  }

rhonehundredandonetwo = {
		\stemDown
		\slashedGrace { \stemDown c,!8_~ } c16[ <fis dis'> cis <b g'>]
		b,[ <c d e> <f! a!>32^\( cis'' ais,\)] r 
		r <cis, b'>[^\( a' disis,\) bis16 <gis' ais>] b,!8[ \slashedGrace { \stemUp f!_\ff^~_( } \stemDown f)_~]\tweak X-offset 2_\markup { \draw-dashed-line #'(1.5 . -1) } |
  }

lhonehundredandoneone = {
		\stemUp
		\slashedGrace { d'8^~_( } d8)[ e_.] s2. |
  }

lhonehundredandonetwo = {
		<<
			{
				\stemDown
				\slashedGrace { \stemDown a8_~ } a4
				\clef treble
				r16[ \slashedGrace { f'''8_( } dis'8)^. b16]^._~
				b16[ f^. \slashedGrace { f,!8_( } fis'16^-)] 
				r8[ \slashedGrace { fis,8_\ff_~^( } fis8]^~\tweak X-offset 3\tweak Y-offset 0^\markup { \draw-dashed-line #'(10 . 8) } |
			}
			\new Staff = "LH.2" \with { alignBelowContext = "LH" \clef bass \once \omit TimeSignature } {
				\global_settings
				\relative c {
					\stemUp
					s4 \slashedGrace { <gis fis'>8^~_( } <gis fis'>2
					\stemDown r8.[ \slashedGrace { \stemDown d'!8\tweak X-offset -4_\ff_~^( } d16^-]\tweak X-offset -6^\markup { \italic "(R.H.)" }\tweak X-offset 3^\markup { \draw-dashed-line #'(1 . 5) } |
				}
			}
		>>
  }
