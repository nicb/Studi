%
% bar n.129
%

rhonehundredandtwentynineone = {
		\time 3/4
		\stemUp
		r8 <f aes>[ <ges bes> <e! a!> <ges bes> <g! b!>] |
  }

rhonehundredandtwentyninetwo = {
		\stemDown
		a,8[ c ees des aes c] |
  }

lhonehundredandtwentynineone = {
		\stemDown
		g8\=1) e,[^\(_\< d'! des f' d,!] | 
  }

lhonehundredandtwentyninetwo = {
		s2.
  }
