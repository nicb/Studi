%
% bar n.18
%

rheighteenone = {
		\global_settings
		d16[ <gis, ais e' g>8.] \stemDown
		\ottava 1 f''!32_\pp^\([ d' b ais a!  g'^.\)] \stemUp \ottava 0 <g,,, ais b a'>16~
		<g ais b a'>8 \slashedGrace { e'8~ } \stemDown e32_\pp^\([ dis c b]
		\stemUp fis[ f e \slashedGrace { d8_( } c'!32) b, fis f'! gis_.\)]                    | % bar 18
  }

rheighteentwo = {
		s1 |
  }

lheighteenone = {
 		cis16 <f,! fis'>8. \stemUp r32[ <bis cis>32_-\f~ <bis cis>8 <fis e'>16\p_-~]
 		<fis e'>8 \stemDown \tuplet 4/3 { <cis dis>8^-\f[ <a gis'>^- <cis dis>^- <a gis'>^-] }  | % bar 18
  }

lheighteentwo = {
		s1 |
  }

