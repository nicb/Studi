%
% bar n.128
%

rhonehundredandtwentyeightone = {
		\time 9/4
		<<
			{
				<e c'>8 s2.. s4 \tweak Y-offset 4r16\tweak positions #'(6 . 6)[ d!8.]^~ d2\shape #'((0 . 0) (0 . 0)(2 . 0) (2 . 0))^~ \hideNote d8 \once \set fontSize = #3 \tweak Y-offset 5.5r8 |
			}
			\new Staff = "RH.2" \with { alignAboveContext = "RH" \clef treble } {
				\global_settings
				<<
					\relative c'' {
						\stemUp
						r2 <d a'>2_-^~ <d a'>1^~ <aes d a'>8 s8  |
					}
					\new Voice {
						\relative c'' {
							\stemDown
							r4 r8[ aes8]_\f^-_~ aes2_~ aes1_~ \hideNote aes8 s8 |
						}
					}
				>>
			}
		>>
  }

rhonehundredandtwentyeighttwo = {
		\stemDown
		d8 <e b! g>2..^-_~ <e b g>1_~ \stemUp <e d b g>8 s8 |
  }

lhonehundredandtwentyeightone = {
		<<
			{
				\stemUp
				r4 r8 b8_-_\f^~ b2^~ b1\shape #'((0 . 0) (0 . 0) (-2 . 0) (-2 . 0))^~ \hideNote b8 s8 |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemUp
					s2. s8 b!8^~ b1\shape #'((0 . 0) (0 . 0) (-3 . 0) (-3 . 0))^~ \hideNote b8 s8 |
				}
			}
		>>
  }

lhonehundredandtwentyeighttwo = {
		<<
			<<
				{
					\stemDown
					r2 a!2_~ a1\shape #'((0 . 0) (0 . 0) (-3 . 0) (-3 . 0))_~ \hideNote  a8 s8 |
				}
				\new Voice {
					\global_settings
					\relative c, {
					    \stemDown
							s2.. a8_~ a1_~ \crossStaff { <a a'' b b'>8 }  s8 |
					}
				}
			>>
			\new Staff = "LH.2" \with { alignBelowContext = "LH" \clef bass }{
				\global_settings
				<<
					\new Voice {
						\relative c {
							\stemDown
							c1_\f^-^~ c1\shape #'((0 . 0) (0 . 0) (-2 . 0) (-2 . 0))^~ \hideNote  c8 s8 |
						}
					}
					\new Voice {
						\relative c, {
							\stemDown
							r8 des2..^-_~ des1_~ \crossStaff { <des c'>8 } \change Staff = "LH" \voiceFour a''^\=1(\tweak X-offset -3.5_\markup { \dynamic "mp" \italic "leggero" } |
						}
					}
				>>
			}
		>>
  }
