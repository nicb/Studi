%
% bar n.116
%

rhonehundredandsixteenone = {
		\stemDown
		r16[ ais,!_\p g'!32 d aes f]
		cis'[ b a! c! e g,! ais b]
		\stemUp
		d[ c ais gis
		\tempo \markup { "Tempo" \circle 3 ", affrettando" } 4 = 118
		a' cis, f,! e']
		r16[ g,_\p \tuplet 5/4 { gis32 e' d f,! fis] } |
  }

rhonehundredandsixteentwo = {
		\stemDown
		s2. \tuplet 3/2 { g'!8^._\mf a,^. \tweak Y-offset -4 r } |
  }

lhonehundredandsixteenone = {
		\clef treble
		\stemUp
		r8 gis'8[_._\p dis_. gis_. dis_. fis_.]
		\clef bass \stemDown
		b,,[^-_~ \slashedGrace { b8 } fis''']^- |
  }

lhonehundredandsixteentwo = {
		s1 |
  }
