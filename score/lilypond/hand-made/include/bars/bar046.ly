%
% bar n.46
%

rhfortysixone = {
	d4 r8\stopTrillSpan \pitchedTrill g!4.~\startTrillSpan ges
	\tuplet 6/4 { g8[ gis16\stopTrillSpan d c fis] } |
  }

rhfortysixtwo = {
	s1 |
  }

lhfortysixone = {
	\slashedGrace { b8 } s4 r8 gis4._-^\p^~ \slashedGrace { gis8 } r4 |
  }

lhfortysixtwo = {
	\slashedGrace { g8 } <gis' d' f>8_>_\ff[
	\shape #'((1 . 5) (5 . 7) (-4 . -2) (0 . -2)) PhrasingSlur
	c^._\(] \slashedGrace { gis,8_(^~ } gis16)\) r16 r32 e'16._\f^- r2 |
  }
