%
% bar n.68
%

rhsixtyeightone = {
		\time 3/4
		\tempo \markup { "Tempo" \circle 5 } 4 = 116
		\stemUp
		b'16\tweak positions #'(8.5 . 6.25)[ dis dis, fis] d'!16[ <dis,,! gis f'!>8.]
		\slashedGrace { <a gis'! bes>8_\p_(^~ } <a gis' bes>4)^~ | 
  }

rhsixtyeighttwo = {
		\stemDown
		ais'''16\tweak positions #'(-2 . -4)[\tweak Y-offset 2.5_\mp_\tweak positions #'(2.5 . 0)\< cis dis, fis]
		c'!16[_\mf <dis,,! gis f'!>8._\ff] s4 |
  }

lhsixtyeightone = {
		\clef bass
		\stemDown
		<g,, gis'>8_\mf <e, f'>4^> e^> r8 |
  }

lhsixtyeighttwo = {
		s2. |
  }
