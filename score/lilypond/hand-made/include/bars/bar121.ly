%
% bar n.121
%

rhonehundredandtwentyoneone = {
		\stemDown
		b,32[ dis g! fis,
		a cis f! e]
		ees[ d c ais
		e'_\markup { \italic "cresc." } fis g gis]
		a[ f! cis! ais!
		f! g! a! c!]
		cis[ d ais dis
		a! f_\markup { \italic "dim." } bes c!] |
  }

rhonehundredandtwentyonetwo = {
		s1 |
  }

lhonehundredandtwentyoneone = {
		s1 |
  }

lhonehundredandtwentyonetwo = {
		\stemDown
		<f'! c'!>8]^.
		gis,^.[ <g'! b>^.]
		d!^.[ <b' dis>^.]
		<fis! c'! dis>^.[ <gis b e>^.]
		<g! b gis'>^.[ \allowBreak |
  }
