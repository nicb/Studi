%
% bar n.67
%

rhsixtysevenone = {
	\time 4/4
	\stemUp
	\clef treble
	b'32\tweak positions #'(5 . 6)[ bes <gis a> ges d'! <b c> bes a
	<fis g> f ees <cis d> aes' g <dis e> d
	c <ais b> c cis <cisis dis> e fis <gis a>
	b c! <bis cis> dis b! cis! dis! e] |
  }

rhsixtyseventwo = {
	s1 |
  }

lhsixtysevenone = {
	<<
		\new Staff = "LH.3" \with { alignBelowContext = "LH" }
		{
			\global_settings
			\relative c {
				\clef bass
				\stemUp
				s2. d,16_!\tweak positions #'(4 . 6)[ f_! b_! g'_!] 
				\once \omit Staff.TimeSignature |
			}
		}
		{
			\clef treble
			\stemDown
			d'''8^!_\markup { \dynamic "sfz" \italic "sempre" }[
			\change Staff = "LH.3" 
			\stemUp <bes,,, ees c'>_!]
			\change Staff = "LH" \stemDown
			b'''^!\tweak positions #'(-4 . -3.5)[ c^!]
			\change Staff = "LH.3" \stemUp
			<a,,,, b'>_![
			\change Staff = "LH" \stemDown
			d'''^!] r4 |
		}
	>>
  }

lhsixtyseventwo = {
	s1 |
  }

