%
% bar n.48
%
% upper staff

rhfortyeightthree = {
	s1 |
  }

% central staves (4 voices)

rhfortyeightone = {
	\stemUp
	\shape #'((0 . 0) (0 . 0) (0 . 0) (0 . 0)) PhrasingSlur
	r4 r32[_\mp_\( bes c ees\) r16 f,32_\( b]
	\stemDown
	a[ c ees ges\)] ges16^. r16
	\stemUp
	\tuplet 5/4 { r32 <g,! cis e!>8_\f^~ } <g cis e>16 <e f'>_. | 
  }

rhfortyeighttwo = {
	s1 |
  }

% lower staves 

lhfortyeightone = {
	\clef treble
	\stemUp
	r4 r8 <e! f'>4_>
	\clef bass \stemDown
	\tuplet 5/4 { g,!16\tweak X-offset -3_\f^\([ a gis ais, gis'] } c8^.\) |
  }

lhfortyeighttwo = {
	s1 |
  }

lhfortyeightthree = {
	s1 |
  }

lhfortyeightfour = {
	\once \override Staff.TimeSignature.stencil = ##f
	\set Staff.forceClef = ##t
	\clef bass
	\slashedGrace { c8^~_(^>_\sfz~ } c1)^~ |
  }
