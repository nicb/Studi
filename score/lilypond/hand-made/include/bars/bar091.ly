%
% bar n.91
%

rhninetyoneone = {
		\stemDown
		f!32^\([ fis, e'\) r
		cis^\( e, dis'\) r
		b^\( d, c'!\) r
		ges!^\( f g\)] r
		c,^\([ d e\) r
		f^\( fis dis\) r
		gis^\( e fis\) r
		g^\( gis g\)] r |
  }

rhninetyonetwo = {
		s1 |
  }

lhninetyoneone = {
		\stemUp
		r16.[ a32_. r16. g32_. r16. <fis ais>32_. r16. <cis ais'>32_.]
		r16.[ <cis! a'!>32_. r16. <g' b>32_. r16. <dis b'>32_. r16. <a' cis>32_.] |
  }

lhninetyonetwo = {
		s1 |
  }
