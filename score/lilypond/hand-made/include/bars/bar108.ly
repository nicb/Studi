%
% bar n.108
%

rhonehundredandeightone = {
		r4\stopTrillSpan r8 r32 f_\([\tweak X-offset -2_\p_\markup { \italic "dolce" } d\)] r
		b_\([ g' ais,\)] r
		cis[_\( f a,!\)] r
		gis_\( fis' a,\) r
		d_\([ g! b,]\) r |
  }

rhonehundredandeighttwo = {
		s1 |
  }

lhonehundredandeightone = {
  		ees4._~ \slashedGrace { ees8 }
		r16.[ \clef bass <ais,, fis'>32]^._\p r16.[ <c dis>32]^.
		r16.[ <d! e>32]^. r16.[ <dis f!>32]^. r16.[ <dis e>32]^. |
  }

lhonehundredandeighttwo = {
        	s1 |
  }
