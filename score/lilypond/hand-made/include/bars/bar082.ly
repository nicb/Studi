%
% bar n.82
%

rheightytwoone = {
	<<
		{
			<d f aes>4 \ottava 0 r8[ \ottava 1 <c,! e gis cis>8_\mp]_~ <c e gis cis>2 |
		}
		\new Staff = "RH.2" \with { alignBelowContext = "RH" \clef treble \omit TimeSignature } {
			\global_settings
			\relative c' {
				\stemUp
				r4 \tuplet 8/9 { c16[_._\(_\mf_\< a'_. g_. ges_. a_. bes_. c_. ees_.] } aes8._-_\f |
			}
		}
	>>
  }

rheightytwotwo = {
	s1 |
  }

lheightytwoone = {
	<<
		\new Staff = "RH.2" \with { alignAboveContext = "LH" \clef treble \omit TimeSignature } {
			\global_settings
			\relative c' {
				r4 r8 <f fis a b d dis>8^~ <f fis a b d dis>2 |
			}
		}
		{
			gis32^! r32 r8. \stemUp \slashedGrace { bes8_( } \tuplet 5/4 { g!16_.)_\(\mf\<[ b,_. aes'_.\) bes,,_.\shape #'((0 . 0) (9 . -3) (-35 . 22) (0 . 6))_\( g''_.] }
			\stemDown \tupletDown
			\tuplet 5/4 { aes^.[ e^. g^. des'^. b,!^.] }  d!16^.[ a'!^. g^. des^.\)]_\f |
		}
	>>
  }

lheightytwotwo = {
    s1 |
  }

