%
% bar n.35
%

rhthirtyfiveone = {
 	r8 \slashedGrace { d'8_\f^( } cis^.)[
	\change Staff = "LH" \slashedGrace { c,!8\tweak X-offset -2_\f_( } ais!_.)] \change Staff = "RH"
	\tempo \markup { "Tempo" \circle 3 } 4 = 116
	\clef treble
	<g''! cis>8_\mp[ <g! ais> <a! c!>] \clef bass s4 \clef treble |
  }

rhthirtyfivetwo = {
	s1 |
  }

lhthirtyfiveone = {
	s4 s8
	<gis, b>8_\mp[ <ais c!> <bis d>]	
	\tuplet 6/4 { r16 b,8_\p\([ \change Staff = "RH" e\tweak X-offset -1_\p\)] r16 } |
	\change Staff = "LH"
  }

lhthirtyfivetwo = {
	s1 |
  }
