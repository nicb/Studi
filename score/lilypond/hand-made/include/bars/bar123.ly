%
% bar n.123
%

rhonehundredandtwentythreeone = {
		c,32[ bes aes g'
		b, ees d des]
		c[ bes aes f'
		c e dis a']
		ais[ d \ottava 1 a'! dis,!
		e_\f c'! gis! d'!]
		g,![ e c' gis
		a b cis, dis!] \ottava 0 |
  }

rhonehundredandtwentythreetwo = {
		s1 |
  }

lhonehundredandtwentythreeone = {
		\stemUp
		e8] e4 gis,8[ <b! cis g'!>_.]
		cis![ <f! fis! ais!>_.] g,![ \allowBreak |
  }

lhonehundredandtwentythreetwo = {
		\stemDown
		<b e>8] fis![ <a! f'!>]^. fis,4 b_\f f!8[_~ \allowBreak |
  }
