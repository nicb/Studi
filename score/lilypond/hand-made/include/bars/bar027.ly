%
% bars n.27-28-29
%

rhtwentysevenone = {
		\change Staff = "RH"

		\time 4/4
		<<
			\new Staff \with { alignAboveContext = "RH" } { 
			<<
				\relative c {
					\clef bass
					\global_settings
					\stemDown
					r8 ees~ ees2~ \tuplet 5/4 { ees8.  d8~ }                      | % bar 27

					\time 3/4
					d2.~                                                          | % bar 28

					\time 6/4
					d1~ d4. r8                                                    | % bar 29
				} \\
				\relative c {
					\global_settings
					\stemUp
					s1 | s2 e'4~                                                  | % bar 28

					e1~ e4. s8                                                    | % bar 29
				}
			>>
			}
			{
				\stemUp
				\tuplet 10/8 { r8[ d aes' r c \clef treble a'!_.  \clef bass g,! aes r8. aes16] } | % bar 27

				\tuplet 5/4 { b!8 b b b b } r4                                        | % bar 28

				r4 b8_\([ a,\)] r ees'_\f_\([ f,\) cis_._\p b''_.] r
				\slashedGrace { a,!8_\( } ges8\)
				\clef treble \slashedGrace { gis'8_\( } g'!8_.\)                      | % bar 29
			}
		>>
  }

rhtwentyseventwo = {
		s1 | s2. | s1. |
  }

lhtwentysevenone = {
		\stemNeutral
		\change Staff = "LH"
		s2 s8 \clef treble b8_.[ \change Staff = "RH" \ottava -1 d,,,,^.] \ottava 0 \change Staff = "LH" s8 | % bar 27

		c''8_.[ \change Staff = "RH" c!^.] \change Staff = "LH" s2                                | % bar 28

		\tuplet 15/12 { \tweak Y-offset 0 r4 <cis, dis'> ges' aes8 g,!4 r8.
		                <bes' c>4 dis, r16 }                                                      | % bar 29
  }


lhtwentyseventwo = {
		\clef bass
		\stemDown
		\tuplet 5/4 { a!8 f,4 r8 <c' des'>4 r \clef bass d'!4 }                                   | % bar 27

		\tuplet 15/12 { r4 <a, bes'>4 ges' c8 ees,16~ }                                  | % bar 28

		ees8 r8 s4 s1                                                                    | % bar 29
  }

