%
% bar n.117
%

rhonehundredandseventeenone = {
		<<
			{
				\stemUp
				r4 r8 r32 gis_\p[ a' ais,]
				fis[ cis' dis a \tuplet 3/2 { b16 e gis] }
				ais,32[ b gis e' \tuplet 3/2 { f!16 g,! a!] } |
			}
			\new Staff = "RH.2" \with { alignBelowContext = "RH" \clef treble } {
				\global_settings
				\relative c'' {
					\tuplet 15/12 { r16 g8[ e a,] c4 r } r4 |
				}
			}
		>>
  }

rhonehundredandseventeentwo = {
		s1 |
  }

lhonehundredandseventeenone = {
		<<
			\new Staff = "LH.2" \with { alignAboveContext = "LH" \clef bass } {
				\global_settings
				\relative c, {
					\stemUp
					\tuplet 15/12 { r16 r8 d4_\mf <b' d f!> r } r4 |
				}
			}
			{
				\stemDown
				r4 r8.[ c!16_\p]_~ c8[ \slashedGrace { gis8^(_~ } gis8^.]
				\slashedGrace { eis'8^(_~ } eis8^.[ cis^.]
			}
		>>
  }

lhonehundredandseventeentwo = {
		s1 |
  }
