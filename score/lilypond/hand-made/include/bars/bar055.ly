%
% bar n.55
%
% upper staff
rhfiftyfivethree = {
	\stemDown
	c2_~ c8[ des]\) \slashedGrace { <f, g aes>8_~ } <f g aes>4_\pp_~ |
  }

rhfiftyfiveone = {
	\stemDown
	\tweak positions #'(-13 . -14)
	\tuplet 5/4 { <des f>16[ <ges a> 
	\change Staff = "LH" \stemUp <aes, c ees>
	\change Staff = "RH" \stemDown <des ees>
	\change Staff = "LH" \stemUp <f,! ges a>] }
	<aes b! d>[
	\change Staff = "RH" \stemDown <f' a>
	\change Staff = "LH" \stemUp <bes, c des>
	\change Staff = "RH" \stemDown <d! e!>]
	<f aes>[
	\change Staff = "LH" \stemUp  <ges, g! b!>\=2)
	\change Staff = "RH" \stemDown <ees g>]\=1)
	r16 r4 |
  }

rhfiftyfivetwo = {
	s1 |
  }

lhfiftyfiveone = {
	s1 |
  }

lhfiftyfivetwo = {
	s1 |
  }

% lower staves 

lhfiftyfivethree = {
	s1
  }

lhfiftyfivefour = {
	f1~ | 
  }


