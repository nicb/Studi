%
% bar n.83
%

rheightythreeone = {
		\stemDown
		\ottava 0 \slashedGrace { e,!8_~^( } e16)[ f a,_\mf^> d]_~ \tempo \markup { "Tempo" \circle 6 } 4 = 70 d[  <c! fis cis'>]
		\clef bass
		\repeat tremolo 4 { g,32\tweak X-offset -3_\f fis, }
		\clef treble
		\ottava 1 <e'''' f'!>32^!_\ff[ r <a b'>^! r32 r32 <e f'>^! r32 <fis g'>^!] \ottava 0
		<e, a d!>16_\f[^\(^\markup { \italic "loco" } <b a'! c>] |
  }

rheightythreetwo = {
		s1 |
  }

lheightythreeone = {
		\stemDown \tupletDown
		\slashedGrace { \stemDown d'!8^(_~ } \tuplet 5/4 { d16)[ c, ees \slashedGrace { g,8_\mf\shape #'((-1 . -1.5) (-1.5 . 1) (-2 . 2) (0 . 0))^( } ges'!16) f] } r8
		\stemUp
		\repeat tremolo 4 { dis,,32\tweak X-offset -3.5_\f e' }
		<g,! gis'>32\tweak X-offset -5_\ff_![ r \ottava -1 <a, b'>_! r r <cis d'>_! r <ais f'>_!] \ottava 0
		\stemDown <b''' dis gis>16_\f[^\(_\markup { \italic "loco" } <ais dis fis>] |
  }

lheightythreetwo = {
	s1 |
  }
