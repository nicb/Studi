%
% bars n.13-14-15
%

rhthirteenone = {
	 	\tuplet 5/4 { r4 <gis, a! ais dis>_-_\mp_\startTextSpan
		<f! fis gis ais>_- <d f! fis
		gis>_-\!\stopTextSpan_\markup { \bold \italic "poco" }_\startTextSpan 
	        <dis f! d'!>_- }\stopTextSpan_\markup { \bold \italic "a" }_\startTextSpan       | % bar 13

		\tuplet 5/4 { <c a' b d>4_-
		              <c g'! gis b>_-\stopTextSpan_\markup { \bold \italic "poco" }_\startTextSpan
			      <c! ais' b cis>8_- r8
		              <d! dis fis cis'>4_-\stopTextSpan_\markup { \bold \italic "diminuendo" }_\startTextSpan
			      <dis fis a! b>_- }                                                 | % bar 14

		\tuplet 5/4 { <d fis b cis>4_-
		              <d b' c! cis!>_-\stopTextSpan_\markup { \bold \italic "sino a" }_\startTextSpan
			      <d fis b cis>_-
		              <dis e a c!>_-
			      <bes gis' a cis>_-\stopTextSpan_\pp }                              | % bar 15
  }

rhthirteentwo = {
		ges''16^.[ ees,^. d^. r] s2.                                            | % bar 13

		s1 | s1 | % bars 14-15
  }

lhthirteenone = {
		s1 | s1 | s1 | % bars 13-15
  }

lhthirteentwo = {
		<<
		  {
			<des e g>1~                                                      | % bar 13

			<des e g>4~ \slashedGrace { <des e g>8 } r4
			\slashedGrace { <bes g' aes>8\mf~ } <bes g' aes>2~               | % bar 14

			<bes g' aes>4~ \slashedGrace { <bes g' aes>8 } r2.              % | % bar 15
		}
		\new Staff \with { alignBelowContext = "LH" } {
			\relative c {
			   \global_settings
		\clef bass
		\stemUp
		r8 <ees, f'>4\mf <c' bes'>8~_\(
		\tuplet 5/4 { <c bes'>16[ f' a, ees']\) r } r8
		\clef treble a16 aes                                                                    | % bar 13

		\slashedGrace { b8~ } b16_-[ aes_. ees_. r] ges8 r8 r2 \clef bass        | % bar 14

		\stemNeutral
		r8 ges,16\mp[^\( c, f e des' f,\)]
		r8.[ b,16\p]~ \slashedGrace { b8 } r8[ f,8_>\f]                                | % bar 15
			}
		}
		
		>>
  }

