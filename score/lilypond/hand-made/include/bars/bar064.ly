%
% bar n.64
%

rhsixtyfourone = {
	\time 5/4
	\stemUp
	<e, cis' gis'>2
	\stemDown
	\repeat tremolo 8 { a''!32_\ff^\( aes,\) } s4 |
  }

rhsixtyfourtwo = {
	s1 s4 |
  }

lhsixtyfourone = {
	\stemUp
	b,4_\( gis a8\)
	\stemDown
	\repeat tremolo 6 { eis'32_\ff_\( fis'\) }
	\stemUp
	\tweak positions #'(12 . 16)
	\tuplet 3/2 { e,!8\tweak Y-offset 6^\p[ b' \change Staff = "RH" \stemDown a'] } \change Staff = "LH"
  }

lhsixtyfourtwo = {
	s1 s4 |
  }
