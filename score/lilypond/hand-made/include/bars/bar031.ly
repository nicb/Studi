%
% bar n.31
%

rhthirtyoneone = {
		\clef treble
		s4 r2 \clef bass s4
		\clef treble  |
  }

rhthirtyonetwo = {
		s1 |
  }

lhthirtyoneone = {
		\stemNeutral
		\time 4/4
		\slashedGrace { e8\tweak X-offset -3_\ff_( } \tweak Y-offset -3\ottava -1 f,!8_.) \ottava 0
		\change Staff = "RH" \slashedGrace { g''!8\tweak X-offset -4_\ff^( } gis'8^.) 
		\change Staff = "LH"
		r2 
		\clef treble
		ais,8_\f_\( \change Staff = "RH" b,,\tweak X-offset -3_\f\)
		\change Staff = "LH"  |
  }

lhthirtyonetwo = {
              s1 |
  }

