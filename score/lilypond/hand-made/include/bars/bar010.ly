%
% bar n.10
%

rhtenone = {
		\tuplet 5/4 { <e a ais b>4_- <d b' c dis>_- <d f ges ees'>8_- }\!\mf
		\tuplet 3/2 { <g ais>4_- d'4_- <f,! a!>4_- }                            | % bar 10
  }

rhtentwo = {
		s1                                                                      | % bar 10
  }

lhtenone = {
		s1                                                                                 | % bar 10
  }

lhtentwo = {
		<f g aes>16[ <dis, fis'>16^-\f r <gis~ a'!~>^-] \slashedGrace { <gis a'>8 }
		<cis~ e~ ais~>4\mf <cis e ais>4 <fis gis a>                 | % bar 10
  }

