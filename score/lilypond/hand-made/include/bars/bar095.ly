%
% bar n.95
%

rhninetyfiveone = {
		\stemDown
		cis32\)[ r f!^\( gis, g'!\) r c,!^\( a ais\) r c^\( a b\) r dis^\( fis,]
		f'!\)[ r ais,^\( g! gis\) r a!^\( fis! gis!\) r d!^\( c' dis,\) r g!^\( e'] |
  }

rhninetyfivetwo = {
		s1 |
  }

lhninetyfiveone = {
		\stemUp \clef treble
		r32\tweak positions #'(3 . 3)[ dis_. r16 r32 e_. r16 r32 e_._\markup { \italic "a" } r16 r32 cis_._\f] r16
		r32 d!_.\tweak positions #'(3 . 3)[ r16 r32 cis_. r16 r32 ais_._\markup { \italic "poco" } r16 r32 b_.] r16 |
  }

lhninetyfivetwo = {
		s1 |
  }
