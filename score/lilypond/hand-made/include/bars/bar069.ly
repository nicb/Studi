%
% bar n.69
%

rhsixtynineone = {
		\time 4/4
		\stemUp
		<a gis' bes>2 \slashedGrace { <dis gis f'>8_\ff_(^~ } <dis gis f'>4)
		cis'16\tweak X-offset -3_\p g!8. |
  }

rhsixtyninetwo = {
	s1 |
  }

lhsixtynineone = {
	\stemUp
	\slashedGrace { cis'8_\p_(^~ } cis4)
	f8.[ e,16_\ff]^~ e8[ ais'16_\p e']
	\change Staff = "RH" \stemDown
	a16[ fis
	\change Staff = "LH" \stemUp
	f,! dis'] |
  }

lhsixtyninetwo = {
	s1 |
  }
