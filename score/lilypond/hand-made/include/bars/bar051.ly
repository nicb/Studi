%
% bar n.51
%
% upper staff
rhfiftyonethree = {
	<c g' cis>4 r8 \ottava 0 \ottava 1 <ees ges e'!>4\tweak X-offset -8^\ppp r8 \ottava 0 |
  }

rhfiftyoneone = {
        \time 3/4
	\stemUp
	\tuplet 4/3 { g!16\)[ gis b f'] } f16\tweak Y-offset -3.5_\mf\tweak X-offset 3_\markup { \italic "cresc." }
	\slashedGrace { ges8_( } \tuplet 6/4 { ees16)
        \shape #'((0 . 1) (0 . 2) (0 . 2) (0 . 0.5)) PhrasingSlur
	^\(\tweak positions #'(6 . 8)[ gis a \tweak Y-offset 5 r b c]}
	\slashedGrace { ees8_( } \tuplet 3/2 { d16) \arpeggioArrowUp e!\arpeggio \arpeggioArrowDown d\) \arpeggio } s8 |
  }

rhfiftyonetwo = {
	s4 \tuplet 6/4 { <b d>16[ r8 r16 d r16] }
       \set PianoStaff.connectArpeggios = ##t
        \tuplet 3/2 { r16\tweak positions #'(-3 . -3)[ \arpeggioArrowUp <fis g>\arpeggio \arpeggioArrowDown <b,! a' c>]_~\arpeggio } <b a' c>8_~ |
  }

lhfiftyoneone = {
 	\stemUp
	s4. \tuplet 5/4 { r32 g,! r16. } s4 |
  }

lhfiftyonetwo = {
        \stemDown
        \clef bass
	\tuplet 4/3 { cis'8[ a] } r16
	<g bes c>8^-\tweak X-offset -4_\mf[ \slashedGrace { bes,8_\pp_( } \tuplet 5/4 { des,32)_\( ges, g!\)] r16 }
	r16 <des''! g! bes!>\tweak X-offset -3_\f^- r8 |
  }

% lower staves 

lhfiftyonethree = {
        \once \override Staff.TimeSignature.stencil = ##f
	\stopStaff s2. |
  }

lhfiftyonefour = {
  	<d e'>4 r2 |
  }
