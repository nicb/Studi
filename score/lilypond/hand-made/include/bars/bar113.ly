%
% bar n.113
%

rhonehundredandthirteenone = {
		cis32[ f,! g e' c'! a, ges f
		cis' b a c! gis' g,! ais d!
		fis b, a! g f'! fis,! ais! d!
		cis! gis! ais! gis! fis'! d f,! a!] |
  }

rhonehundredandthirteentwo = {
		s1 |
  }

lhonehundredandthirteenone = {
		dis8_.[ gis_. dis_. e_. c_. dis_. e_. e_.] |
  }

lhonehundredandthirteentwo = {
		s1 |
  }
