%
% bar n.124
%

rhonehundredandtwentyfourone = {
		\stemDown
		d!32[ des f,! a!
		gis! fis! e dis!]
		g![ f! dis e
		c' b d! fis!]
		ais,![ d ais! gis'
		c,_\markup { \italic "dim." } a! b fis'!]
		d![ ees! c f!
		g, a b cis] |
  }

rhonehundredandtwentyfourtwo = {
		s1 | 
  }

lhonehundredandtwentyfourone = {
                \stemUp
		<dis'! fis! ais!>8_.] c![ <g' a! b>_.]
		<cis, gis'>_-[ r8]
		\clef treble
		<e' g!>_.[ <cis! dis! f!>_.]
		<e gis!>_. |
  }

lhonehundredandtwentyfourtwo = {
		\stemDown
		f8] a4 s8 s2 |
  }
