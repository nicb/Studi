%
% bar n.88
%

rheightyeightone = {
		<<
			{
				\stemUp
				<bes e des'>8.[ f''16_\ff_-]^~ f4
				g,!32_\(^\markup { \italic "sempre leggero" }[ e' f,\) \tweak Y-offset 0r
				bes_\( a c!\) \tweak Y-offset 0r g_\( a ais\) \tweak Y-offset 0r fis_\( e'! gis,\)] \tweak Y-offset 0r |
			} 
			\new Voice {
				\global_settings
				\relative c'' {
					\stemUp
					s4. gis8_- s2 |
				}
			}
		>>
  }

rheightyeighttwo = {
		\stemDown
		s4 g4^- s2 |
  }

lheightyeightone = {
		\stemUp
		g8. s16 s2. |
  }

lheightyeighttwo = {
		\stemDown
		\once \omit NoteHead
		\once \omit Stem
		g'8.[ <cis ais'>16]^-_\markup { \dynamic "p" \italic "ma marcato" }
		r16[ <b c'!>^.] r8
		r16.[ <fis' cis'>32^. r16. <fis cis'>32^. r16. <e d'!>32^. r16. <b' cis>32^.] |
  }

