%
% bar n.94
%

rhninetyfourone = {
		\stemUp
		\tuplet 5/4 { gis32_\([ d ais'\) r a_\( }
		\tuplet 5/4 { d, b'\) r16 fis32_\(] }
		\tuplet 5/4 { c32[ ais'\) r dis,] r } e16_.\tweak positions #'(6 . 6)[ r]
		s8 \tempo \markup { "Tempo" \circle 6 ", ben marcato" } 4 = 82
		dis32[ r g^\( a,]
		\stemDown
		ais'\)[ r d,^\( b c\) r d^\( b] |
  }

rhninetyfourtwo = {
		s1 |
  }

lhninetyfourone = {
		\stemDown
		\clef treble
		\slashedGrace { d'8_~^( } \tuplet 5/4 { d32)[ r16 c32^. r }
		\tuplet 5/4 { r16 g!32^.] r16 }
		\tuplet 5/4 { r16 d'!32 r \clef bass f,, }
		<gis, b a'>32^>^\([ \change Staff = "RH" dis'' gis, b] fis'[ a gis\)] r
		\change Staff = "LH" s4. |
  }

lhninetyfourtwo = {
		\stemDown
		s2 r16[ <e, dis'>^.] r32[ f'!^._\markup { \italic "cresc." }] r16
		\clef treble \stemUp r32[ fis'_. r16 r32 fis_.]_\markup { \italic "poco" } r16 |

  }
