%
% bar n.75
%

rhseventyfiveone = {
	\stemUp
	\tempo \markup { "Tempo" \circle 2 ", senza affrettare" } 4 = 70
	r4 <dis,,, fis g e'>2. |
  }

rhseventyfivetwo = {
	s1 |
  }

lhseventyfiveone = {
	\clef bass
	\stemUp
	s2 \tuplet 3/2 { r8 <a bes c>4_\pp^~ } <a bes c>4
  }

lhseventyfivetwo = {
	<<
		{
			\stemDown
			\clef bass
			\slashedGrace { cis'8_\mp_~^( } cis4) r8 \slashedGrace { gis,8_~^( } gis8)_~ \slashedGrace { gis8 }
		  fis4_~ fis8[ \slashedGrace { cis8_\mp_~^( } cis8)] |
		}
		\new Staff \with { alignBelowContext = "LH" \clef bass } {
			\global_settings
			\relative c,, {
				\stemDown
				r2 \tuplet 3/2 { r8 \ottava -1 <b cis d>4_~ } <b cis d>4 \ottava 0 |
			}
		}
	>>
}
