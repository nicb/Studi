%
% bar n.37
%

rhthirtysevenone = {
	fis2.
	\tempo \markup { "Tempo" \circle 2 "trattenendo un poco" } 4 = 69
	\tuplet 4/3 { r16 c,!_\f_\([ e bes'\)] } s16 | 	
  }

rhthirtyseventwo = {
	<< 
		{\voiceThree \stemUp <a c>8[ <e b'> <e gis> <a! c!> <a c> <dis, gis>] }
		\new Voice {\voiceFour \stemDown r4 \tuplet 3/2 { r8 cis4~ } \slashedGrace { cis8 } s4 }
	>> \oneVoice
	\stemUp s8. <d e'>16^\( \stemNeutral |
  }

lhthirtysevenone = {
	<gis b>8[ <g! a> <d f> <g ais> <gis a> <d f>]
	\clef bass \stemUp r16 \slashedGrace { cis8~_( } cis16)[
	\arpeggioArrowUp <f, gis a g'! a>16\arpeggio % from pp to f
	\arpeggioArrowDown <g! cis d c'!>]\arpeggio | % from f to p to\f
	\stemNeutral
  }

lhthirtyseventwo = {
	s1 |
  }

