%
% bar n.111
%

rhonehundredandelevenone = {
		<<
			{
				\stemUp
				des32[ c b' bes
				e, d! cis' g]
				ges[ f] \clef treble s8 \tieSolid des''16^~ des2 |
			}
			\new Voice {
				\global_settings
				\relative c' {
					\stemUp
					s4 s8 f!_-^~ f2 |
				}
			}
		>>
  }

rhonehundredandeleventwo = {
		<<
			{
				\stemDown
				s4 s16 c,8._\f^-_~ c2 |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemDown
					s2.. <a' d! e bes'>8^-_\ff |
				}
			}
		>>
  }

lhonehundredandelevenone = {
		e16 gis,8 a'16
		\stemDown \tieSolid
		r16[ fis'8.]^-_~ fis2 |
  }

lhonehundredandeleventwo = {
		 <<
		 	{
				\stemDown
				s4 s8. <bes b'>16^-_~ <bes b'>2 |
			}
			\new Voice {
				\global_settings
				\relative c' {
					s2.. <a d! e g>8^-_\ff |
				}
			}
		 >>
  }
