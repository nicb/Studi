%
% bar n.77
%

rhseventysevenone = {
	\time 8/4
	\stemUp
	<b gis' c des>8 \clef bass \slashedGrace { <a bes c!>8_(^~ } <a bes c>4)
	\slashedGrace { <b,, c'>8_(^~ } <b c'>8)^~ \slashedGrace { <b c'>8 }
	<dis, e'>8.[ \clef treble <ais'''' g' aes>16]^~ <ais g' aes>4
	\clef bass \slashedGrace { <a, bes c>8_(^~ } <a bes c>2.)^~ <a bes c>8^~
	\slashedGrace { <a bes c>8 } r8 |
  }

rhseventyseventwo = {
	s1 s1 |
  }

lhseventysevenone = {
	s1 \slashedGrace { \stemDown <e f'!>32_([ <dis fis'> \ottava -1 <b,, cis d>]^~ }
	\stemUp <b cis d>2._\pp_)^~ <b cis d>8[ \ottava 0 \slashedGrace { <c'! d'>_(^~_\markup { \italic "loco" }_\mp} <c d'>8)]^~ |
  }

lhseventyseventwo = {
	\tuplet 3/2 { <ais g'>16[ <e f'> \ottava -1 <b, cis d>_\pp]_~ } <b cis d>4_~ \slashedGrace { <b cis d>8 } \ottava 0 r8
	r8.[ \slashedGrace { <g''! gis'>8^( } <a! fis'>16])_~ <a fis'>4 s1 |
  }

