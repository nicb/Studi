%
% bar n.5
%

rhfiveone = {
		\stemNeutral
		cis2~ cis8. s16 s4 | % bar 5
  }

rhfivetwo = {
		s2 s8. r16 \clef bass \tuplet 5/4 { f,16\f g gis a g } | % bar 5
  }

lhfiveone = {
		s2 s8 \tieSolid \once \set tupletFullLengthNote = ##t \tuplet 3/2 { r8 ees16\mf~ }
		\tuplet 10/8 { ees32[ a,16 <c, d'> r32 ais''16 g] }                                             | % bar 5
  }

lhfivetwo = {
		e2~ e8. s16 s4 | % bar 5
  }

