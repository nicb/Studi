%
% bar n.90
%

rhninetyone = {
	\stemUp
	c32_\([ d c\) r
	gis_\( fis' f,\) r
	cis'_\( e dis\) r
	b_\( d b\)] r
	\stemDown
	dis^\([ e cis\) r
	f^\( fis gis\) r
	c!^\( c, ais'\) r
	fis'^\( g,! e'\)] r |
  }

rhninetytwo = {
		s1 |
  }

lhninetyone = {
		\stemUp
		r16.[ ais32_. r16. a32_. r16. g32_. r16. g32_.]
		r16.[ a32_. r16. ais32_. r16. gis32_. r16. b32_.] |
  }

lhninetytwo = {
		s1 |
  }
