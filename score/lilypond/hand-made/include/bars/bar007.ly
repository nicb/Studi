%
% bar n.7
%

rhsevenone = {
		\time 3/4
		% \once \override padding = #5
		\tempo \markup { "Tempo " \circle 1 ", trattenendo molto" } 4 = 36
		\tweak positions #'(12 . 12)\tuplet 5/4 { \change Staff="LH" \slashedGrace { gis,32_([ fis'] } gis8)\tweak X-offset 2\tweak Y-offset 0\p\tweak positions #'(1 . 1)_\>
			\change Staff="RH" \stemDown <a' b>\! \tweak Y-offset 0 r16 } s4 \crossStaff { fis16-> } r8. | % bar 7
  }

rhseventwo = {
		s2. | % bar 7
  }

lhsevenone = {
		\stemDown
		\slashedGrace { \stemDown cis32_( dis } cis!8) r \slashedGrace { cis!32\pp\>( g'? gis!\!) } r8
		\tupletDown \tuplet 3/2 { d16\tweak X-offset -3\p \stemUp \slashedGrace { <ais gis'>8\shape #'((-1.5 . -16)(-2 . -2)(0 . 0)(0 . 0))^(} \stemDown
		\change Staff="RH" { \offset Y-offset 1 \ottava 1 b'''''16) } \ottava 0 \change Staff="LH" <c,,, dis>\tweak X-offset 2\f }
		\crossStaff { a16 } r16 s8 \stemNeutral\tupletNeutral                              | % bar 7
  }

lhseventwo = {
		s2 s8 \tuplet 3/2 { c'16\pp[ \slashedGrace { d,8_( } e16) \tweak Y-offset -4 r32 a,32\mp]~ } | % bar 7
  }
