%
% bar n.17
%

rhseventeenone = {
		\tuplet 2/3 { gis,16_-[ <cis e>_-] } <bis dis>16
		<ais e'>8[ \clef treble d''!32_\pp_\( a gis ais] f![ gis]\) r8.
		\slashedGrace { <fis ais cis g'!>8_\p~ } <fis ais cis g'>8_\([ d'\)]~                   | % bar 17
  }

rhseventeentwo = {
		s2 r16 b,,8\f\tweak Y-offset -1.5^_ r16 s4                                              | % bar 17
  }

lhseventeenone = {
		g!2_-~ \slashedGrace { g8 } f''!8\tweak Y-offset 1\p\)^\([ e8]
		\slashedGrace { <gis ais>8~ } <gis ais>8[\)^\( cis\)]~                                  | % bar 17
  }

lhseventeentwo = {
		s1 |
  }
