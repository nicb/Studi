%
% bar n.98
%

rhninetyeightone = {
		\stemUp
		dis,32\)[ r e_\( d' f,\) r e_\( d' f,\) r ais_\( g' gis,\) r b_\( f']
		gis,\)[ r ais!_\( g'! a,!\) r dis,_\( cis'] e,\)\tweak positions #'(6 . 6)[ r e r bis8_-_\ff] |
  }

rhninetyeighttwo = {
		s1 |
  }

lhninetyeightone = {
		\stemUp
		r32[ <c gis>_._\markup { \italic "sino" } r16 r32 <cis ais'>_. r16 r32 <d fis>32_. r16 r32_\markup { \italic "a" } <c!  a'>] r16
		r32[ <d fis>_. r16 r32 <cis b'>_.] r16 r32[ <b g'!>_._\ff] s8. |
  }

lhninetyeighttwo = {
		\stemDown
		s2. r16 a,^\(\tweak positions #'(-4 . -3)[ cis dis\)] |
  }
