%
% bar n.63
%

rhsixtythreeone = {
	\time 9/8
	a8[ b16 c] \tuplet 3/2 { g8[ gis] r }
	\tempo \markup { "Tempo" \circle 3 } 4 = 116
	\stemDown
	<e' ges>32\tweak Y-offset -6_\markup { \italic "sempre" \dynamic "p" }^\(\tweak positions #'(-2 . -6)[ f, e' <d, ees> d' c, <cis' d> bes,
	ees' <c,! des> b'! aes, <fis' g> ges, e'! <ees, f> des' d, <b'! c> des,\)]
  }

rhsixtythreetwo = {
	<b ais'>8.[ f16] \tuplet 3/2 { b8[ <a fis'>4] } s2 s8 |
  }

lhsixtythreeone = {
	f8.[ e16] \tuplet 3/2 { e8[ f!4]\) }
	\clef bass
	\stemDown
	c,,16^!\tweak positions #'(-7 . -4)[ \slashedGrace { des''8_( } aes16^!)
	des,,16^! \slashedGrace { des''!8_( } c16^!)
	e,,16^! \slashedGrace { des''!8_( } c16^!)
	f,,16^! \slashedGrace { des''!8_( } c16^!)
	bes,16^! \slashedGrace { des'!8_( } c16^!)]
  }

lhsixtythreetwo = {
	s1 s8 |
  }

