%
% bar n.52
%
% upper staff
rhfiftytwothree = {
	s8 <cis, e d'!>4_\ppp \arpeggioArrowUp <cis ais' dis>8_\pp_~\arpeggio <cis ais' dis>2 |
  }

rhfiftytwoone = {
	\time 4/4
	s2 \slashedGrace { ees,8_\f_( }
	\tuplet 6/4 { d16)[
	\shape #'((-4 . 1) (0 . 0) (0 . 0) (0 . 0.5)) PhrasingSlur ^\( des c b aes8] }
	\slashedGrace { bes8_( } \tuplet 6/4 { c16)[ bes b bes a! b!]\) }
  }

rhfiftytwotwo = {
	\stemDown
	<b a' c>8 r4. \tuplet 6/4 { r16\tweak positions #'(-6 . -7) [ <a bes!> r r <ees g> r16] }
	\tuplet 3/2 { r8[ <ees g>16] } r8 |
  }

lhfiftytwoone = {
	\stemUp
	\clef treble
	r16 c'32_\ppp_\([ bes~ bes gis' b c!\)] r8 \clef bass <a,, c b'>8_\pp^~ <a c b'>2 |
  }

lhfiftytwotwo = {
	\stemDown
	s2 r16\tweak positions #'(-8 . -6)[ g,_\f^. \tweak Y-offset 1 r <d' ges a>^.] \tweak Y-offset 1 r <d a'>^.[ \tweak Y-offset 1 r8]
  }

% lower staves 

lhfiftytwothree = {
        \once \omit Staff.TimeSignature
	\stopStaff s1 |
  }

lhfiftytwofour = {
        \once \omit Staff.TimeSignature
        \once \omit Staff.Clef
	\stopStaff s1 |
  }

