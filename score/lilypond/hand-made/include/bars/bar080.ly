%
% bar n.80
%

rheightyone = {
		<d f ges>4.[ \ottava 0 \slashedGrace { <bes,,, f' aes b!>8^~_( } <bes f' aes b>8_\mp])^~ <bes f' aes b>2^~
  }

rheightytwo = {
                \clef treble
		s1 |
  }

lheightyone = {
	<<
		\new Staff = "LH.2" \with { alignAboveContext = "LH" \clef treble \omit TimeSignature } {
			\global_settings
			\relative c' {
	          \stemDown
                    gis32^![ r] r8.
                   \stemUp
		    r4 \tweak positions #'(9 . 1.75)\tuplet 12/8 { r8 d''16_\mp_\(\tweak positions #'(6 . 0)[ c b e, ees bes
		               \change Staff = "LH" aes\)_\( f ees ges\)] } |
		  }	
		}
		{
		  \clef bass
		  s1 |
		}
	>>
  }

lheightytwo = {
    s1 |
   }
