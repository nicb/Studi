%
% bar n.4
%

rhfourone = {
		\tuplet 5/4 { b16_\f[ ais \tweak Y-offset 1.2 r b c] } r4 \tuplet 5/4 { r8. b16[ e,] }
		\tuplet 6/4 { fis16 \tieDashed cis4~\fff cis16~ } |
  }

rhfourtwo = {
		r16 <d' e,>8.\mf <ees, d' f>16\mf[ <ees c'>8 bes16] <g'! ges>16[ <f aes>] r8 s4 |
  }

lhfourone = {
		r4 a,16\p[ r8 f16_\(] fis'[ bes,\) r d32_\( f,] \tuplet 6/4 { d'16\) s16 s4 } \slurNeutral |
  }

lhfourtwo = {
		dis16 cis\p r8 s2 \tuplet 6/4 { r16 \tieDashed e?4~ e16~ } |
  }

