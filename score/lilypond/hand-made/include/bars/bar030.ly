%
% bar n.30
%

rhthirtyone = {
                \clef bass
                \stemNeutral
		r8 \slashedGrace { f!8^\( } dis,^.\tweak X-offset -3_\f\)[
		\change Staff = "LH" \slashedGrace { c,!8\tweak X-offset -3_\f_\( } a'!_.\)]
		\change Staff = "RH" r8 d!\tweak X-offset -2_\p^.[ \change Staff = "LH" cis_._\p]
		\change Staff = "RH" r8 <ais gis' b>^>\tweak X-offset -3_\ff[
		\change Staff = "LH" <f g! e'>\tweak Y-offset -4_\ff_>]
		\change Staff = "RH" fis\tweak X-offset -3_\f
		\change Staff = "LH" d'_\p \change Staff = "RH" r8                                     | % bar 30
  }

rhthirtytwo = {
		s1. |
  }

lhthirtyone = {
		s1. |
  }

lhthirtytwo = {
		s1. |
  }

