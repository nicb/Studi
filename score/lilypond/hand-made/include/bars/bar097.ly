%
% bar n.97
%

rhninetysevenone = {
		\stemUp
		gis,32\)[ r ais_\( g' a,!\) r dis,_\( c' e,\) r ais_\( d! fis,\) r f!_\( des']
    ees,\)[ r d!_\( c' e,!\) r f_\( cis' a\) r dis,_\( b' fis\) r dis!_\( b'] |
  }

rhninetyseventwo = {
		s1 |
  }

lhninetysevenone = {
		\stemDown
		r32[ <fis' d'>^. r16 r32_\markup { \italic "poco" } <cis b'>^. r16 r32 <b gis'>^. r16 r32 <g! a'!>^.] r16
		r32_\markup { \italic "a" }[ <e' c'!> r16 r32 <fis! gis!>^. r16 r32 <g! ais>^. r16 r32 <g,!  a'!>^.] r16_\markup { \italic "poco" } |
  }

lhninetyseventwo = {
		s1 |
  }
