%
% bar n.103
%

rhonehundredandthreeone = {
		<<
			{
				\stemDown
				dis''32[_\f cis dis f, e' ais, c! b,
				g''! cis, fis,! fis! cis'! dis! gis,! a!
				d! gis! cis,! dis,! ais'! b c! d!]
				\stemUp
				d4_\ff^-^~ |
			}
			\new Staff = "RH.2" \with { alignBelowContext = "RH" \clef treble \omit TimeSignature } {
				\global_settings
				\relative c' {
					\stemDown
					r2. r8[ \slashedGrace { \stemDown d'!8_~^( } d8)]^-\tweak X-offset 0_\ff_~ |
				}
			}
		>>
  }

rhonehundredandthreetwo = {
		\stemDown
		s2. r8[ f'^-_~] | 
  }

lhonehundredandthreeone = {
		<<
			\new Staff = "LH.2" \with { alignAboveContext = "LH" \clef bass \omit TimeSignature } {
				\global_settings
				\relative c {
					<<
						{
							\stemUp
							<cis b'>8_\(_\f[ <c gis'> <dis, e'> <ais' b'> <ais! b'> <a! g'!>]\) f'4_-^~ |
						}
						\new Voice {
							\global_settings
							\relative c {
								\stemDown
								s2. e,4\tweak X-offset -4_\ff^-_~ |
							}
						}
					>>
				}
			}
			{
				<<
					{
						\stemUp
						r2. r16[ \slashedGrace { gis8^~_( } g8.)]\tweak X-offset 0_\ff_-^~ |
					}
					\new Voice {
						\global_settings
						\relative c {
							\stemUp s2.. a8^~ |
						}
					}
				>>
			}
		>>
  }

lhonehundredandthreetwo = {
		\stemDown
		s2. \tweak Y-offset -4r8[ des,,8]_-_~ |
  }
