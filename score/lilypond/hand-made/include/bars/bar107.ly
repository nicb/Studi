%
% bar n.107
%

rhonehundredandsevenone = {
		r4 r8. \slashedGrace { b,8_(\tweak X-offset -2^\ff^~ }
		\tieDashed \pitchedTrill b16)^~\startTrillSpan cis
		\once \override Hairpin.circled-tip = ##t
		\after 4.. \! b2_\>
  }

rhonehundredandseventwo = {
		s4 c32_\f[ e g] \tweak Y-offset -2 r c,[ gis' a] \tweak Y-offset -2 r s2 |
  }

lhonehundredandsevenone = {
		g8.[ a,16]\stopTrillSpan r16. \clef bass e32 r8
		r8 \clef treble
		\after 4 \ff
		ees'4._~_\pp\< |
  }

lhonehundredandseventwo = {
		s1 |
  }

