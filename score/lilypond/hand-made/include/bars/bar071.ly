%
% bar n.71
%

rhseventyoneone = {
	\stemUp
	s16 g,!8.^\( fis16\) r8. r4 r16 f'!8. |
  }

rhseventyonetwo = {
	\stemDown
	fis16[ f,,! ais, cis] e16 s8. s4
	\slashedGrace { b'32 cis dis } fis16[ e d b] |
  }

lhseventyoneone = {
	s1 |
  }

lhseventyonetwo = {
	<b e'>16[ <gis a'>8 c'!16_~^\(] c8.[ gis16_~\)] gis4
	\stemUp
	<a, c'>16_>[ <gis g'!>^~ \slashedGrace { <gis g'!> } cis8~]
  }
