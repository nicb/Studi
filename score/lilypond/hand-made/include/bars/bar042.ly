%
% bar n.42
%

rhfortytwoone = {
	\time 3/4
	\tuplet 10/8 { a!32\)_\([ f d' b! r a fis dis' cis b] }
	\tuplet 10/8 { ais[ e' a! cis, e fis gis b\)] r16 }
	\tempo \markup \left-column { \line {"Tempo" \circle 3 "," } \line { "ben marcato" } } 4 = 120
	\stemDown
	\slashedGrace { <cis, fis a! d>8^>_\f~ } <cis fis a dis>4^-~ |
}

rhfortytwotwo = {
	s2. |
  }

lhfortytwoone = {
	\stemDown \tupletDown
	\tuplet 5/4 { ges16_\<[ e!8^. f!^. gis^. dis^.\)] r16 }  
	\stemUp
	\set PianoStaff.connectArpeggios = ##t
	\arpeggioArrowDown \slashedGrace { e8~\arpeggio } e8\f r  |
  }

lhfortytwotwo = {
	\stemDown
	\arpeggioArrowDown
	s2 \shiftOn \slashedGrace { ais,8~\arpeggio  } ais8[ b']~ \shiftOff |
  }
