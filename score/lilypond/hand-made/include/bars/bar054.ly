%
% bar n.54
%
% upper staff
rhfiftyfourthree = {
	<c f cis'>4_~ \slashedGrace { <c f cis'>8 } <f, d' g>2_\pp
	\tuplet 5/4 { gis'4^\(_\mp[ c,16_\p_]~ } | 
  }

rhfiftyfourone = {
	\change Staff = "LH" \stemUp <bes'''' c! des ees>16[
	\change Staff = "RH" \stemDown <b'! d!> <f aes>
	\change Staff = "LH" \stemUp <ees e! ges g!>]
	\change Staff = "RH" r2 \stemDown \tupletDown \tuplet 5/4 { r16 <b'! c>16_\ppp^\=1(
	\change Staff = "LH" \stemUp <c, ees! a>_\=2(
	\change Staff = "RH" \stemDown <ges' b> s } |
  }

rhfiftyfourtwo = {
	s1 |
  }

lhfiftyfourone = {
	s1 |
  }

lhfiftyfourtwo = {
	s1 |
  }

% lower staves 

lhfiftyfourthree = {
	s1 |
  }

lhfiftyfourfour = {
	<d b'>4^~ \slashedGrace { <d b'>8 } <a' e' cis'>2_\pp^~ \stemDown \tupletUp \tuplet 5/4 { <a e' cis'>4[ f'''16]^~ }	
  }
