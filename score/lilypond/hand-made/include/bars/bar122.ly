%
% bar n.122
%

rhonehundredandtwentytwoone = {
		\stemDown
		ges'32[ f gis e
		fis ais, d! des!]
		b[ a! g! fis'!
		c! e, g cis]
		b[_\markup { \italic "cresc." }  ais d! b'
		ais,! c! d cis]
		f,[ fis dis' g
		b ees d des] |
  }

rhonehundredandtwentytwotwo = {
		s1 |
  }

lhonehundredandtwentytwoone = {
		\stemUp
		s4 r8 gis4 g e8[^~ |
  }

lhonehundredandtwentytwotwo = {
		\stemDown
		<d ais'! e'>8^.]
		<cis'! d>^.\tweak positions #'(-3 . -3)[ r8]
		a\tweak positions #'(-2. . -3)[ <f! dis'>^.]
		aes\tweak positions #'(-3 . -3)[ <a! e'>^.]
		fis[ \allowBreak |
  }
