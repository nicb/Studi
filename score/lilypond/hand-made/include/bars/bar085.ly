%
% bar n.85
%

rheightyfiveone = {
	\stemUp
	<a! c! ais'!>8\)_-[ f!_._\markup { \italic "dim."}] gis,_.[ d'_. g,!_. cis_.]
	r8 <a'! c! a'!>16_\([ <gis cis ais'>] |
  }

rheightyfivetwo = {
	\stemDown
	s2. c,!4^-^> |
  }

lheightyfiveone = {
	\stemDown
	<cis fis b>8\)^-[ e^.]
	\stemUp
	<g,,! gis'>8_.[ f''!_. <fis,, e'>_. dis''_.]
	r8 <dis fis b>16_\( <d! b' e> |
  }

lheightyfivetwo = {
	\stemDown
	s2. <a,,! ais'>4^-^>
  }
