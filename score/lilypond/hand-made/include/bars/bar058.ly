%
% bar n.58
%
% upper staff
rhfiftyeightthree = {
         \stopStaff   s1 |
        
  }

rhfiftyeightone = {
      \stemUp
      s4. \arpeggioArrowUp <g''! e' gis ais>8^~\arpeggio <g e' gis ais>2 |
  }

rhfiftyeighttwo = {
    s1 |
  }

lhfiftyeightone = {
	s1 |
  }

lhfiftyeighttwo = {
	<f d' fis gis>4.\tweak X-offset 3\tweak Y-offset 6^\markup { \override #'((thickness . 2)) \draw-dashed-line #'(3 . 4) } s8 s2 |
  }

% lower staves 

lhfiftyeightthree = {
	s1 |
  }

lhfiftyeightfour = {
	cis8 gis'4 cis gis! g'!8
  }
