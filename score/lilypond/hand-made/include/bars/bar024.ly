%
% bar n.24
%

rhtwentyfourone = {
		s8 r8 d8\tweak X-offset -3_\f_\([ \change Staff = "LH" c''_\f\)]
		\change Staff = "RH" \clef treble cis'8\tweak X-offset -3_\p^.[
		\change Staff = "LH"
		\clef bass \ottava -1 dis,,,,8\tweak X-offset -3_\p_.] \ottava 0 s2 \change Staff = "RH" | % bar 24
  }

rhtwentyfourtwo = {
		s1 s4 |
  }

lhtwentyfourone = {
		\time 5/4
		\change Staff = "RH" fis,,,8\tweak X-offset -3\tweak Y-offset -4_\p\)]
		\change Staff = "LH" s8 s2 \clef bass
		r8 \slashedGrace { d'!8_\(_\ff } fis,_.\)[
		\change Staff = "RH" \slashedGrace { g''8_\ff^\( } a'^.\)] r8 \change Staff = "LH"      | % bar 24
  }

lhtwentyfourtwo = {
		s1 s4 |
  }

