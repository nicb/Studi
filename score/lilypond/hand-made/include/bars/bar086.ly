%
% bar n.86
%

rheightysixone = {
		<f! cis' g'!>8_-\)[ c!_.] r8 \tempo \markup { "Più agitato e marcato" } 4 = 76
		<c d! g>32\tweak X-offset -3_\fp_\markup { \italic "leggero" }[_\( b' a,\)] r
		\slashedGrace { <bes e des'>8^~_(_\ff } <bes e des'>8)[_-^~ \slashedGrace { <bes e des'>8 } dis'_-]_~
		<dis a'!>16[ r gis,32_\(\tweak X-offset -4_\fp_\markup { \italic "leggero" } b' gis,\)] r |
  }

rheightysixtwo = {
		s4 a4.^-^> r8 s4 |
  }

lheightysixone = {
		<dis ais' e'>8_-\)[ b_.] r8 <dis f>16[_\markup { \italic "marcato" } \tweak Y-offset 0 r] \slashedGrace { g,!8_(^~ } g8)[_-^~ \slashedGrace { g8 } r8]
    \stemDown
		r16[ e'']^._\markup { \italic "marcato" } r[ g!]^. |
  }

lheightysixtwo = {
		s4 <fis gis'>4.^-^> \tweak Y-offset -4 r8 s4 |
  }
