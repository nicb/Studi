%
% bar n.39
%

rhthirtynineone = {
	e4~ \tuplet 3/2 { e8 f4~ } f4 \stemDown a4_\f~ |
  }

rhthirtyninetwo = {
	<<
		{ \stemDown \tuplet 6/4 { dis8[ a'16^. b8^. r16] } 	
			r8 \tuplet 6/4 { r8[ ais16 r16 d!8^>] } r8 s4 | }
		\new Staff = "RH2" \with { alignBelowContext = "RH" } {	
			\relative c {
			    \global_settings
			    \override Staff.TimeSignature.stencil = ##f
			    \clef bass
			    r2 r8 \slashedGrace { dis,8~_\( } dis16 a'\) r4
			}
		}
	>> |
  }

lhthirtynineone = {
	\tuplet 3/2 { d16\![ gis8\>] }
	\tuplet 6/4 { r16\! gis,8_.[ c!_.\)_\(] r16 }
	\stemDown
	\tuplet 6/4 { r16[ <a gis'>16 r16 r dis^.\) r16] }
	\tuplet 8/6 { des'16_\f[ bes ges bes des e ees c] } |
	\stemNeutral
  }

lhthirtyninetwo = {
	s1 |
  }

