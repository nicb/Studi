%
% bar n.56
%
% upper staff
rhfiftysixthree = {
       \set PianoStaff.connectArpeggios = ##f
	<f g aes>2 \arpeggioArrowUp \slashedGrace { <fis, dis' g>8_~\arpeggio } <fis dis' g>2^~
	<fis dis' g>8 <e a f'>^~ \tweak positions #'(-7 . -4)\tuplet 5/4 { <e a f'>16^~[ \slashedGrace { <e a f'>8 }
	<f c' e>4^~]\arpeggio } <f c' e>2^~ |
  }

rhfiftysixone = {
	\time 8/4
	s1 s4
	\change Staff = "LH" \tupletUp \tweak positions #'(14.5 . 17.5)\tuplet 5/4 { \stemUp g,16\tweak X-offset -5_\ppp\shape #'((0 . 12) (18 . -8.5) (-10 . -19) (0 . 3))^\=2(\shape #'((0 . 0) (12 . 16) (-12 . 6) (0 . -9))_\=1([
	\change Staff = "RH" \stemDown \tweak Y-offset -2 r16 <bes' des> <g a>
	\change Staff = "LH" \stemUp <f ges a>] }
	\change Staff = "RH" \stemDown <d'! ees>16[
	\change Staff = "LH" \stemUp <b,! c e!> <g a b>
	\change Staff = "RH" \stemDown <a' bes?>]
	\change Staff = "LH" \stemUp <ees f ges>[
	\change Staff = "RH" \stemDown <c' d> <bes e!>
	\change Staff = "LH" \stemUp <f! aes! a!>] |
  }

rhfiftysixtwo = {
	s1*2 |
  }

lhfiftysixone = {
	s1 s4 \once \omit TupletBracket \once \omit TupletNumber\tuplet 5/4 { s16 r16 s8. } s2 |
  }

lhfiftysixtwo = {
	s1*2 |
  }

% lower staves 

lhfiftysixthree = {
	s1*2 |
  }

lhfiftysixfour = {
	\stemDown
	f2
	\arpeggioArrowDown \slashedGrace { <a,, e' cis'>8^~\arpeggio }
	<a e' cis'>2_~ <a e' cis'?>8[ 
	<b fis' cis'>8_~] \tuplet 5/4 { <b fis' cis'>16_~[ \slashedGrace { <b fis' cis'>8 }
	\arpeggioArrowUp <ees, d' b'>4_\pp_~]\arpeggio } <ees d' b'>2_~
  }
