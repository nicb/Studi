%
% bar n.115
%

rhonehundredandfifteenone = {
		\stemDown
		b32[ bes a d,
		e dis cis b
		a e g! fis!
		d' f,! g gis]
		a[ b cis dis_\ff
		fis d! a'! e
		c'! cis ees d]
		r8 |
  }

rhonehundredandfifteentwo = {
		s1 |
  }

lhonehundredandfifteenone = {
		\stemDown
		<d fis>8^.
		gis[^. <c! gis'>]^. ais![^. <e c'>^.] f![_\ff^. <a b>^.] <f! fis!>^.
  }

lhonehundredandfifteentwo = {
		s1 |
  }
