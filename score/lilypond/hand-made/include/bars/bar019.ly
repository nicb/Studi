%
% bar n.19
%

rhnineteenone = {
		\tempo \markup { "Tempo " \circle 4 } 4 = 160
		\tweak Y-offset -0.5 r4 \slashedGrace { g8~ } g8._-_\f[ ais16_-_\f] \clef bass s8
		\tweak Y-offset -0.15 r8 s4 \clef treble                                              | % bar 19
  }

rhnineteentwo = {
		s1 |
  }

lhnineteenone = {
		<b d c'>8^>_\sfz[ r8] \slashedGrace { f8~ } f8_\f_- \clef treble a''\p^.[
		\change Staff = "RH" { fis,,,_\p^.] s8 \stemNeutral
		cis'_\tweak X-offset -4\tweak Y-offset -2\f_\([ }
		\change Staff = "LH" { f''!8_\f\)] } \clef bass                                         | % bar 19
  }

lhnineteentwo = {
		s1 |
  }

