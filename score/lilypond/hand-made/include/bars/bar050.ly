%
% bar n.50
%
% upper staff
rhfiftythree = {
	<c g' cis>1_~
  }

rhfiftyone = {
	r4 r8 \tuplet 3/2 { r16 <c! g bes>8_-_\mp~ }
	\tuplet 3/2 { <c g bes>8 <c ges a!>4_- } r8 \tuplet 3/2 { r16 d,16_\f_\( gis } |
  }

rhfiftytwo = {
	s1 |
  }

lhfiftyone = {
	s1 |
  }

lhfiftytwo = {
	\clef treble
	\stemDown
	r2 r8 f''32_\f\([ e cis' gis \change Staff = "LH.3" \stemUp dis,, a'! g! a,~ a8\)] \change Staff = "LH"
  }

% lower staves 

lhfiftythree = {
        \clef bass
	\stemDown
	r4 \slashedGrace { aes8^(~ } aes8)^-_\mp_~ \tuplet 3/2 { aes16[ <ees ges>8^-]_~ }
	\tuplet 3/2 { <ees ges>8 aes!4^- } s4 |
  }

lhfiftyfour = {
	<d e'>1_~ |
  }

