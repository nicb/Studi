%
% bar n.57
%

% upper staff
rhfiftyseventhree = {
	<f c' e>4 r4 r2 |
  }

rhfiftysevenone = {
	\time 4/4
	\change Staff = "RH" \stemDown <f ges>16\tweak positions #'(-8 . -9)[ 
	\change Staff = "LH" \stemUp <c d ees> <bes! b! ees!>
	\change Staff = "RH" \stemDown <g'! a>
	\change Staff = "LH" \stemUp <f, ges! aes!>
	\change Staff = "RH" \stemDown <d'' ees!> <b! c>\=1)\=2)
	\change Staff = "LH" \stemUp \clef bass cis,,,]_\p^~
	cis8 s4. \change Staff = "RH" |
  }

rhfiftyseventwo = {
	\stemUp s2 r8 <ees! e!>16_\ppp\tweak positions #'(6 . 5)[_\( <bes,! c des> <f' a> <b,! d!>\)] r8 |
  }

lhfiftysevenone = {
	s1 |
  }

lhfiftyseventwo = {
       \stemDown
	s2
       \set PianoStaff.connectArpeggios = ##t 
 	cis,8 \arpeggioArrowDown
 	<f'! d' fis! gis!>4._~\arpeggio
  }

% lower staves 

lhfiftyseventhree = {
	s1 |
  }

lhfiftysevenfour = {
	<ees d' b'>2_~ \stemDown <ees d' b'>8
	\stemUp <g!>4_\p[\arpeggio cis,8]^~ | 
  }
