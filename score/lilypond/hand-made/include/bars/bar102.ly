%
% bar n.102
%

rhonehundredandtwoone = {
		<<
			{
				\stemUp
				<e, fis>1_- |
			}
			\new Voice {
				\global_settings
				\relative c' {
					\stemUp
					s8 f!2.._- |
				}
			}
			\new Voice {
				\global_settings
				\relative c' {
					s4 \tweak Y-offset -3r16[ b8.]_-^~ b2 |
				}
			}
			\new Voice {
				\global_settings
				\relative c'' {
				        \stemUp
					s2 s8 \slashedGrace { c!8_(^~ } c4.)_- |	
				}
			}
		>>
  }

rhonehundredandtwotwo = {
		s1 |
  }

lhonehundredandtwoone = {
                \clef bass
		<d f>1 |
  }

lhonehundredandtwotwo = {
		\stemDown
		s4 \tweak Y-offset 2r16[ a,8.]^-_~ a2 |
  }
