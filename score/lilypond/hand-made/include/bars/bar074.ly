%
% bar n.74
%

rhseventyfourone = {
	\stemUp
	<fis g'>2 r16 e''^> r8 r16 \ottava 1 d'^> r8 \ottava 0 |
  }

rhseventyfourtwo = {
	\stemDown
	<b a'>2 bis''16_\p[ dis gis b!] f[ c'! dis,! ais'] |
  }

lhseventyfourone = {
	\stemUp
	c,2 s4 \clef treble <bis' cisis'>16[ <dis e'>8.] \clef bass |
  }

lhseventyfourtwo = {
	r8 f,4^\( c8\)_~ c16[ <f'! fis'>8.] s4 |
  }
