%
% bar n.61
%

rhsixtyoneone = {
	\stemDown
	\time 4/4
	\tupletDown \tuplet 20/16 { aes'16_\ppp^\([ f d\) <b'c>^\( a! f des\)
	<a'! b>^\( aes g ges <e f>\) bes'^\( ges ees <c d!> b! aes!\) f'^\( d\)] } |
  }

rhsixtyonetwo = {
	s1 |
  }

lhsixtyoneone = {
	\stemDown
	\clef treble
	r4 r8 bes''4\tweak X-offset -6_\ppp^-^\( b!^- a!8^-\)
  }

lhsixtyonetwo = {
	s1 |
  }
