%
% bar n.41
%

rhfortyoneone = {
        \time 4/4
	\stemUp f'16\)[ r8 d16_\mf] g!8. <g! bes>16~ <g bes>16 \tweak Y-offset 3r8.
	\tuplet 10/8 { gis,32_\([ a d, ees des ees ces c! d! aes] } |
  }

rhfortyonetwo = {
	\stemDown a16 s8. ges'16 f c8^>_~ c16
	\slashedGrace { \stemDown ees32[^(_\pp a,)] } r16
	\tuplet 5/4 { c32_\pp^\([ ces ees g,! g\)] } s4 |

  }

lhfortyoneone = {
	\stemDown f,,16\tweak X-offset -2_\f \change Staff = "RH" bes''^.\tweak X-offset -2\tweak Y-offset -3_\p \change Staff = "LH"
	\ottava -1 dis,,,,^.\tweak X-offset -3_\p \ottava 0 f'8\tweak X-offset -4_\mf_\markup { \italic "loco" }[ <d'' gis b>^!] fis,,8[ <e'' fis g>16^!]
	\clef treble
	\stemUp
	\tuplet 5/4 { bes'32_\pp_\([ aes f ges f] }
	\tuplet 10/8 { ees32[ c des c ces bes a bes g g] } \clef bass |
  }

lhfortyonetwo = {
    s1 |
  }
  
