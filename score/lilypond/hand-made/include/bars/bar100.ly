%
% bar n.100
%

rhonehundredone = {
		<<
			{
				\stemUp
				\slashedGrace { e,8_\ff_(^~ } e2.)^- \slashedGrace { d8^>^~_\f } d4 |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemUp
					\tweak Y-offset -1 r8 \slashedGrace { bis8^~_( } bis8_-^~ bis2^- s4 |
				}
			}
		>>
  }

rhonehundredtwo = {
		\stemDown
		s4 \slashedGrace { \stemDown a!8_~ } a2_- \slashedGrace { \stemDown cis8_~ } cis16[ ais' gis b] |
  }

lhonehundredone = {
		<<
			{
				\stemUp
				\slashedGrace { <bis, cis>8_\ff_~^( } <bis cis>2.)_- \slashedGrace { b,8^~_( } b8_\f)[ g!_.] |
			}
			\new Voice {
				\global_settings
				\relative c' {
					\stemUp
					s4 \slashedGrace { cis8^~_( } cis2)^- s4 |
				}
			}                                             
		>>
  }

lhonehundredtwo = {
		<<
			{
				\stemDown
				r8[ \slashedGrace { \stemDown ais,8_~^( } ais8)^-_~] ais2 s4 |
			}
			\new Voice {
				\global_settings
				\relative c' {
					\stemDown
					s4 \tweak Y-offset 0.5r8.[ \slashedGrace { \stemDown b!8_~^( } b16)^-]_~ b4 s4 |
				}
			}
		>>
  }
