%
% bar n.36
%

rhthirtysixone = {
	r8 \stemUp <fis' gis'>16_\mp[ fis_\f]~ fis2.~ |
  }

rhthirtysixtwo = {
	r4 r8 <ais' cis>8\tweak X-offset -5_\mp[ <d e> <dis fis> <cis e> <a! f'!>] |
  }

lhthirtysixone = {
	\tuplet 3/2 { r8 \slashedGrace { a,8\tweak X-offset -4_\ff_( } g,!_.)[ \clef treble
	\slashedGrace { f''!_( } gis'8_.)] }
	r8 <a,! c!>_\mp[ <b dis> <d! f!> <c! ais'!> <gis dis'>] |
  }

lhthirtysixtwo = {
	s1 |
  }
