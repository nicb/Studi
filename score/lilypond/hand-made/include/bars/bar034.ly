%
% bar n.34
%

rhthirtyfourone = {
	s1 |
  }

rhthirtyfourtwo = {
	s1 |
  }

lhthirtyfourone = {
	r8 e_\f^\([ \change Staff = "RH" g,\tweak X-offset -2_\f\)
	\change Staff = "LH" f_._\p \change Staff = "RH" e'^.\tweak X-offset -2_\f] \change Staff = "LH"
	r8 \slashedGrace { fis,8\tweak X-offset -3_\ff_( } \ottava -1  dis,_.)[ \ottava 0 \change Staff = "RH"
	\clef treble
	\slashedGrace { c'''8_(\tweak X-offset -2_\ff } a'8^.)] \clef bass \change Staff = "LH"
  }

lhthirtyfourtwo = {
	s1 |
  }

