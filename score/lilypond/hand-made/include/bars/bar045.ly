%
% bar n.45
%

rhfortyfiveone = {
	\tupletUp \stemUp \tuplet 3/2 { r16 b8_._\f }
	\tempo \markup { "Tempo" \circle 5 } 4 = 116
	\tieDashed
	\shape #'((0 . 0.5) (0 . 0) (0 . 2) (0 . 2)) PhrasingSlur
 	\pitchedTrill 
	e,8_\p^\(~\startTrillSpan ees ~e4 ~e8 \tuplet 3/2 { r16\stopTrillSpan g cis }
	\stemDown \pitchedTrill
	d4_~\startTrillSpan c |
  }

rhfortyfivetwo = {
	s1 |
  }

lhfortyfiveone = {
	s4 \slashedGrace { g8_(^~ } g16_.) r8. s4 b''4^~  |
  }

lhfortyfivetwo = {
	\stemDown \tupletDown
        \once \override TupletBracket.positions = #'(-2.5 . -1.5)
	\tuplet 3/2 { r16 \once \override Beam.positions = #'(-1 . -0.25) d'!_\f^( f!) } r32 fis32^._\p r16
	s4
	gis,16 r8. r8. g,!16^-~ |
  }

