%
% bar n.65
%

rhsixtyfiveone = {
	\time 4/4
	\stemUp
	f16[ e' b fis'] e8.[ d!16] \repeat tremolo 8 { e!32_\ff^\( ees,!\) } |
  }

rhsixtyfivetwo = {
	\stemDown
	e16[ e' b fis'] dis[ bes d! aes] s2 |
  }

lhsixtyfiveone = {
	\stemDown
	<cis,,, d'>8.^-[ <c! d'>16^-] <c a'>4^- \repeat tremolo 8 { eis'!32_\ff_\( fis'\) } |
  }

lhsixtyfivetwo = {
	s1 |
  }
