%
% bar n.26
%

rhtwentysixone = {
		\time 5/4
		\clef bass
		<dis f d'!>8\tweak X-offset -3_\ff^>[ \change Staff = "LH" <c,! e b'>8\tweak X-offset -4_\ff_>]
		\change Staff = "RH" r8 cis'8^.\tweak X-offset -3_\p[
		\change Staff = "LH" ais8_.\tweak Y-offset -2_\p] \change Staff = "RH" r8
		g8\tweak X-offset -3_\f^\([ \change Staff = "LH" a'!8_\f\)
		\change Staff = "RH" f_\p^. \change Staff = "LH" ais,!_.\tweak Y-offset -2_\p]          | % bar 26
		\change Staff = "RH"
  }

rhtwentysixtwo = {
		s1 s4 |
  }

lhtwentysixone = {
		s1 s4                                                                                   | % bar 26
  }

lhtwentysixtwo = {
		s1 s4                                                                                   | % bar 26
  }

