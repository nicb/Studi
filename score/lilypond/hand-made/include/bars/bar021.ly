%
% bar n.21
%

rhtwentyoneone = {
		\time 7/4
		\stemUp c8~ \slashedGrace { c8 }
		% \once \override Score.NonMusicalPaperColumn.line-break-system-details = #'((Y-offset . -30))
	        \tuplet 12/10 { b8_\([^\markup { \bracket { \rhythm { 1~ 4 } } } ais8 gis ais\) f! g! e fis f!_\(  e g dis] }
		ais4.\)                                                                               | % bar 21 
  }

rhtwentyonetwo = {
		s1 s2. |
  }

lhtwentyoneone = {
		<<
			{ r8.[ gis,,16_\f~] gis4~ gis8[ \slashedGrace { gis8_\( }
			fis8\)~] fis4~ \slashedGrace { fis8 } \offsetPositions #'(0 . -8) d4._\( a'16[ e'] r8 f\) }

			\new Staff \with { alignBelowContext = "LH" } {
				\relative c {
					\clef bass
					\global_settings \stemDown
					r4 r8 \slashedGrace { a,8_\(_\p } b'8^.\)
					r4 r8 <c, a' b>8\tweak X-offset -2_\ff\tweak Y-offset -0.01^> r8 ais'^._\p r2
					\once \override Staff.TimeSignature.stencil = ##f
				}
			}
		>>                                                                                      | % bar 21
  }

lhtwentyonetwo = {
		s1 s2. |
  }

