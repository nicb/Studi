%
% bar n.120
%

rhonehundredandtwentyone = {
		\stemDown
		b32[ cis dis g] r8
		r16 fis,,32_\p[ g b gis a des]
	  c![ ais c e, cis' a! f! b]
		g![ gis ais fis e' d cis! a'!] |
  }

rhonehundredandtwentytwo = {
		s1 |
  }

lhonehundredandtwentyone = {
		s1 |
  }

lhonehundredandtwentytwo = {
		\stemDown
		<g' a>8^.] <d, e'>^.[ r]
		dis^.[_\p <f ais>^.]
		d!^.[ <fis dis'>^.]
		gis^.[ \allowBreak |
  }
