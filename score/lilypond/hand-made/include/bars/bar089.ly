%
% bar n.89
%

rheightynineone = {
		\stemUp
		a32_\([ d fis,\) r
		c'_\( b g\) r
		dis_\( dis' g,\) r
		e_\( d'! c\)] r
		\stemDown
		cis^\([_\> f a,\) r
		b^\( e gis,\) r
		f'^\( dis cis\) r
		d!^\( fis bes,\)]_\mp r |
  }

rheightyninetwo = {
		s1 |
  }

lheightynineone = {
		\stemUp
		s4 \clef treble
		r16.[ fis''32_. r16. ais32_.]
		r16.[ gis32_. r16. g!32_. r16. b32_. r16. a!32_.] |
  }

lheightyninetwo = {
		\stemDown
		r16.[ <gis d'>32^.
		r16. <e f'!>32^.] s2. |
  }
