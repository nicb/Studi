%
% bar n.3
%

rhthreeone = {
		\tuplet 5/4 { aes16 \slashedGrace { e,,8\shape #'((0 . 0) (0 . 0) (0 . 0) (0 . 6))_\( } g''16\) \tweak Y-offset 6 r32 }
		\voiceOne
		\tuplet 3/2 { \tweak Y-offset 0 r16 fis,,\f^\markup{ \italic{"(poco legato)"} } g } \tuplet 3/2 { r b c } r8 \breathe
		\stemUp
		\tempo \markup { "Tempo " \circle 2 ", ben sostenuto" } 4 = 72
		\tieDashed \tuplet 5/4 { r4 gis16\fff~ } gis8~ \tuplet 3/2 { gis16 a_\f cis } \tieSolid |
  }

rhthreetwo = {
		s2 \tuplet 5/4 { dis16_\f\shape #'((0 . 1) (0 . 0) (0 . 0)(0 . 0))^\( gis e fis\) s16 } s4 |
  }

lhthreeone = {
		\arpeggioArrowUp
		fis''8^\f[ f a d] \tuplet 5/4 { r8. <ais, dis e b'>16\arpeggio s16 } s8
		\arpeggioArrowDown \tuplet 5/4 { r16. <g ais e'>16^\f\arpeggio } |
  }

lhthreetwo = {
		g,,8\mf \slashedGrace g'^( g4) r8
		\tuplet 5/4 { <c,, ais>16\mf[ g \tweak Y-offset -5 r8 \tieDashed f'16~] } f8~
		\tuplet 5/4 { f32[ \tweak Y-offset -3 r16 \tieSolid
		dis16\p~]} |
  }

