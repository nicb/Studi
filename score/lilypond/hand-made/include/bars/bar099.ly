%
% bar n.99
%

rhninetynineone = {
		\stemDown
		r8[ \slashedGrace { \stemUp ais'''8_( } \stemDown b,8^-)]
		\stemUp
		r32 dis,,!_\(_\f[ g! d!\) r f_\( a\)] r
		\stemDown
		\slashedGrace { \stemUp e''8_( } \stemDown g,8)^.[ a,!^.]
		r8[ \ottava 1 \slashedGrace { \stemUp e'''8_( } \stemDown f,^.)] \ottava 0 |
  }

rhninetyninetwo = {
		s1 |
  }

lhninetynineone = {
		\slashedGrace { a,8_( } \stemDown <fis''! gis!>4^-^>)
		\stemUp
		<d c'>32_\f_.\tweak positions #'(6 . 5)[ r16. <fis,,! ais!>32_. r16.]
		<gis'! c!>32[ b'^\( cis a!\)] s8
		\slashedGrace { c,!8\shape #'((-0.5 . -3.5)(-1 . -2)(-2 . 2)(0 . 0))^( } gis''4^>) |
  }

lhninetyninetwo = {
		s2 r16 a_\([ g! ais!\)] e'[ e^\( a,! d\)] |
  }
