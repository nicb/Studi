%
% bar n.20
%

rhtwentyone = {
		\time 3/4 \stemNeutral
		b8^._\p[ \change Staff = "LH" gis,,,8_._\p]
		\change Staff = "RH" \tuplet 7/4 { r16 gis'''8_\f_\([ fis e~] }
		e8.\) c'16~                                                                           | % bar 20
  }

rhtwentytwo = {
		s2. |
  }

lhtwentyone = {
		s4 r4 \clef treble \slashedGrace { dis,8_\(_\ff } cis''8\) r \clef bass                 | % bar 20
  }

lhtwentytwo = {
		s2. |
  }

