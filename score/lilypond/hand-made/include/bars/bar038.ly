%
% bar n.38
%

rhthirtyeightone = {
	\slashedGrace { f8~_( } f16) s16
	\tuplet 7/4 { b'!16_\(_\>[ dis, ais gis e8\)_> d16_\pp~] } \slashedGrace { d8 }
	\stemNeutral b'16_\pp^.[ \change Staff = "LH" \clef bass g,,!16_._\pp]
	\change Staff = "RH" s4 \stemUp e'''4~ | \break
}

rhthirtyeighttwo = {
	\stemNeutral <b' d>16[ c]\) s8 s2 r8 \stemDown r32[ a16 dis,32~] \stemNeutral |
  }

lhthirtyeightone = {
	gis'16^\([ bes,]\)
	\tuplet 3/2 { fis,16_\(\>[ cis' g'!] }
	\slashedGrace { fis8~ } fis32[ a'16\)_\( \clef treble gis'32_\pp]~\) \slashedGrace { gis8 }
	s8
	\stemNeutral
	r8 \slashedGrace { cis,,,8_\ff_\( } g16_.\)[
	\change Staff = "RH" \slashedGrace { fis''8^\(\tweak X-offset -4_\ff } a'16^.\)]
	s8
	\change Staff = "LH" \slashedGrace { <b,, gis'>8~ } <b gis'>32_\([ r16 d32~\<] |
  }

lhthirtyeighttwo = {
	s1
  }
