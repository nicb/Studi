%
% bar n.126
%

rhonehundredandtwentysixone = {
		\time 9/8
		\clef bass \stemUp
		\tempo \markup { "Tempo" \circle 4 } 4 = 69
		r16 \repeat tremolo 3 { cis,,,32[_\(_\mf gis]\) }
		\clef treble
		\stemDown
		<a'' des b'>8^__\mp \breathe
		\override NoteHead.style = #'harmonic
		\slashedGrace { <d! e f g! ges!>8_~^( } <d e f g ges>2.) |
		\revert NoteHead.style
  }

rhonehundredandtwentysixtwo = {
    s1 s8 |
  }

lhonehundredandtwentysixone = {
		\stemUp
		\clef bass
		s8 \repeat tremolo 2 { a,,,32_\(_\mf f'!\) } ees!8___\mp bes!16_\fff_![ r8.] s2 |
  }

lhonehundredandtwentysixtwo = {
		\stemDown
		<dis'! g!>8^. s8 s8 s4 bes8[^\(_\mp_\>  ges' d b!]\)_\p |
  }
