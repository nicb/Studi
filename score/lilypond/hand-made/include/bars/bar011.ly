%
% bar n.11
%

rhelevenone = {
		\slashedGrace { b8~_( } \tuplet 3/2 { b4) <bes, g'>8 } cis16 b'!
		<g! ges> <d f! fis dis'>_\mp s2                                           | % bar 11
  }

rheleventwo = {
		s2 \slashedGrace { e''8~^(\ff } e8) d16
		\slashedGrace { e8~^( } e16)~ e16 b! fis'8                              | % bar 11
  }

lhelevenone = {
		s2 \slashedGrace { <b c'>8~\ff_( } <b c'>8) cis16
		\slashedGrace { c8(~ } c16)~ c \ottava -1 bes, \ottava 0 a'8_\markup { \italic "loco" } | % bar 11
  }

lheleventwo = {
		\slashedGrace { <c, a''>8~^( } <c a''>4.) <bes' c a'>16^- r16 s2              | % bar 11
  }

