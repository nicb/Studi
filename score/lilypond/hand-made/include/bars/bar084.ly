%
% bar n.84
%

rheightyfourone = {
		\stemDown
		<d! gis dis'>16_\f\)[ r16 fis,8]^. 
		\ottava 1
		<d'' dis'>32^!_\ff[ r <g e'>^! r <g e'>^! r r <dis fis'>^! r16 r32 <cis d'!>^! r16 r32 <e f'!>^!] 
		\ottava 0 \clef bass
		\cadenzaOn
		\once \omit Stem
		\repeat tremolo 4 { g,,,32_\f^\markup { \draw-line #'(4.5 . 0) "2\"" \draw-line #'(4 . 0) \arrow-head #X #RIGHT ##f } \once \omit Stem fis, } 
		\cadenzaOff s8.  
		\stemUp \clef treble
		<a''! cis ais'>16_\( |
  }

rheightyfourtwo = {
		s2. \cadenzaOn s4 \cadenzaOff s4 |
  }

lheightyfourone = {
		\stemDown
		<c e g!>16_\f\)[ r16 f,!8]^. 
		\ottava -1 \stemUp
		<b,,, gis' c!>32_!_\ff[ r <c cis'>_! r <ais cis'>_! r r <a c'>_! r16 r32 <ais b'!>_! r16 r32 <b g'!>_!] 
		\ottava 0
		\cadenzaOn
		\once \omit Stem
		\repeat tremolo 4 { dis32_\f \once \omit Stem e' }
		\cadenzaOff \stemDown
		s8. <dis' g c>16^\( |
  }

lheightyfourtwo = {
		s2. \cadenzaOn s4 \cadenzaOff s4 |
  }
