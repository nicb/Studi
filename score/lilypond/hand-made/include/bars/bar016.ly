%
% bar n.16
%

rhsixteenone = {
		\tempo \markup { "Tempo" \circle 2 ", un poco trattenuto" } 4 = 69
		r4 \slashedGrace { <f' a c fis>8_\p~ } <f a c fis>8 s8
		r8 \slashedGrace { <gis b d g!>8_\p~ } <gis b d g>~ \tuplet 3/2
		{ <gis b d g>8 \clef bass dis,4 }                                                    | % bar 16
  }

rhsixteentwo = {
		r16 a8\f^_ r16 s8 r32[ \slashedGrace { b''!8_( } bes16\pp)^\(
		\slashedGrace { e32 cis d\)} bes32^!]  s2                               | % bar 16
  }

lhsixteenone = {
		\stemUp
		\slashedGrace { e'8( } c8.)[ gis'16]
		\slashedGrace { dis'8~ } dis8_-\p~ \slashedGrace { dis8 } r8
		r16[ <f! fis'!>8
		d,16\f_\shape #'(((0 . 0)(0 . 0)(0 . 0)(0 . -1))((0 . -1) (0 . 0) (0 . 0) (0 . 0)))\(~]
		\tuplet 3/2 { d8[ c cis] }\stemNeutral                                                  | % bar 16
  }

lhsixteentwo = {
		s1 |
  }

