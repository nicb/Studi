%
% bar n.109
%

rhonehundredandnineone = {
		\time 8/4
		gis32[_\( fis' ais,]\) r
		ais[_\( fis' g,]\) r
		fis[_\( dis' f,!]\) r
		e[_\( cis'
		\override TextSpanner.bound-details.left.text = \markup { \dynamic "pp" \italic "cresc." }
		\override TextSpanner.bound-details.center.text = \markup { \italic "sino" }
		\override TextSpanner.bound-details.right.text = \markup { \italic "a" }
		\tieDashed \pitchedTrill b'16\)]^\startTrillSpan cis ^~_\startTextSpan b2^~ \after 2... \stopTrillSpan b1\stopTextSpan |
  }

rhonehundredandninetwo = {
		s1 s1 |
  }

lhonehundredandnineone = {
		\stemDown
		r16.[ <c! d!>32]^. r16.[ b32]^. r16.[ a32]^.
		r16 \clef treble \tieDashed ees''16_~ ees2_~ ees1 |
  }

lhonehundredandninetwo = {
		s1 s1 |
  }
