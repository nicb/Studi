%
% bar n.81
%

rheightyoneone = {
		<bes f' aes b>4 \tweak Y-offset 4 r16[ \stemDown \ottava 1 \slashedGrace { <d''' f aes>8_\pppp^(_~ } <d f aes>8.)]_~ <d f aes>2 |
  }

rheightyonetwo = {
		s1 |
  }

lheightyoneone = {
	\stemUp
	\clef bass
	\tuplet 6/4 { a,16_\([ c, g' aes bes, b'] } \slashedGrace { \stemDown des'^( e,)\) }
	\stemDown
	r16 r32\tweak positions #'(-4 . -4)[ gis^!_\ffff gis^! r r gis^! r8 r32 g^!] r16 r4 |
  }

lheightyonetwo = {
	s1 |
  }
