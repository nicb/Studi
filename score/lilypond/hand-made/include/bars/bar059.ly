%
% bar n.59
%

% upper staff
rhfiftyninethree = {
        \stopStaff
        \once \omit Staff.TimeSignature
	\cadenzaOn
	s1
	\cadenzaOff
	s1 |
  }

rhfiftynineone = {
        \change Staff = "RH"
	\tempo \markup { "Tempo" \circle 6 } 4 = 70
	\cadenzaOn
	\stemDown
	a'1^-^\markup { \draw-line #'(2 . 0) "ca.2\"" \draw-line #'(2 . 0) \arrow-head #X #RIGHT ##f }\tweak X-offset -6\tweak Y-offset -10_\ffff_\markup { \override #'((thickness . 6)) \draw-line #'(0 . -6) }_\markup { \override #'((thickness . 3)) \draw-line #'(9.5 . 0) \arrow-head #X #RIGHT ##f } 
	\cadenzaOff
	\stemUp
	\tuplet 5/4 { r8.[ gis!8]^~ } gis[ \tuplet 5/4 { cis16. fis16^~] } fis4
	<ges, aes>16[_\(_\pp_> a! aes b_\ppp]
  }

rhfiftyninetwo = {
        \cadenzaOn
        s1
        \cadenzaOff
	\stemDown
	\tuplet 5/4 { r8.[ a'!8]_~ } a8_~ \tuplet 5/4 { a16. f!16_~ } f4 s4 |
  }

lhfiftynineone = {
        \cadenzaOn
        s1
        \cadenzaOff
	\clef bass
	r2 r8 \stemDown e'16^([ d)] dis[^\( e bes'_~ <bes d!>]_~ |
  }

lhfiftyninetwo = {
        \cadenzaOn
        s1
        \cadenzaOff
	s1 |
  }

% lower staves 

lhfiftyninethree = {
        \cadenzaOn
        s1
        \cadenzaOff
	s1 |
  }

lhfiftyninefour = {
	\cadenzaOn
	gis,1_-
	\cadenzaOff
	\stemUp
        \tuplet 5/3 { gis!4_-_\p_\< f'!_- g,_- dis'_- fis,_-_\mf } r4 \stopStaff |
        \once \omit Staff.TimeSignature
  }
