%
% bar n.118
%

rhonehundredandeighteenone = {
		\stemUp
		fis'32[ dis b] r r8
		\tuplet 3/2 { a8_\mf[_\( <e fis e'> <d! dis'>] } <e f'!>4\)
		\tuplet 3/2 { <e! f'>8[_\( <d e> <c! cis'>\)]^~ } | 
  }

rhonehundredandeighteentwo = {
		s1 |
  }

lhonehundredandeighteenone = {
		\stemUp
		dis8^~ \slashedGrace { dis8 } s8 s2. |
  }

lhonehundredandeighteentwo = {
		\stemDown
		r8 <c'! d! g>4^-_\mf d,,8^- <c'' d fis>8^- <e, gis ees'>4^- <g! b dis!>8^-_~ |
  }
