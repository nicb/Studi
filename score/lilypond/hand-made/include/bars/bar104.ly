%
% bar n.104
%

rhonehundredandfourone = {
		<<
			{
				\stemUp
				\slashedGrace { ees,8_(^~ } <d ees) d'>2. s4 |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemDown
					r8\tweak positions #'(-7 . -7)[ f8]_~^- f2 s4 |	
				}
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemUp
					s4 r8.[ \slashedGrace { a'!^~_( } a16]^~ a4 s4 |
				}
			}
		>>
  }

rhonehundredandfourtwo = {
		\stemDown
		f2. r8[ bis_\f^-^>]_~ |
  }

lhonehundredandfourone = {
		\stemUp
		<a, f' g>2. r4 |
  }

lhonehundredandfourtwo = {
		<<
			{
				\stemDown
				<des e>2. s4 |
			}
			\new Voice {
				\global_settings
				\relative c {
					\stemDown
					s4 \tweak Y-offset 0r8.[ \slashedGrace { \stemDown <ees ges bes>8^(_~ } <ees ges bes>16])^-_~ <ees ges bes>4 s4 |
				}
			}
		>>
  }
