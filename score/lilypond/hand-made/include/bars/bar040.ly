%
% bar n.40
%

rhfortyone = {
	\time 5/4
	\stemDown a16[ \stemUp \slashedGrace { ges8~_( } \stemDown ges8.)]
	\stemUp \slashedGrace { bes8_( } \stemDown
	\tuplet 15/12 { g!16)[ ees ges c d! e! a, f bes b ees c a ges bes] } d16
	\stemUp \slashedGrace { f8~_( }
	\stemDown \tuplet 3/2 { f32)[ ees des] }
	\tempo \markup \left-column { \line {"Tempo " \circle 2 ","} {"molto trattenuto"} } 4 = 63
	\tuplet 5/4 { d,!32_\pp^\([ des bes bes! b!] } |
  }

rhfortytwo = {
	s1 s4 |
  }

lhfortyone = {
	\stemDown \tuplet 20/16 { a16[ g' ees a, b f' d f des bes g b ees c a e!  bes d f bes] }
	c16 \stemUp \slashedGrace { e,8_(~ }
	\stemDown 
	\tuplet 3/2 { e32) c' d! } r16 \clef treble \stemUp g_\pp_. \clef bass |
  }

lhfortytwo = {
	s1 s4 |
  }
