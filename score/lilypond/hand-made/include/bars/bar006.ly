%
% bar n.6
%

rhsixone = {
		\time 5/4
		\stemUp
		s2. s8 \dynamicUp \slashedGrace b8_( b16) \tweak DynamicText.X-offset -5.5 \mf r16 s4 | % bar 6
  }

rhsixtwo = {
		\tuplet 5/4 4 { aes16 ees des c a'  f c e fis gis  g f f e' cis 
		c g gis gis r } \change Staff="LH" \offsetPositions #'(0 . 2) \stemUp\slashedGrace c,!8_(
				\change Staff="RH" \clef treble <bes' aes'>8) r8 | % bar 6
  }

lhsixone = {
		\tuplet 5/4 4 { r16 \arpeggioArrowUp <c, d f>\f\arpeggio r8.  r8 <gis d' f>16\arpeggio r8 }
		r16 fis\mp\< c' <ais gis'> <ais gis'>
		fis! cis'\mf s16 s4 | % bar 6
  }

lhsixtwo = {
		\tieSolid
		s2 s4 s8. <d,~ g'~>16
		\tuplet 3/2 { <d g'>8 f'4\tweak Y-offset 4\f } | % bar 6
  }

