%
% bar n.110
%

rhonehundredandtenone = {
		\time 4/4
		\stemUp
		\tuplet 3/2 { <gis e'>16_-_>_\ff[ <gis e'>8]_-_> }
		c,!32_\(_\f[ a!\) ais'_\( fis] d![ g,!\) \tweak Y-offset 2.5 r8 b''16]_-
		\tuplet 3/2 { <b,, dis cis'>16_-_>_\ff[ <b dis cis'>8_-_>] }
		\tuplet 3/2 { <b dis cis'>8_-_>_\ff[ <b dis cis'>16_-_>]^~ } \slashedGrace { <b dis cis'>8 }
		\tuplet 3/2 { <b dis cis'>16_-_>[ \clef bass a_\f gis }
		ais32 d,! f'! gis,!] |
  }

rhonehundredandtentwo = {
		\stemDown
		s4 s16 r16[ \slashedGrace { g8^(_~ } g8]^- s2 |
  }

lhonehundredandtenone = {
		\clef bass
		\stemDown
		\tuplet 3/2 { <f,,! g'!>16^-^>_\ff[ <f g'>8]^-^> } r8
		r16[ <bes' fis'>8.]^-_\f
		\stemUp
		\tuplet 3/2 { c,,!16_-_>[_\ff c!8_-_>] }
		\tuplet 3/2 { c!8[_-_> c!16]_-_>^~ } \slashedGrace { c8 }
		c16[_-_> g'8_\f \tieSolid e16]^~ |
  }

lhonehundredandtentwo = {
		s1 |
  }
