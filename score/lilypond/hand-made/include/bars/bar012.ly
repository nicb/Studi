%
% bar n.12
%

rhtwelveone = {
		bes'16 ees'8.^>~ ees16 des_. ges,_. ees_. r4 f,16_\mf[ r8 f'16_\mf]              | % bar 12
  }

rhtwelvetwo = {
		\tuplet 3/2 { r8[ d,16^-~] } d8~ \slashedGrace { d8 } s4
		\tuplet 5/4 { r8 <g,! a! gis ais>4_\mp^- <c, gis' a ais>8_\mp^- r8 }    | % bar 12
  }

lhtwelveone = {
		s1                                                                                      | % bar 12

  }

lhtwelvetwo = {
		\clef bass
		aes16[ g'8^-~ \slashedGrace { g8 } r16]
		ees'^-~ \slashedGrace { ees8 } bes16^. f^. d^-~ \slashedGrace { d8 }
		r8.[ <des e g>16_\mf~] <des e g>8[ <des e g>~]              | % bar 12
  }
