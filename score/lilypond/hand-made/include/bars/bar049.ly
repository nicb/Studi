%
% bar n.49
%
% upper staff
rhfortyninethree = {
    \override Staff.Clef.transparent = ##t
    \clef treble
    r2. r8.\tweak positions #'(-6 . -3)[ \stemDown \ottava 1 <c'! g' cis!>16_\p]_~ |
  }

% central staves (4 voices)

rhfortynineone = {
	\slashedGrace { b''8^~_(^\( } b4)_\mp \slashedGrace { e,32_([ d)\)] } s4
	r16
	\shape #'((0 . 0) (4 . -5) (8 . 3) (17 . -6)) PhrasingSlur
	\tuplet 3/2 { ees_\( e a,!\)\tweak X-offset 3^\markup { \override #'((thickness . 2)) \draw-dashed-line #'(6 . 7) }} r16 r4 |
  }

rhfortyninetwo = {
	s4 r16 <a, cis dis>_\f^-[ <cis! dis! f!>^- <b cis d!>^-] s2 |
  }

lhfortynineone = {
	s1 |
  }

lhfortyninetwo = {
	\slashedGrace { \stemDown b,8^(_~ } b8)^\([ bes'\)]~
	\tuplet 8/6 { bes16[ gis^_ c,!^_ gis'!^_ c!^_ gis^_ ais^_ f^_] } r8
	\slashedGrace { cis'8_\ppp^(_~ } \tuplet 5/4 { cis16) ges ees r8 } |
  }

% lower staves 

lhfortyninethree = {
	s1 |
  }

lhfortyninefour = {
	c2.^~ c8.[ <d,, e'>16\tweak X-offset -3_\p]_~ |
  }
