%
% bar n.8
%

rheightone = {
		\time 4/4
		\stemNeutral
		r4 dis8.-- r16 \tuplet 3/2 { r16[ <fis' cis'>8-.] }
		\slashedGrace { \change Staff = "LH" <ais,, b'>32^\(_\pp_\< \change Staff
		= "RH" <c' a'>32 \change Staff = "LH" <fis,, d'>32_\> \change Staff = "RH" <ges' a>32_\pp }
		<e' f'!>8-!\)
		\tempo \markup { "Tempo " \circle 3 ", ben sostenuto" } 4 = 123
		\stemUp\tuplet 5/4 { r16 <d, ais' b cis>8_-_\mp[ \tweak X-offset 1 r8]}  | % bar 8
  }

rheighttwo = {
		s2. \stemDown\tuplet 5/4 { \tweak Y-offset -2r8.[ <d bes' c>8^-\mf] }            | % bar 8
  }

lheightone = {
		\stemUp\tupletUp\tuplet 5/4 { r16.[ c,16~\p] } c8[ r8.] <ais' b>16\ff
		\arpeggioArrowDown <gis' f e, cis g d>16_--\arpeggio
		r8. r8 <e,, f'>8\f                               | % bar 8
  }

lheighttwo = {
               <<
		    { a4~ a8~ \slashedGrace { a } s8 s2 | } % bar 8
		    { s2. \tuplet 5/4 { s16 s8.. s32 } }
               >>
  }

