%
% bar 2
%
rhtwoone = {
		e16 \stemUp b,,32\mp\<\([ c dis fis e g\f\!]
		\slashedGrace { bes,8_( } e32-^[) f fis ais b, \slashedGrace c8_( d32])\) r16
		\tuplet 5/4 { r4 ees''16\mf~ } \tuplet 5/4 { ees[ r aes8.~] } |
	}

rhtwotwo = {
		s1 |
	}

lhtwoone = {
		\tuplet 5/4 { r16 a,4\mf~ } \tuplet 5/4 { a8 a'8. } s4
		\tuplet 5/4 { r32 \ottava -1 <b,, c>8\mf } \ottava 0 r8                                           | % bar 2
	}

lhtwotwo = {
		\tuplet 5/4 { r8[ d,32] } r8 \tuplet 3/2 { r4 dis'8 }
		e32\f^\([ fis f c' b e ais\)] r s4 |
	}
