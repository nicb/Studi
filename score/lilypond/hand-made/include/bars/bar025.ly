%
% bar n.25
%

rhtwentyfiveone = {
		\time 3/4
		\clef bass
		r8 f''4^-_\f r8 s8 r8                                                                   | % bar 25
  }

rhtwentyfivetwo = {
		s2. |
  }

lhtwentyfiveone = {
		ais,,4_\f_- r8 \slashedGrace { gis,8_\(_\p } b8_.\)[
		\change Staff = "RH" \clef treble \slashedGrace { aes'''_\p_\( } fis8^.\)] s8           | % bar 25
  }

lhtwentyfivetwo = {
		s2. |
  }

