%
% bar n.96
%

rhninetysixone = {
		\stemUp
		f,!32\)[ r f_\( d' e,\) r ais_\( gis' b,\) r c_\( cis b\) r e,_\(_\f c'!]
		g!\)[ r f_\( d' dis,\) r d!_\( c' e,\) r f_\( dis' fis,\) r gis_\( fis'] |
  }

rhninetysixtwo = {
		s1 |
  }

lhninetysixone = {
		\stemDown \clef bass
		r32[ a^._\markup { \italic "sino" } r16 r32 fis^. r16 r32 <g dis>^. r16_\markup { \italic "a" } r32 <gis, b'>_\f] r16
		\stemUp
		r32[ <ais fis'>_. r16_\markup { \italic "cresc." } r32 <cis ais'>_. r16 r32_\markup { \italic "ancora" } <cis a'!>_. r16 r32 <b g!>_.] r16 |
  }

lhninetysixtwo = {
		s1 |
  }
