%
% bar n.92
%

rhninetytwoone = {
		\stemDown
		ais32^\([ c! d,!\) r
		gis^\( f g!\) r
		b^\(_\< d, cis'\) r
		dis^\( fis, e'\)] r\!
		dis!^\([_\> fis,! f'!\) r
		cis!^\( e, d'!\)] r\!
		\stemUp
		\tempo \markup { "Tempo" \circle 2 }
		\tuplet 5/4 { r16 a32[ f] r32 }
		\tuplet 5/4 { g_\([ c,! f\)] r16 } |
  }

rhninetytwotwo = {
		\stemDown
		s2. gis32\tweak positions #'(-5 . -3.5)[_\( cis dis d\) \tweak Y-offset -1 r16 fis!] | 
  }

lhninetytwoone = {
		\stemUp
		r16.[ c,32_. r16. a'32_. r16. ais32_. r16. b32]
		r16.[ a32_. r16. c,!32_.]
		\stemDown
		r16[ <fis e'>^.] r[ \clef bass <cis, b' dis>^>] |
  }

lhninetytwotwo = {
		s1 \clef treble |
  }
