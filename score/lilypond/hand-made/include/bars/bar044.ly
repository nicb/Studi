%
% bar n.44
%

rhfortyfourone = {
	\slashedGrace { e8^>~ } e16[ r8 ais,16_\f] r16.[ c,!32] \stemDown
	r32 <ees' f'>16.
	\tupletDown \tuplet 3/2 { <d e'>8 <e f'> <ais,! a'!> } \tupletNeutral r4 |
  }

rhfortyfourtwo = {
	s1 |
  }

lhfortyfourone = {
	\tupletUp \tuplet 3/2 { ais16[ a'!_\( aes } \tupletNeutral g16\) dis_.]
	r16.[ f!32] r8
	\stemDown
	\arpeggioArrowDown <cis b' g'>8_\mf[\arpeggio
	\shape #'((2 . 9)(5 . 7)(-4 . -2) (0 . 0)) PhrasingSlur
	d'\tweak X-offset -2\tweak Y-offset -5^\f_\(]
	\stemUp
	\slashedGrace { c,,8~ } c16\)[ r8 \ottava -1 dis,16] \ottava 0 |
  }

lhfortyfourtwo = {
        r8.[ f,!16^.] \tuplet 5/4 { r16 b4_\mf^- } s2 |
  }
