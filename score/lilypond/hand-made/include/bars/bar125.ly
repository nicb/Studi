%
% bar n.125
%

rhonehundredandtwentyfiveone = {
		\time 7/8
		\stemDown
		ais32[ fis ais gis
		c!_\p a! b fis!]
		d'![ a cis! f,!
		c'! ais! gis! <fis dis'>]
		ais[ b cis <e, f'!>
		c'! gis! a! <fis! d'!>]
		ais[ c d <e b'>] |
  }

rhonehundredandtwentyfivetwo = {
		s2.. |
  }

lhonehundredandtwentyfiveone = {
		\stemUp
		r8 <e gis>8_\p_.[ <c! dis! e!>_.]
		<d! e>_.[ <b g'!>_.]
		<dis g>_.[ <fis gis>_.] |
  }

lhonehundredandtwentyfivetwo = {
		s2.. |
  }
