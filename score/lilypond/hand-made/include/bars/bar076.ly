%
% bar n.76
%

rhseventysixone = {
	\stemUp
	r4 <a'! c ais'>2_\mp \slashedGrace { <b, gis' c des>8^~_( } <b gis' c des>4)^~ |
  }

rhseventysixtwo = {
	s1 |
  }

lhseventysixone = {
	r8 e,,8[ \arpeggioArrowDown <g''! a! b gis'>\arpeggio <fis g'!> <dis e'> <d! f'!>] s4 |
  }

lhseventysixtwo = {
	\stemDown
	\slashedGrace { e,8_~^( } e8_~)[ e8] s2 r8[ \slashedGrace { <ais' g'!>8^(_~ } <ais g'>8)_~] |
  }
