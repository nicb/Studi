%
% bar n.78
%

rhseventyeightone = {
	<<
		\new Staff = "RH.2" \with { alignAboveContext = "RH" \clef treble } {
			\global_settings
		  <<
			\new Voice {
				\relative c' {
					\stemUp
					r8.[ <ais' b'>16_\mf]^~ <ais b'>2 r4 |
				}
			}
			\new Voice {
				\relative c''' {
					\stemDown
				  s2 \override NoteHead.style = #'harmonic-black
					\ottava 1 \slashedGrace { <d' f ges>8_~ }
					<d f ges>4_\pppp r4 \ottava 0
					\revert NoteHead.style |
				}
			}
		  >>
		}
		{
			\clef bass 
			\time 4/4
			\stemUp
			r4 \tuplet 6/4 { r16 \tempo \markup { "Rallentando un poco" } 4 = 64 g!_\f^\([ f a ees b'\)] } s4 \tuplet 7/4 { e,16^\(\tweak X-offset -6^\markup { \italic "(R.H.)" }[ bes c_\> ces a! ges f'_\mp\)] } |
		}
	>>
  }

rhseventyeighttwo = {
	\stemDown
	s4 r8. \slashedGrace { d,,,,8^(_~ } d16)_\mf_~ d8_~ \slashedGrace { d8 } r8 \slashedGrace { d8^(_~ } d8) r8 |
  }

lhseventyeightone = {
	<<
		{
			<c d'>8.[ gis''16]^~ gis2 s4 |
		}
		\new Staff = "LH.2" \with { alignBelowContext = "LH" \clef bass } {
			\global_settings
			\relative c' {
			        \stemDown
				r2 r32\tweak positions #'(-4.5 . -4.5)[ gis\tweak X-offset -10_\ffff^! gis^! r gis^! r r gis^!] r4 |
			}
		}
  >>
  }

lhseventyeighttwo = {
	s4 r8 e16[ \slashedGrace { <c' ges'>8_~ } <c ges'>16]_~ <c ges'>8_~ \slashedGrace { <c ges'>8 } r8
	\slashedGrace { <des g!>8_~^( } <des g>8)[ aes'8] |
  }
