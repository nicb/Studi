%
% bar n.62
%

rhsixtytwoone = {
        \stemUp
	\tupletUp
	\tuplet 3/2 { r4[ dis8^\(_\pp^~] } \tuplet 2/3 { dis16[ e] } \tuplet 2/3 { b'8[\) e,] } a!8.^~ |
  }

rhsixtytwotwo = {
        \stemDown
	\tupletDown
	\tuplet 3/2 { r4[ cis'8]_~ } cis8.[ <cis! e>16_~] <cis e>16[ <ais b>4 <b ais'>8._\p]_~ |
  }

lhsixtytwoone = {
	\stemDown
	\slashedGrace { c'8^(_~ } c8)[ a,8^-_\(]_~ \slashedGrace { a8 }
	\stemUp
	gis4 g!16\)\tweak positions #'(4 . 4)[ a4_\(] fis8\)_\([ f!16]^~ |
  }

lhsixtytwotwo = {
	s1 |
  }
