%
% bar n.23
%

rhtwentythreeone = {
		\clef treble
		s4 r4 \clef bass <dis, e gis>8\tweak X-offset -2_\f^>[
		\change Staff = "LH" <cis, ais c!>8_\f_>]
		\change Staff = "RH" r4                                                               | % bar 23
  }

rhtwentythreetwo = {
		s1 |
  }

lhtwentythreeone = {
		\time 4/4
		\stemNeutral
		\slashedGrace { dis8_\p_\( } f8\)[ \change Staff = "RH"
		\slashedGrace { b''8_\p_\( } a8\)^.] s2
		\change Staff = "LH" r8 \clef treble g!8_\p_\([                                         | % bar 23
  }

lhtwentythreetwo = {
		s1 |
  }

