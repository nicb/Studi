%
% bar n.127
%

rhonehundredandtwentysevenone = {
		\time 8/4
		\stemUp
		r2 r4 r8 <ges bes>4_\markup { \dynamic "p" \italic "dolce" } <g! b!> <e! aes!> <f b> <e! c'>8^~ |
  }

rhonehundredandtwentyseventwo = {
		\stemDown
		bes2.^-_\f_~ bes8 c'4 des c a! d!8_~ |
  }

lhonehundredandtwentysevenone = {
		\stemUp
		<<
			{
				r8 ges''8_-^~ ges2^~ \once \hide NoteHead \once \hide Stem \once \hide Flag ges8
			}
			\new Voice {
				\global_settings
				\relative c {
					\stemUp
					s2 des4^~ <des ges>8 s8 s1 |
				}
			}
		>>
  }

lhonehundredandtwentyseventwo = {
		\stemDown
		r4 r8 b,!^-_~ b4_~ b8
		a'8[^\(_\markup { \dynamic "p" \italic "dolce" } d a f]\)
		ees,[^\( ees' aes des bes]\) |
  }
