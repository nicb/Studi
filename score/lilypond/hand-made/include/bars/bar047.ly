%
% bar n.47
%

rhfortysevenone = {
	\stemDown
	\pitchedTrill
	ees4_\p~\startTrillSpan ces
	ees16 a,8.^-\)\stopTrillSpan \tieSolid
	\stemUp
	\slashedGrace { d!8_(^~ } \tuplet 3/2 { d4) <c! d'>8 }
	\tuplet 3/2 { <b c dis>8_\f <dis, cis' e> <fis a b> } |
  }

rhfortyseventwo = {
	s1 |
  }

lhfortysevenone = {
	f'!4_\p^~ \slashedGrace { f8 } s2. |
  }

lhfortyseventwo = {
	r8
	<<
		\new Voice = "T47.2" {
			\global_settings
			\relative c {
				\stemUp
				\slashedGrace { ais8^~ } ais8^~ \slashedGrace { ais8 } s2. |
			}
		}
		{
			\stemDown
			\slashedGrace { \stemDown fis,8_~ } \tuplet 3/2 { fis8 cis16_~ }
			cis16 ges''8.^-
			\slashedGrace { ges8^~_( } ges8)_~[ \slashedGrace { ges8 } a,8]
			\slashedGrace { gis'8_( } \tuplet 6/4 { e16\tweak X-offset -3_\f)[ a d,,! gis'! g!8] } |
		}
	>>
  }
