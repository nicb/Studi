%
% bar n.114
%

rhonehundredandfourteenone = {
		\stemDown
		cis32[ dis g, b e_\cresc ees des b
		a' ges f gis des' c! ais! gis!
		e' ees, d f_\f \ottava 1 c'' f, dis fis
		cis' b a g f! f dis cis] \ottava 0 | 
  }

rhonehundredandfourteentwo = {
		s1 |
  }

lhonehundredandfourteenone = {
		\stemUp
		c!8_. c_.[ <ais d g>] a![_.  <g! b fis'>] gis_._\f[ <ais! d!>]
		\stemDown \clef bass
		g,!^. | 
  }

lhonehundredandfourteentwo = {
		s1 |
  }
