%
% bar n.53
%
% upper staff
rhfiftythreethree = {
	r4 \tempo \markup { "Tempo" \circle 5 } 4 = 116
	r16 <b'! d>8._~ \tuplet 3/2 { <b d>16\tweak positions #'(-2 . -2)[ a! <c! f cis'!>_\pp]_~ } <c f cis'>4._~ |
  }

rhfiftythreeone = {
	\stemUp
	
	\tuplet 3/2 { <g bes>16_-\tweak positions #'(4 . 4)[ bes_- a_-] } \stemDown <ges' aes>16\tweak X-offset -5_\ppp\shape #'((0 . 0) (0 . -12) (0 . 10) (0 . 0))_\([
	<f g>
	\stemNeutral
	\change Staff = "LH" \stemUp \clef treble <fis, aes>16 \change Staff = "RH" \stemDown <g a!>
	\change Staff = "LH" \stemUp <des'! ees! fes!> <bes! c! d!>]
	\change Staff = "RH" \tweak positions #'(3.5 . -0.3)\tuplet 3/2 { aes16 \change Staff = "LH" <ees! f! g>\)
	\change Staff = "LH.4" <d,,, b'>_\pp } \change Staff = "LH" s4.
  }

rhfiftythreetwo = {
	\stemDown
	s2 s8 r8 <f'! a!>16\shape #'((0 . 0) (8 . 1) (24 . 8) (18 . -10))^\=1(\tweak X-offset -5_\ppp[ \change Staff = "LH" \stemUp <b,! des! d! e>\shape #'((0 . 0) (5 . -1) (15 . -0.5) (21 . 3))_\=2( <ges a bes c>\=2) \change Staff = "RH" \stemDown <ges' a>]\=1) |
  }

lhfiftythreeone = {
	s1 |
  }

lhfiftythreetwo = {
	s1 |
  }

% lower staves 

lhfiftythreethree = {
	s1 |
  }

lhfiftythreefour = {
	\startStaff
	\clef bass
	s2 \once \omit TupletBracket \once \omit TupletNumber \tuplet 3/2 { s8 \once \omit Flag \once \omit Stem <d b'>16^~ } <d b'>4.^~
  }
