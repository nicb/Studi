%
% bar n.60
%

rhsixtyone = {
	\time 3/4
	\set PianoStaff.connectArpeggios = ##f
	\slashedGrace { a'!8_\ppp_(^~ } \tuplet 5/4 { a16)[ <ges aes> <d e> f <ees aes>] }
	\tuplet 3/2 { c8\)[ \arpeggioArrowUp <a'! c! d b'>4_\p_\markup { \italic "marcato" }]\arpeggio }
	s4
	\cadenzaOn
	a,1^-_\ffff^\markup { \draw-line #'(3.5 . 0) "ca.3.5\"" \draw-line #'(3.5 . 0) \arrow-head #X #RIGHT ##f }
	\cadenzaOff |
  }

rhsixtytwo = {
	s2. \cadenzaOn s1 \cadenzaOff |
  }

lhsixtyone = {
	\stemDown
	<bes d>4_~ \tuplet 3/2 { <bes d>8\)[ \arpeggioArrowDown <g! ais cis gis' c!>4\arpeggio] }
	s4
	\cadenzaOn
	gis,1_-
	\cadenzaOff |
  }

lhsixtytwo = {
  	s2. \cadenzaOn s1 \cadenzaOff |
  }
