%
% bar n.93
%

rhninetythreeone = {
		\stemUp
		g16_.[ r] \tuplet 5/4 { r32 a[ \tweak Y-offset 5 r16 b32] } s2. |
  }

rhninetythreetwo = {
		\change Staff = "LH" \stemUp dis,32[
		\change Staff = "RH" \stemDown r32 gis_\( e' dis g f'!\)] r32
		\tupletUp \tuplet 5/4 { dis,32[ b' \change Staff = "LH" \stemUp <fis, g>_. \change Staff = "RH" \stemDown  ais' f] }
		s8 \change Staff = "LH" \stemUp fis,,32^\([ e' dis \change Staff = "RH" \stemDown e'\)
		\tupletDown \tuplet 3/2 { d,!16^\( gis \change Staff = "LH" \stemUp dis,]_\=1( } \change Staff = "RH" \stemDown
		\tuplet 3/2 { <e' d'>16\)[ \change Staff = "LH" \stemUp gis,, cis'\=1) } \change Staff = "RH" \stemDown
		b'32^\( e,\)] r16 |

  }

lhninetythreeone = {
		\stemDown \clef treble
		\slashedGrace { <d'! a'>8^~ } <d a'>16^.[ r8 <fis ais>16^.]	
		r16 d32_\([ \clef bass d,_~ d \change Staff = "RH" f' dis' \change Staff = "LH" ais,\)] s2 |
  }

lhninetythreetwo = {
		s1 |
  }
