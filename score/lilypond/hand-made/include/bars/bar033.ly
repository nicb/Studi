%
% bar n.33
%

rhthirtythreeone = {
	r4 ais_-_\f s8 \clef treble s4. |
  }

rhthirtythreetwo = {
	s1 |
  }

lhthirtythreeone = {
	r8 b4_-_\f r8 <a, c fis>8\tweak Y-offset -7_\ff_>[
	\change Staff = "RH" <ais'' gis' b>^>\tweak X-offset -4\tweak Y-offset -5_\ff]
	\change Staff = "LH" g!_\(_\p
	\change Staff = "RH" \clef bass fis!\)_\p
	\change Staff = "LH"
  }

lhthirtythreetwo = {
	s1 |
  }

