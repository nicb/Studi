%
% bar n.73
%

rhseventythreeone = {
	\stemUp
	s4 r16 f^> r8 r16 cisis^> r8 r16\tweak positions #'(8 . 7.5)[ f^> <fis, g'!>8]^~ |
  }

rhseventythreetwo = {
	\stemDown
	\change Staff = "LH" b,16[ \change Staff = "RH" d! g! c!] 
	dis,[ e' g, b] cis[ bis dis, a'] e'[ ais, <b, a'!>8^~] |
  }

lhseventythreeone = {
	\stemUp
	cis16\tweak positions #'(17 . 18)[ \change Staff = "RH" f!^>] r8 \change Staff = "LH" s2. |
  }

lhseventythreetwo = {
	\stemDown
	\tweak Y-offset -3 r16 <fis gis'>8._> <ais b'>16[
	<cis d'>16_~ \slashedGrace { <cis d'>8 } gis''8_~]^\markup { \italic "crescendo poco a poco" }
	gis8 c,!4.~
  }
