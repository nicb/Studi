%
% bar n.22
%

rhtwentytwoone = {
		\clef bass
		\time 6/4
		\stemNeutral
		fis8_\p^.[ \change Staff = "LH" gis_\p_.]
		\change Staff = "RH" r4 s8 r8 s2 a4_\f^-                                              | % bar 22
  }

rhtwentytwotwo = {
		s1. |
  }

lhtwentytwoone = {
		s4 r8 \clef treble d'8_\f_\([
		\change Staff = "RH" c,8]\) s8
		\change Staff = "LH" \clef bass dis8_._\p[ \change Staff = "RH" cis8^._\p]
		\change Staff = "LH" r8 g4_\f_- r8                                                      | % bar 22
  }

lhtwentytwotwo = {
		s1. |
  }

