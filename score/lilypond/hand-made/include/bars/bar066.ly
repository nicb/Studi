%
% bar n.66
%

rhsixtysixone = {
	\time 3/4
	\clef bass
	\stemUp
	\slashedGrace { g!8_( } b,8.)[ cis!16] gis!4 s4 |
  }

rhsixtysixtwo = {
	\stemDown
 	ais,16_\p[ fis d bis' fis g fis! ais] c[ cis <ais b>32_\f a ges <d ees>] |
  }

lhsixtysixone = {
	\stemUp
	<gis,,, c>2_\p \stemDown r8 <des'' ees g>16_\sfz^! r16
  }

lhsixtysixtwo = {
	s2. |
  }
