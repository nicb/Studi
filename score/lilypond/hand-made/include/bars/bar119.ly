%
% bar n.119
%

rhonehundredandnineteenone = {
		\stemUp
		<c cis'>16[ gis']
		\tempo \markup { "Tempo" \circle 5 ", ben sostenuto" } 4 = 126
		g''!32[_\f f!_\cresc ees! d]
    ais![ c! e, dis
		fis! d! des! b]
		a[ aes g ees
		d e fis gis]
		cis[_\ff ais b c!
		gis' fis des ais'] |
  }

rhonehundredandnineteentwo = {
		s1 |
  }

lhonehundredandnineteenone = {
		\stemUp
		r8 a,_.[\cresc <cis fis>_.]
		ais_.[ <f'! g>_.] c!_.[ <f! a!>_.]_\ff
		s8 |
  }

lhonehundredandnineteentwo = {
		\stemDown
		<g b dis!>8 r s2 s8
		f^.[ \allowBreak |
  }
