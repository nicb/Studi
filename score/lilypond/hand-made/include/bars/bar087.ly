%
% bar n.87
%

rheightysevenone = {
		<<
			{
				a32_\([ fis' dis,\) r \slashedGrace { f8_( } e'8])_\ff_-^~ e4~ e8.[
			  \slashedGrace { <bes, e des'>8^~ } <bes e des'>16]_-_>^~ <bes e des'>4^~ |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemUp
					s4. dis8_-^~ dis8. s16 s4 |
				}
			}
		>>
  }

rheightyseventwo = {
		\stemDown
		<<
			{
				s4 ais'4^-_~ ais8. s16 s4 |
			}
			\new Voice {
				\global_settings
				\relative c'' {
					\stemDown
					s2 s16 b16[^-_~ b] s16 s4 |
				}
			}
		>>
  }

lheightysevenone = {
		\stemDown
		r16 cis,^.\tweak positions #'(-2 . -2)[ r8]
		r16[ <e, g>_\fp] r[ <fis gis>] \stemUp r8.\tweak positions #'(3 . 3)[
		\slashedGrace { g,!8^~_( } g16)]\tweak X-offset -4_\ff_-_>^~ g4^~ |
  }

lheightyseventwo = {
    s1 |
  }
