%
% bar n.106
%

rhonehundredandsixone = {
		\slashedGrace { a8_( } fisis8)\tweak positions #'(6 . 6)[ cis e \tweak Y-offset 4.5 r gis fis]
		\tempo \markup { "Bruscamente Tempo" \circle 6 } 4 = 79
		s4 |
  }

rhonehundredandsixtwo = {
		cis8 c!4^-^> d8^-^>_~ d4_~ \slashedGrace { d8 }
		\once \override Hairpin.circled-tip = ##t
		\after 8.. \!
		ees,4_\ff\>
  }

lhonehundredandsixone = {
		ais16[ cis] r ais[ d fisis] r fis,,[ c' b'] r f! ais![ r
		\clef treble
		\tieDashed
		\pitchedTrill
		\after 16.. \ff
		g'!8]_\pp\<_~\startTrillSpan ais |
  }

lhonehundredandsixtwo = {
		s1 |
  }
