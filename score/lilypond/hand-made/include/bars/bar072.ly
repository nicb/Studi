%
% bar n.72
%

rhseventytwoone = {
	\stemUp
	e16[ des8.^~] des16[ r8 <c dis f ges>16_\pp]^~ <c dis f ges>2 |
  }

rhseventytwotwo = {
	\stemDown
	dis16[ c g gis] f![ r8.] s2 |
  }

lhseventytwoone = {
	s4 r8.[ g'16] a8[ c,] g16[ cis8.]^~ |
  }

lhseventytwotwo = {
	\stemUp
	<<
		{ \tupletUp \tuplet 3/2 { cis8\tweak positions #'(4 . 2)[ <a! ais'> fis]^~ } fis2. | }
		\new Voice {
			\global_settings
			\relative c {
				\stemDown
				s4 s8. <d f>16_\pp_\< s4 \tweak Y-offset -1 r16 bis'[_\p dis, e] |
			}
		}
	>>
  }

