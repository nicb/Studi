%
% bar 1
%
rhoneone = {
		\stemNeutral
		\tempo \markup { "Tempo " \circle 1 ", energicamente" } 4 = 46
		<<
		{ \tuplet 5/4 { r4 \ottava 1 d''8_\mf \ottava 0 r4 } s4 \tuplet 5/4 { r4 e,16\mf~ } | }          % bar 1

		\new Staff \with { alignBelowContext = "RH" } { 
		<<
			\relative c' {
				\clef treble
				\global_settings
				\stemUp
				r8. \acciaccatura b''8_( b16_\mf)~ b4~ b16.[ \slurDown c,,32_\f( a b aes f')] s4 | % bar 1
				\stopStaff
			} \\
			\relative c' {
				\global_settings
				\stemDown
				s2 \acciaccatura aes''8~_( aes16) s4..                                           | % bar 1
			}
		>>
		}
		>>
	}

rhonetwo = {
		\stemDown
		s1 |
	}

lhoneone = {
		r32 e32(_\mp\<[ g ais gis a c dis_\f\!] \stemDown f[ fis c bes) r32 g,,16_\mf_._- f''32_\f\(]
		\stemUp d[ e\) \slashedGrace { d,,32_( g } f8.\mf~)] \tuplet 3/2 { f8 e''4 } |
	}

lhonetwo = {
		\stemDown
		s1 |
	}
