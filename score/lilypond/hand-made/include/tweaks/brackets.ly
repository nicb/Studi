%
% Vertical bracket parenthesis attempt
%
lbrack = \markup { \path #0.1 #'((moveto 0 0) (rlineto -0.5 0)(rlineto 0 1.5)(rlineto 0.5 0)) }

rbrack = \markup { \path #0.1 #'((moveto 0 0) (rlineto 0.5 0)(rlineto 0 1.5)(rlineto -0.5 0)) }
