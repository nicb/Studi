%
% upper staff when needed
%
upperthree = {
	\relative c'' {
		\global_settings
		\stopStaff
		\omit Staff.Clef
		\skiponefortyseven
		\rhfortyeightthree
	        \startStaff
		\rhfortyninethree
		\rhfiftythree
		\rhfiftyonethree
		\rhfiftytwothree
		\rhfiftythreethree
		\rhfiftyfourthree
		\rhfiftyfivethree
		\rhfiftysixthree
		\rhfiftyseventhree
		\rhfiftyeightthree
		\rhfiftyninethree
	}
}
