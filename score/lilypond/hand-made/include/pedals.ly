%
% this is a separate line for the pedals
% (so that they can be precisely defined and come all at the bottom)
% 
sustain = {
    \global_settings
    s1 | s1 | s2 \tuplet 5/4 { s4 s16\sustainOn } s8 \tuplet 5/4 { s32\sustainOff s16 s16 } | % bar 3
    s2. \tuplet 5/4 { s16 s16\sustainOn s8. }                                               | % bar 4
    	s2 s8 \tuplet 3/2 { s8 s16\sustainOff } s4                                          | % bar 5
    	s2 s16\sustainOn s8.\sustainOff\sustainOn s4 \tuplet 3/2 { s8\sustainOff s8 s8 }    | % bar 6
    	s2.                                                                                 | % bar 7
	\tuplet 5/4 { s16. s16\sustainOn } s8 s8 \slashedGrace { s8\sustainOff\sustainOn }
	s8 s16\sustainOff s8. \tuplet 5/4 { s16\sustainOn s4\sustainOff }                   | % bar 8
	s2 s2\sustainOn                                                                     | % bar 9
	s2 s2\sustainOff                                                                    | % bar 10
	s1                                                                                  | % bar 11
	s1                                                                                  | % bar 12
	s1                                                                                  | % bar 13
	s1                                                                                  | % bar 14
	s1                                                                                  | % bar 15
	s1                                                                                  | % bar 16
	s1                                                                                  | % bar 17
	s1                                                                                  | % bar 18
	s1                                                                                  | % bar 19
	s2.                                                                                 | % bar 20
	s1. s4                                                                              | % bar 21
	s1.                                                                                 | % bar 22
	s1                                                                                  | % bar 23
	s1 s4                                                                               | % bar 24
	s2.                                                                                 | % bar 25
	s1 s4                                                                               | % bar 26
	s1                                                                                  | % bar 27
	s2.                                                                                 | % bar 28
	s1.                                                                                 | % bar 29
	s1.                                                                                 | % bar 30
	s1 | s1 | s1 | s1 | s1                                                              | % bars 31-35
	s1 | s1 | s1 | s1 |  s1 s4 | s1                                                     | % bars 36-41
	s2 s8\sustainOn s8\sustainOff\sustainOn                                             | % bar 42
	\slashedGrace { s8\sustainOff } s1                                                  | % bar 43
	s1                                                                                  | % bar 44
	s2. s4\sustainOn                                                                    | % bar 45
	\slashedGrace { s8 } s1\sustainOff                                                  | % bar 46
	s1                                                                                  | % bar 47
	s1                                                                                  | % bar 48
	s1                                                                                  | % bar 49
	s1                                                                                  | % bar 50
	s2.                                                                                 | % bar 51
	s1                                                                                  | % bar 52
	s8 s4.\sustainOn s8\sustainOff\sustainOn s4.                                        | % bar 53
	s4 s2\sustainOff\sustainOn \tuplet 5/4 { s4 s16\sustainOff\sustainOn }              | % bar 54
	s1                                                                                  | % bar 55
	s2 \slashedGrace { s8\sustainOff\sustainOn } s2 s8
	s8\sustainOff\sustainOn s2.\sustainOff\sustainOn                                    | % bar 56
	s2 s8 s4\sustainOff\sustainOn s8                                                    | % bar 57
	s8 s4 s4\sustainOff\sustainOn s4 s16. s32\sustainOff                                | % bar 58
	\cadenzaOn s1\sustainOn \cadenzaOff s1\sustainOff                                   | % bar 59
	s4\sustainOn \tuplet 3/2 { s8 s4\sustainOff\sustainOn }
	\cadenzaOn s1\sustainOff\sustainOn \cadenzaOff s4                                   | % bar 60
	\tuplet 20/16 { s8. s4\sustainOff\sustainOn s4 s16
	s4\sustainOff\sustainOn s8 s8\sustainOff\sustainOn  }                               | % bar 61
	s1\sustainOff                                                                       | % bar 62
	s1 s8                                                                               | % bar 63
	s1 s4                                                                               | % bar 64
	s1                                                                                  | % bar 65
	s2.                                                                                 | % bar 66
	s1                                                                                  | % bar 67
	s2.                                                                                 | % bar 68
	s1                                                                                  | % bar 69
	s1                                                                                  | % bar 70
	s1                                                                                  | % bar 71
	s1                                                                                  | % bar 72
	s1                                                                                  | % bar 73
	s1                                                                                  | % bar 74
	s4 s2\sustainOn s8.. s32\sustainOff                                                 | % bar 75
	s1                                                                                  | % bar 76
	s1 s1                                                                               | % bar 77
	s1                                                                                  | % bar 78
	s1                                                                                  | % bar 79
	s4. \slashedGrace { s8\sustainOn } s8 \tuplet 12/8 { s4. s8 s16\sustainOff\sustainOn s8. }                                | % bar 80
	s4 s32\sustainOff s8.. s2                                                           | % bar 81
	s4 s8\sustainOn s8\sustainOff\sustainOn s4 s8... s64\sustainOff                     | % bar 82
	s1                                                                                  | % bar 83
	s2. \cadenzaOn s4 \cadenzaOff s4                                                    | % bar 84
	s1                                                                                  | % bar 85
	s1                                                                                  | % bar 86
	s1                                                                                  | % bar 87
	s1                                                                                  | % bar 88
	s1                                                                                  | % bar 89
	s1                                                                                  | % bar 90
	s1                                                                                  | % bar 91
	s1                                                                                  | % bar 92
	s1                                                                                  | % bar 93
	s1                                                                                  | % bar 94
	s1                                                                                  | % bar 95
	s1                                                                                  | % bar 96
	s1                                                                                  | % bar 97
	s1                                                                                  | % bar 98
	s1                                                                                  | % bar 99
	s2\sustainOn s8... s64\sustainOff s4                                                | % bar 100
	s2... s16\sustainOn                                                                 | % bar 101
	s2..... s64\sustainOff                                                              | % bar 102
	s2. s4\sustainOn                                                                    | % bar 103
	s2. s4\sustainOff                                                                   | % bar 104
	s1                                                                                  | % bar 105
	s1                                                                                  | % bar 106
	s1                                                                                  | % bar 107
	s1                                                                                  | % bar 108
	s1 s1                                                                               | % bar 109
	s1                                                                                  | % bar 110
	s4 s16 s8.\sustainOn s4.... s64\sustainOff                                          | % bar 111
	s1                                                                                  | % bar 112
	s1                                                                                  | % bar 113
	s1                                                                                  | % bar 114
	s1                                                                                  | % bar 115
	s1                                                                                  | % bar 116
	s1                                                                                  | % bar 117
	s1                                                                                  | % bar 118
	s1                                                                                  | % bar 119
	s1                                                                                  | % bar 120
	s1                                                                                  | % bar 121
	s1                                                                                  | % bar 122
	s1                                                                                  | % bar 123
	s1                                                                                  | % bar 124
	s1                                                                                  | % bar 125
	s1 s8                                                                               | % bar 126
	s1 s1                                                                               | % bar 127
	s1\sustainOn s1 s8\sustainOff                                                       | % bar 128
}

unaC = {
    \global_settings
        s1 | s1 | s2 \tuplet 5/4 { s4 s16 } s8 \tuplet 5/4 { s32 s16 s16\unaCorda } 	    | % bar 3
        s2.\treCorde \tuplet 5/4 { s16 s16 s8. }                                            | % bar 4
        s1 |
        s2 s16\unaCorda s8. s4 \tuplet 3/2 { s8 s8 s8 }                                     | % bar 6
        s4 s8 \tuplet 3/2 { s8\treCorde s16 } s4                                            | % bar 7
	s1                                                                                  | % bar 8
	s2 s2\unaCorda                                                                      | % bar 9
	s2 s2\treCorde                                                                      | % bar 10
	s1                                                                                  | % bar 11
	s1                                                                                  | % bar 12
	\tuplet 5/4 { s4 s1\unaCorda }                                                      | % bar 13
	s1                                                                                  | % bar 14
	\tuplet 5/4 { s1 s4\treCorde }                                                      | % bar 15
	s1                                                                                  | % bar 16
	s1                                                                                  | % bar 17
	s1                                                                                  | % bar 18
	s1                                                                                  | % bar 19
	s2.                                                                                 | % bar 20
	s1. s4                                                                              | % bar 21
	s1.                                                                                 | % bar 22
	s1                                                                                  | % bar 23
	s1 s4                                                                               | % bar 24
	s2.                                                                                 | % bar 25
	s1 s4                                                                               | % bar 26
	s1                                                                                  | % bar 27
	s2.                                                                                 | % bar 28
	s1.                                                                                 | % bar 29
	s1.                                                                                 | % bar 30
	s1 | s1 | s1 | s1 | s1                                                              | % bars 31-35
	s1 | s1 | s1 | s1 | s1 s4 | s1                                                      | % bars 36-41
	s2.                                                                                 | % bar 42
	s1                                                                                  | % bar 43
	s1                                                                                  | % bar 44
	s1                                                                                  | % bar 45
	s1                                                                                  | % bar 46
	s1                                                                                  | % bar 47
	s1                                                                                  | % bar 48
	s1                                                                                  | % bar 49
	s1                                                                                  | % bar 50
	s2.                                                                                 | % bar 51
	s1                                                                                  | % bar 52
	s8 s4.\unaCorda s2                                                                  | % bar 53
	s1                                                                                  | % bar 54
	s1                                                                                  | % bar 55
	s2 \slashedGrace { s8\treCorde } s2. \slashedGrace { s8\unaCorda } s2.              | % bar 56
	s4 s32\treCorde s8.. s2                                                             | % bar 57
	s1                                                                                  | % bar 58
	\cadenzaOn s1   \cadenzaOff s2. s4\unaCorda                                         | % bar 59
	s4 \tuplet 3/2 { s8 s4\treCorde } \cadenzaOn s1 \cadenzaOff s4                      | % bar 60
	s1\unaCorda                                                                         | % bar 61
	s1\treCorde                                                                         | % bar 62
	s1 s8                                                                               | % bar 63
	s1 s4                                                                               | % bar 64
	s1                                                                                  | % bar 65
	s2.                                                                                 | % bar 66
	s1                                                                                  | % bar 67
	s2.                                                                                 | % bar 68
	s1                                                                                  | % bar 69
	s1                                                                                  | % bar 70
	s1                                                                                  | % bar 71
	s1                                                                                  | % bar 72
	s1                                                                                  | % bar 73
	s1                                                                                  | % bar 74
	s1                                                                                  | % bar 75
	s1                                                                                  | % bar 76
	s1 s1                                                                               | % bar 77
	s1                                                                                  | % bar 78
	s1                                                                                  | % bar 79
	s1                                                                                  | % bar 80
	s1                                                                                  | % bar 81
	s1                                                                                  | % bar 82
	s1                                                                                  | % bar 83
	s2. \cadenzaOn s4 \cadenzaOff s4                                                    | % bar 84
	s1                                                                                  | % bar 85
	s1                                                                                  | % bar 86
	s1                                                                                  | % bar 87
	s1                                                                                  | % bar 88
	s1                                                                                  | % bar 89
	s1                                                                                  | % bar 90
	s1                                                                                  | % bar 91
	s1                                                                                  | % bar 92
	s1                                                                                  | % bar 93
	s1                                                                                  | % bar 94
	s1                                                                                  | % bar 95
	s1                                                                                  | % bar 96
	s1                                                                                  | % bar 97
	s1                                                                                  | % bar 98
	s1                                                                                  | % bar 99
	s1                                                                                  | % bar 100
	s1                                                                                  | % bar 101
	s1                                                                                  | % bar 102
	s1                                                                                  | % bar 103
	s1                                                                                  | % bar 104
	s1                                                                                  | % bar 105
	s1                                                                                  | % bar 106
	s1                                                                                  | % bar 107
	s1                                                                                  | % bar 108
	s1 s1                                                                               | % bar 109
	s1                                                                                  | % bar 110
	s1                                                                                  | % bar 111
	s1                                                                                  | % bar 112
	s1                                                                                  | % bar 113
	s1                                                                                  | % bar 114
	s1                                                                                  | % bar 115
	s1                                                                                  | % bar 116
	s1                                                                                  | % bar 117
	s1                                                                                  | % bar 118
	s1                                                                                  | % bar 119
	s1                                                                                  | % bar 120
	s1                                                                                  | % bar 121
	s1                                                                                  | % bar 122
	s1                                                                                  | % bar 123
	s1                                                                                  | % bar 124
	s1                                                                                  | % bar 125
	s1 s8                                                                               | % bar 126
	s4\sustainOn s2 s8 s8\sustainOff s1                                                 | % bar 127
	s1 s1 s8         							                                                      | % bar 128
}

tonal = {
    \global_settings
        s1 | s1 | s1 | s1 | s1 |
        s2 s16 s8. s4 \tuplet 3/2 { s8 s8 s8\sostenutoOn }                                  | % bar 6
        s4\sostenutoOff s8 \tuplet 3/2 { s8 s16 } s4                                        | % bar 7
	s1                                                                                  | % bar 8
	s1                                                                                  | % bar 9
	s1                                                                                  | % bar 10
	s1                                                                                  | % bar 11
	s2.. s8\sostenutoOn                                                                 | % bar 12
	s1                                                                                  | % bar 13
	s4 s4\sostenutoOff \slashedGrace { s8\sostenutoOn } s2                              | % bar 14
	s4 \slashedGrace { s8\sostenutoOff } s2.                                            | % bar 15
	s1                                                                                  | % bar 16
	s1                                                                                  | % bar 17
	s1                                                                                  | % bar 18
	s1                                                                                  | % bar 19
	s2.                                                                                 | % bar 20
	s1. s4                                                                              | % bar 21
	s1.                                                                                 | % bar 22
	s1                                                                                  | % bar 23
	s1 s4                                                                               | % bar 24
	s2.                                                                                 | % bar 25
	s1 s4                                                                               | % bar 26
	\tuplet 5/4 { s4. s8\sostenutoOn s8 } s4 \tuplet 5/4 { s8. s8\sostenutoOff }        | % bar 27
	s2.                                                                                 | % bar 28
	s8 s8\sostenutoOn s1 s4\sostenutoOff                                                | % bar 29
	s1.                                                                                 | % bar 30
	s1 | s1 | s1 | s1 | s1                                                              | % bars 31-35
	s4 s2.\sostenutoOn                                                                  | % bar 36
	s2. s4\sostenutoOff                                                                 | % bar 37
	s2. s4\sostenutoOn                                                                  | % bar 38
	s4 \tuplet 3/2 { s8 s4\sostenutoOff\sostenutoOn } s4 s4\sostenutoOff                | % bar 39
	s1 s4 | s1 | s2.                                                                    | % bar 40-42
	s1                                                                                  | % bar 43
	s1                                                                                  | % bar 44
	s1                                                                                  | % bar 45
	s1                                                                                  | % bar 46
	s4\sostenutoOn s16\sostenutoOff s8. s2                                              | % bar 47
	s1\sostenutoOn                                                                      | % bar 48
	s2... s16\sostenutoOff\sostenutoOn                                                  | % bar 49
	s1                                                                                  | % bar 50
	s4 s4\sostenutoOff s8 s8\sostenutoOn                                                | % bar 51
	s8 s4\sostenutoOff s8\sostenutoOn s4... s32\sostenutoOff                            | % bar 52
	s1                                                                                  | % bar 53
	s1                                                                                  | % bar 54
	s1                                                                                  | % bar 55
	s1*2                                                                                | % bar 56
	s1                                                                                  | % bar 57
	s1                                                                                  | % bar 58
	\cadenzaOn s1   \cadenzaOff s1                                                      | % bar 59
	s2 \cadenzaOn s1 \cadenzaOff s4                                                     | % bar 60
	s1                                                                                  | % bar 61
	s1                                                                                  | % bar 62
	s1 s8                                                                               | % bar 63
	s1 s4                                                                               | % bar 64
	s1                                                                                  | % bar 65
	s2.                                                                                 | % bar 66
	s1                                                                                  | % bar 67
	s2.                                                                                 | % bar 68
	s1                                                                                  | % bar 69
	s1                                                                                  | % bar 70
	s1                                                                                  | % bar 71
	s4 s16 s8.\sostenutoOn s4 s8.. s32\sostenutoOff                                     | % bar 72
	s2.. s8\sostenutoOn                                                                 | % bar 73
	s4... s32\sostenutoOff s2                                                           | % bar 74
	s1                                                                                  | % bar 75
	s1                                                                                  | % bar 76
	s1 s1                                                                               | % bar 77
	s8. s16\sostenutoOn s2 s4\sostenutoOff                                              | % bar 78
	s4 s4\sostenutoOn s4... s32\sostenutoOff                                            | % bar 79
	s1                                                                                  | % bar 80
	s1                                                                                  | % bar 81
	s1                                                                                  | % bar 82
	s1                                                                                  | % bar 83
	s2. \cadenzaOn s4 \cadenzaOff s4                                                    | % bar 84
	s2. s8\sostenutoOn s16.. s64\sostenutoOff                                           | % bar 85
	s4 s4.\sostenutoOn s8\sostenutoOff s4                                               | % bar 86
	s1                                                                                  | % bar 87
	s1                                                                                  | % bar 88
	s1                                                                                  | % bar 89
	s1                                                                                  | % bar 90
	s1                                                                                  | % bar 91
	s1                                                                                  | % bar 92
	s1                                                                                  | % bar 93
	s1                                                                                  | % bar 94
	s1                                                                                  | % bar 95
	s1                                                                                  | % bar 96
	s1                                                                                  | % bar 97
	s1                                                                                  | % bar 98
	s1                                                                                  | % bar 99
	s1                                                                                  | % bar 100
	s4 s2\sostenutoOn s4\sostenutoOff                                                   | % bar 101
	s1                                                                                  | % bar 102
	s1                                                                                  | % bar 103
	s2.. s8\sostenutoOn                                                                 | % bar 104
	s4. s8\sostenutoOff s4 s8\sostenutoOn s4                                            | % bar 105
	s8 s4\sostenutoOff s8\sostenutoOn s4 s4\sostenutoOff                                | % bar 106
	s1                                                                                  | % bar 107
	s1                                                                                  | % bar 108
	s1 s1                                                                               | % bar 109
	s1                                                                                  | % bar 110
	s1                                                                                  | % bar 111
	s1                                                                                  | % bar 112
	s1                                                                                  | % bar 113
	s1                                                                                  | % bar 114
	s1                                                                                  | % bar 115
	s1                                                                                  | % bar 116
	\tuplet 15/12 { s8. s4 s4\sostenutoOn s4\sostenutoOff } s4                          | % bar 117
	s1                                                                                  | % bar 118
	s1                                                                                  | % bar 119
	s1                                                                                  | % bar 120
	s1                                                                                  | % bar 121
	s1                                                                                  | % bar 122
	s1                                                                                  | % bar 123
	s1                                                                                  | % bar 124
	s1                                                                                  | % bar 125
	s1 s8                                                                               | % bar 126
	s1 s1                                                                               | % bar 127
	s1 s1 s8         							                                                      | % bar 128
}
