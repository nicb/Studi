#!/usr/bin/env ruby

require 'erb'
require 'humanize'

class Bar

  NBARS  = 212
  OFFSET = 3
  BARDIR = 'bars'
  INCDIR = 'include'
  BAR_FACTORY= File.join('.', INCDIR, 'bars.ly')
  TEMPLATE =<<EOF
%
% bar n.<%= self.number %>
%
<% self.symbols.each do |s| %>
<%= s.variable_decl %> = {
  }
<% end %>
EOF

  class FileExisting < StandardError; end

  class Voice

    attr_reader   :symbol, :number, :bar_number
    attr_accessor :file

    def initialize(bn, sym, n)
      @bar_number = bn
      @symbol = sym
      @number = n
    end

    def file_name
      return sprintf("%s%d", self.symbol.to_s, self.number)
    end

    def variable_decl
      return sprintf("%s%s%s", self.symbol.to_s, self.bar_number, self.number.humanize)
    end

    def variable_use
      return sprintf("\t\t\t\\%s", self.variable_decl)
    end

    def add_usage
      self.file.puts(self.variable_use)
    end

  end
	
  attr_reader :number, :factory_fh, :template, :symbols, :rhv1, :rhv2, :lhv1, :lhv2

  def initialize(n)
    @number = n
    @template = ERB.new(TEMPLATE)
    @symbols = [
      Voice.new(self.to_words, :rh, 1), Voice.new(self.to_words, :rh, 2),
      Voice.new(self.to_words, :lh, 1), Voice.new(self.to_words, :lh, 2),
    ]
  end

  def bar_file_name
    return sprintf("bar%03d.ly", self.number)
  end

  def bar_path
    return File.join('.', BARDIR, self.bar_file_name)
  end 

  def bar_full_path
    return File.join('.', INCDIR, self.bar_path)
  end

  def to_words
    return self.number.humanize.gsub(/[ \-]/, '')
  end

  def include_it
    return sprintf("\\include \"%s\"", self.bar_path) 
  end

  def create_bar
    raise FileExisting if File.exist?(self.bar_full_path)
    File.open(self.bar_full_path, 'w') { |fh| fh.puts(self.template.result(binding)) }
  end

  def add_to_factory
    self.factory_fh.puts(self.include_it)
  end

  def add_usage
    self.symbols.each { |s| s.add_usage }
  end

  def create
    begin
      self.create_bar
    rescue FileExisting
      STDERR.puts("File #{self.bar_full_path} existing. Not overwriting")
    end
    open_global_files
    self.add_to_factory
    self.add_usage
    close_global_files
  end

private

  def open_global_files
    @factory_fh = File.open(BAR_FACTORY, 'a')
    self.symbols.each { |s| s.file = File.open(s.file_name + '.TO_BE_COPIED_AND_DELETED', 'a') }
  end

  def close_global_files
    [self.factory_fh].each { |fh| fh.close }
    self.symbols.each { |s| s.file.close }
  end

end

Bar::OFFSET.upto(Bar::NBARS) do
  |n|
  b = Bar.new(n)
  b.create
  STDOUT.write('.')
end
STDOUT.write("\n")
