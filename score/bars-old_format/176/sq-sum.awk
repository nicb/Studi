BEGIN {
	root = 3.5;
	k = 1;
}
END {
	printf "%38.6f\n", total;
}
{
	sroot = ($1^(1/root))*k;
	total += sroot;

	printf "%8.6f --> %8.6f (total=%8.6f)\n", $1, sroot, total;
}
