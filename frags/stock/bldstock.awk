# script needed to build the fragment of Stockhausen KlavierStuck IX
BEGIN {
	start = 75; end = start + 38;

	print "# Stockhausen KlavierStucke IX";

	for (i = start; i < end; ++i)
		print i ":8:c#2:1:8\n" \
			i ":8:f#2:1:8\n" \
			i ":8:g2:1:8\n" \
			i ":8:c3:1:8";
}
