# monk.awk: awk script to process Monk's Blue Monk
# This is now tuned on the first excerpt (monk.dat)
#
function find(note)
{
	for (i = 0; i < ik; ++i)
		if (note == keys_f[i] || note == keys_s[i])
			return i;

	print "note not found!";
	exit -1;
}
function transpose(n, t)
{
	if (n == "rest")
		return n;

	nt = (find (n) + t);
	if (match(n,"#"))
		return keys_s[(nt < 0) ? nt + 88 : (nt > 88) ? nt - 88 : nt];
	else
		return keys_f[(nt < 0) ? nt + 88 : (nt > 88) ? nt - 88 : nt];
}
function convert(s,b)
{
	return (conv/b) * s * space;
}
function at_time(s,b)
{
	return (convert(s-1,b)+bar+offset) % page;
}
BEGIN {
	FS=":";
	tempo = 124; # mm
	page = 30.4; # cms
	space = 5 # cms/sec on the page
	transpose_factor = +8; # yup, no transposition for this one!

	conv = ((60/tempo)*4);
	_1_bar = conv * space;
	offset = 47.669108 - (_1_bar/12*5); # initial offset
	bar = 0;

#	keyboard representation

	ik = 0; jk = 0;
	keys_f[ik++] = "a-1";	keys_s[jk++] = "a-1";
	keys_f[ik++] = "bf-1";	keys_s[jk++] = "a#-1";
	keys_f[ik++] = "b-1";	keys_s[jk++] = "b-1";
	keys_f[ik++] = "c0";	keys_s[jk++] = "c0";
	keys_f[ik++] = "df0";	keys_s[jk++] = "c#0";
	keys_f[ik++] = "d0";	keys_s[jk++] = "d0";
	keys_f[ik++] = "ef0";	keys_s[jk++] = "d#0";
	keys_f[ik++] = "e0";	keys_s[jk++] = "e0";
	keys_f[ik++] = "f0";	keys_s[jk++] = "f0";
	keys_f[ik++] = "gf0";	keys_s[jk++] = "f#0";
	keys_f[ik++] = "g0";	keys_s[jk++] = "g0";
	keys_f[ik++] = "af0";	keys_s[jk++] = "g#0";
	keys_f[ik++] = "a0";	keys_s[jk++] = "a0";
	keys_f[ik++] = "bf0";	keys_s[jk++] = "a#0";
	keys_f[ik++] = "b0";	keys_s[jk++] = "b0";
	keys_f[ik++] = "c1";	keys_s[jk++] = "c1";
	keys_f[ik++] = "df1";	keys_s[jk++] = "c#1";
	keys_f[ik++] = "d1";	keys_s[jk++] = "d1";
	keys_f[ik++] = "ef1";	keys_s[jk++] = "d#1";
	keys_f[ik++] = "e1";	keys_s[jk++] = "e1";
	keys_f[ik++] = "f1";	keys_s[jk++] = "f1";
	keys_f[ik++] = "gf1";	keys_s[jk++] = "f#1";
	keys_f[ik++] = "g1";	keys_s[jk++] = "g1";
	keys_f[ik++] = "af1";	keys_s[jk++] = "g#1";
	keys_f[ik++] = "a1";	keys_s[jk++] = "a1";
	keys_f[ik++] = "bf1";	keys_s[jk++] = "a#1";
	keys_f[ik++] = "b1";	keys_s[jk++] = "b1";
	keys_f[ik++] = "c2";	keys_s[jk++] = "c2";
	keys_f[ik++] = "df2";	keys_s[jk++] = "c#2";
	keys_f[ik++] = "d2";	keys_s[jk++] = "d2";
	keys_f[ik++] = "ef2";	keys_s[jk++] = "d#2";
	keys_f[ik++] = "e2";	keys_s[jk++] = "e2";
	keys_f[ik++] = "f2";	keys_s[jk++] = "f2";
	keys_f[ik++] = "gf2";	keys_s[jk++] = "f#2";
	keys_f[ik++] = "g2";	keys_s[jk++] = "g2";
	keys_f[ik++] = "af2";	keys_s[jk++] = "g#2";
	keys_f[ik++] = "a2";	keys_s[jk++] = "a2";
	keys_f[ik++] = "bf2";	keys_s[jk++] = "a#2";
	keys_f[ik++] = "b2";	keys_s[jk++] = "b2";
	keys_f[ik++] = "c3";	keys_s[jk++] = "c3";
	keys_f[ik++] = "df3";	keys_s[jk++] = "c#3";
	keys_f[ik++] = "d3";	keys_s[jk++] = "d3";
	keys_f[ik++] = "ef3";	keys_s[jk++] = "d#3";
	keys_f[ik++] = "e3";	keys_s[jk++] = "e3";
	keys_f[ik++] = "f3";	keys_s[jk++] = "f3";
	keys_f[ik++] = "gf3";	keys_s[jk++] = "f#3";
	keys_f[ik++] = "g3";	keys_s[jk++] = "g3";
	keys_f[ik++] = "af3";	keys_s[jk++] = "g#3";
	keys_f[ik++] = "a3";	keys_s[jk++] = "a3";
	keys_f[ik++] = "bf3";	keys_s[jk++] = "a#3";
	keys_f[ik++] = "b3";	keys_s[jk++] = "b3";
	keys_f[ik++] = "c4";	keys_s[jk++] = "c4";
	keys_f[ik++] = "df4";	keys_s[jk++] = "c#4";
	keys_f[ik++] = "d4";	keys_s[jk++] = "d4";
	keys_f[ik++] = "ef4";	keys_s[jk++] = "d#4";
	keys_f[ik++] = "e4";	keys_s[jk++] = "e4";
	keys_f[ik++] = "f4";	keys_s[jk++] = "f4";
	keys_f[ik++] = "gf4";	keys_s[jk++] = "f#4";
	keys_f[ik++] = "g4";	keys_s[jk++] = "g4";
	keys_f[ik++] = "af4";	keys_s[jk++] = "g#4";
	keys_f[ik++] = "a4";	keys_s[jk++] = "a4";
	keys_f[ik++] = "bf4";	keys_s[jk++] = "a#4";
	keys_f[ik++] = "b4";	keys_s[jk++] = "b4";
	keys_f[ik++] = "c5";	keys_s[jk++] = "c5";
	keys_f[ik++] = "df5";	keys_s[jk++] = "c#5";
	keys_f[ik++] = "d5";	keys_s[jk++] = "d5";
	keys_f[ik++] = "ef5";	keys_s[jk++] = "d#5";
	keys_f[ik++] = "e5";	keys_s[jk++] = "e5";
	keys_f[ik++] = "f5";	keys_s[jk++] = "f5";
	keys_f[ik++] = "gf5";	keys_s[jk++] = "f#5";
	keys_f[ik++] = "g5";	keys_s[jk++] = "g5";
	keys_f[ik++] = "af5";	keys_s[jk++] = "g#5";
	keys_f[ik++] = "a5";	keys_s[jk++] = "a5";
	keys_f[ik++] = "bf5";	keys_s[jk++] = "a#5";
	keys_f[ik++] = "b5";	keys_s[jk++] = "b5";
	keys_f[ik++] = "c6";	keys_s[jk++] = "c6";
	keys_f[ik++] = "df6";	keys_s[jk++] = "c#6";
	keys_f[ik++] = "d6";	keys_s[jk++] = "d6";
	keys_f[ik++] = "ef6";	keys_s[jk++] = "d#6";
	keys_f[ik++] = "e6";	keys_s[jk++] = "e6";
	keys_f[ik++] = "f6";	keys_s[jk++] = "f6";
	keys_f[ik++] = "gf6";	keys_s[jk++] = "f#6";
	keys_f[ik++] = "g6";	keys_s[jk++] = "g6";
	keys_f[ik++] = "af6";	keys_s[jk++] = "g#6";
	keys_f[ik++] = "a6";	keys_s[jk++] = "a6";
	keys_f[ik++] = "bf6";	keys_s[jk++] = "a#6";
	keys_f[ik++] = "b6";	keys_s[jk++] = "b6";
	keys_f[ik++] = "c7";	keys_s[jk++] = "c7";
}
/^#/ {
	print # kill comments
}
/^=/ {
	bar += _1_bar;
	print
}
/^[0-9]/ {
	printf("%4s -> %7.4f %6s %7.4f\n", $3, at_time($1,$2),
		transpose($3,transpose_factor), convert($4,$5));
}
