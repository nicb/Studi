#!	/usr/bin/perl
#
# $Id$
#
# this is an attempt to draw the form of the studi with all the
# information we can add
#
# the form is picked up from ../notes_d.dat and it is re-arranged in
# pic format
#
use strict;

#
# main
#

my $line = "";

while ($line = <>)
{
	if ($line !~ /^=/)
	{
		my %fragment = ();
		convert_frag($line, \%fragment);
		print join('=', %fragment),"\n";
	}
}

#
# endof main
#
sub convert_frag($$)
{
	my ($fragline, $rh_ret) = @_;
	my @info = ();

	$fragline =~ s/(\s)+/ /g;

	($rh_ret->{'number'}, $rh_ret->{'at'}, $rh_ret->{'note'},
		$rh_ret->{'dur'}, $rh_ret->{'type'},
		@info) = split(' ', $fragline); 

	$rh_ret->{'info'} = join (' ', @info);
	chop $rh_ret->{'number'};

	return $rh_ret;
}
#
# $Log$
