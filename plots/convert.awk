# Note to number converter
#
# $Id$
# (log at end of file)
#
function get_note(name,
	pos)
{
	pos = match(name, /[0-9]/);
	return substr(name, 0, pos-1);
}
function get_octave(name,
	pos)
{
	pos = match(name, /[0-9]/);
	return substr(name, pos);
}
function convert(name,
	octave, note, nnotes)
{
	octave = get_octave(name);
	note = get_note(name);

	nnote = _note_[note];
	nnotes = octave * 12;
		
	return nnote+nnotes;
}
BEGIN {
	FS="|";
	OFS="|";

	_note_["b#"]  = _note_["c"]  = _note_["dff"] = 0;
	_note_["b##"] = _note_["c#"] = _note_["df"] = 1;
	_note_["c##"] = _note_["d"]  = _note_["eff"] = 2;
	_note_["d#"]  = _note_["ef"] = 3;
	_note_["d##"] = _note_["e"]  = _note_["fb"] = 4;
	_note_["e#"]  = _note_["f"]  = _note_["gff"] = 5;
	_note_["e##"] = _note_["f#"] = _note_["gf"] = 6;
	_note_["f##"] = _note_["g"]  = _note_["aff"] = 7;
	_note_["g#"]  = _note_["af"] = 8;
	_note_["g##"] = _note_["a"]  = _note_["bff"] = 9;
	_note_["a#"]  = _note_["bf"] = 10;
	_note_["a##"] = _note_["b"]  = _note_["cf"] = 11;
}
/^=/ {
	print;
	next;
}
{
	$3 = convert($3);
	print;
}
#
# $Log$
