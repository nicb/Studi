#
# $Id$
#
function print_header(x_size, y_size,
	max_x, max_y)
{
	max_x = _max_at_ * _x_conv_;
	max_y = _max_note_ * _y_conv_;
	scale = 2.54;
#	printf(".PS %f %f\nscale=%f\n.ps 6\n", x_size/scale, y_size/scale,
#		scale);
	printf(".PS\nscale=2.54\n.ps 6\n");
	printf("Top: line invis from 0,0 to %f,0\n", max_x);
}
function print_trailer()
{
	print ".PE";
}
function get_fillval(type)
{
	return _type_[type];
}
function print_frag(at, note, dur, type, info,
	x, width, height, fill)
{
	x = at * _x_conv_;
	y = (_max_note_-note) * _y_conv_;
	width = dur * _x_conv_;
	height = 6 * _y_conv_;
	fill = get_fillval(type);
	

	printf("box width %f ht %f with .w at Top.w + (%f,-%f) fill %f\n",
		width, height, x, y, fill);
}
BEGIN {
	FS = "|";
	_max_at_ = 675;
	_max_note_ = 56;
	_x_size_ = 29;
	_y_size_ = 24;

# no changes below here

	_x_conv_ = _x_size_/_max_at_;
	_y_conv_ = _y_size_/_max_note_;
	_type_["A"] = 0;
	_type_["B"] = 0.3;
	_type_["C"] = 0.6;

	print_header(_x_size_, _y_size_);
}
END {
	print_trailer();
}
/^=/ {
	print "# " $0;
	next;
}
{
	print_frag($2, $3, $4, $5, $6);
}
#
# $Log$
