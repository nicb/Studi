#
# $Id: rfit.awk,v 0.11 1994/04/29 18:49:57 nicb Exp $
#
# rhythm fitter - takes in sorted input (like fsorted.dat)
#		  and tries to fit into rhythmic/metric grid
#
function abs(num)
{
	return (num < 0) ? -num : num;
}
function print_allowed_vals() # for debug
{
	if (!no_normal_output)
		print "# allowed values(" num_allowed_values "): " allowed_vals_string;
}
function dur_best_fit(num, bar_dur, result_vector,	best_diff, _max_value,
	this_dur, diff_dur, i, j)
{
	best_diff = 10000;
	_max_value = max_grain_value * 8;
	for (i=1; i <= num_allowed_values; ++i)
	{
		div_bar = bar_dur / allowed_vals[i];
		for (j = 1; j <= _max_value; ++j)
		{
			this_dur = j * div_bar;
			diff_dur = abs(num - this_dur);
			if (diff_dur < (best_diff - diff_eps))
			{
				best_diff = diff_dur;
				result_vector[0] = j;
				result_vector[1] = allowed_vals[i];
			}
			else if (this_dur > num)
				break;
		}
	}

	return best_diff;
}
function at_best_fit(num, dur_div, bar_dur, result_vector,	best_diff,
	_max_value, this_dur, this_div, diff_dur, i, j, div_bar,
	last_diff)
{
	best_diff = 10000;
	_max_value = max_grain_value * 8;
	for (i=1; (this_div = i * dur_div) <= max_grain_value; i *= 2)
	{
		last_diff = 10000;
		div_bar = bar_dur / this_div;
		for (j = 0; j < _max_value; ++j)
		{
			this_dur = j * div_bar;
			diff_dur = abs(num - this_dur);
			if (diff_dur < (best_diff - diff_eps))
			{
				best_diff = diff_dur;
				result_vector[0] = j;
				result_vector[1] = this_div;
			}
			if (diff_dur > last_diff)
				break;
			last_diff = diff_dur;
		}
	}

	return best_diff;
}
function collect_stats(at_diff,dur_diff)
{
	++total_calls;
	tot_at_diff += at_diff;
	tot_dur_diff += dur_diff;
}
function fit_output(at, note, dur, bar_dur,	at_vector, dur_vector, tmp_out)
{
	best_dur_diff = dur_best_fit(dur, bar_dur, dur_vector);
	best_at_diff = at_best_fit(at, dur_vector[1], bar_dur, at_vector);
	collect_stats(best_at_diff, best_dur_diff);
	if (debug_output)
		return sprintf ("%d:%d:%s:%d:%d:%g:%g", at_vector[0]+1,
		at_vector[1], get_note_name(note, tmp_out), dur_vector[0],
		dur_vector[1], best_at_diff, best_dur_diff); 
	else
		return sprintf ("%3d/%-3d %4s %3d/%-3d", at_vector[0]+1,
		at_vector[1], get_note_name(note, tmp_out), dur_vector[0],
		dur_vector[1]);
}
function calculate_tempo(t)
{
	if (t != last_calculated_tempo)
	{
		last_calculated_tempo = t;
		calculate_tempo_bar_dur = (240 / t); # (60/tempo)*4
	}
	return calculate_tempo_bar_dur;
}
function get_note_name(num, out_s,	_note, _octave)
{
	_octave = (int(num/12)-2);
	_note = (int(num%12))+1;
	out_s = sprintf("%s%d", note_name[_note], _octave);
	return out_s;
}
function calculate_offset(cur_at, bar_dur, 	true_offset, diff_offset)
{
	true_offset = cur_at % bar_dur;
	diff_offset = true_offset - bar_dur;
	if (abs(diff_offset) < off_eps)
	{
		if (diff_offset > 0)
			true_offset -= off_eps;
		else
			true_offset += off_eps;
	}
	true_offset %= bar_dur;
	return true_offset;
}
function do_output(at, note, dur, tempo, 	cur_dur, offset_dur, out_s)
{
	cur_dur = calculate_tempo(tempo);
	offset_dur = calculate_offset(at + phase_offset, cur_dur);
	if (offset_dur < last_offset_dur)
	{
		last_offset_dur = 0;
		++bar_num;
		out_s = sprintf("===\n");
	}
	else
		last_offset_dur = offset_dur;
	out_s = sprintf("%s%s",out_s,fit_output(offset_dur, $2, $3, cur_dur));
	if (!no_normal_output)
		print out_s;
}
function print_version(out_s)
{
    out_s = "# $Id: rfit.awk,v 0.11 1994/04/29 18:49:57 nicb Exp $\n" \
	"# allowed divisors: \"" allowed_vals_string "\"\n";

	return out_s;
}
function do_header(sect, tempo,		out_s, tmp_s)
{
	if (phase_offset)
		out_s = \
		sprintf("%s# %s: mm/quarter = %4.2f, phase offset = %4.8g",
			print_version(tmp_s), sect, tempo, phase_offset);
	else
		out_s = sprintf("%s# %s: mm/quarter = %4.2f",
			print_version(tmp_s), sect, tempo);
	if (!no_normal_output)
		print out_s;
}
BEGIN {
	last_offset_dur = 10000;
	off_eps = 0.01; at_eps = 0.01; diff_eps = 0.00001;
	allowed_vals_string = "1:2:4:6:8:10:12:16:20:24:26:28:32:40:48:56:64";
	num_allowed_values = split(allowed_vals_string, allowed_vals, ":");
	max_grain_value = 64;
	split("c:c#:d:d#:e:f:f#:g:g#:a:a#:b", note_name, ":");
}
END {
	if (debug_output)
		print "average at diff :" (tot_at_diff/total_calls) ":, average dur diff :" (tot_dur_diff/total_calls) ":";
}
NR == 1 {
	phase_offset = ph ? ph : 0;
	if (version_only)
	{
		printf print_version();
		exit(0);
	}
}
/^allowed/ {
	for (i in allowed_vals)
		delete allowed_vals[i];
	allowed_vals_string = $2;
	num_allowed_values = split(allowed_vals_string, allowed_vals, ":");
	print_allowed_vals();
	next;
}
{
	if (!tempo_x)
		tempo_x = 62;
	if (!first_time)
	{
		first_time = 1;
		do_header(section_name, tempo_x);
	}
	do_output($1,$2,$3,tempo_x);
	next;
}
#
# $Log: rfit.awk,v $
# Revision 0.11  1994/04/29 18:49:57  nicb
# put the full divisors as default
#
# Revision 0.10  93/10/30  13:10:32  nicb
# modified header to print complete information
# 
# Revision 0.9  93/08/06  17:37:56  nicb
# now average statistics follow the -d option
# 
# Revision 0.8  93/07/27  22:25:25  nicb
# simplified single-section pass two
# 
# Revision 0.7  93/07/14  10:35:14  nicb
# modified at calculator to better fit dur calculation
# 
# Revision 0.6  93/07/11  15:06:09  nicb
# added <allowed> rhythmic divisions record
# optimized array loadings
# 
# Revision 0.5  93/07/10  10:34:22  nicb
# corrected bug in do_header() function
# 
# Revision 0.4  93/07/10  10:27:59  nicb
# added phase offset variable
# added version function
# 
# Revision 0.3  93/07/10  09:06:38  nicb
# added all sections
# 
# Revision 0.2  93/05/16  16:07:02  nicb
# *** empty log message ***
# 
# Revision 0.1  93/05/09  16:44:12  nicb
# added debug output option
# added 1/1 rhythm feature
# 
# Revision 0.0  93/05/09  15:37:21  nicb
# Initial revision
# 
#
