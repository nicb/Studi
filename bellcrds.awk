#
# $Id: bellcrds.awk,v 1.0 1994/01/23 15:36:15 nicb Exp $
# bellcrds.awk: bell chords builders
#
function abs(n)
{
	return (n < 0) ? -n : n;
}
function find_index(notes, n, nf,
	i, retval)
{
	for (i = 0; i < nf; ++i)
		if (n == notes[i])
		{
			retval = i;
			break;
		}

	return retval;
}
function find_nearest_index(notes, frqs, nf, nfrq, last_diff,
	i, diff)
{
	last_diff[0] = 10000;
	for (i = 0; i < nf; ++i)
	{
		diff = abs(nfrq - frqs[i]);
		if (diff < last_diff[0])
			last_diff[0] = diff;
		else
			break;
	}

	return i ? i-1 : 0;
}
function load_arrays(notes, frqs,
	freq, inc, octave, j, i)
{
	split("a:a#:b:c:c#:d:d#:e:f:f#:g:g#", note_names, ":");
	freq = 27.5;
	inc = 1.0594658;
	octave = -1;
	j = 1;
	i = 0;
	while (freq < 6000)
	{
		if (note_names[j] == "c")
			++octave;
		notes[i] = sprintf("%s%d", note_names[j], octave);
		frqs[i] = freq;
		freq *= inc;
		if (note_names[j] == "g#")
			j = 1;
		else
			++j;
		++i;
	}

	return i;
}
BEGIN {
	nf = load_arrays(notes, frequencies);
	a = ARGV[1];
	b = ARGV[2];
	a_ndx = find_index(notes, a, nf);
	b_ndx = find_index(notes, b, nf);
	if (!a_ndx || !b_ndx)
	{
		print "Error: a(" a ") or b(" b ") not found";
		exit(-1);
	}
	top_ndx = find_nearest_index(notes, frequencies, nf,
		frequencies[a_ndx] + frequencies[b_ndx], top_diff);
	bot_ndx = find_nearest_index(notes, frequencies, nf,
		frequencies[b_ndx] - frequencies[a_ndx], bot_diff);

	printf("%4s\t(%f)\n", notes[top_ndx], top_diff[0]);
	printf("%4s\n", b);
	printf("%4s\n", a);
	printf("%4s\t(%f)\n", notes[bot_ndx], bot_diff[0]);
}
